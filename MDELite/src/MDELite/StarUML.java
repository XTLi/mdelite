package MDELite;

import CoreMDELite.*;

public class StarUML extends MDELiteObject {

    @Override
    public String fileType() {
        return ".xml";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public StarUML(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public StarUMLpl toStarUMLpl() {
        StarUMLpl result = new StarUMLpl(filename);
        String[] args = { partialName };
        MDELite.starumlparser.Main.main(args);
        result.conform();
        return result;
    }

}
