package MDELite.dot2sdb;

import CoreMDELite.HomePath;

public class Main {

    public static void main(String[] args) {
        String filename = args[0];
        SDB sdb = new SDB(filename);
        Dot d1 = new Dot(filename);
        Dot d2 = d1.dot2dot("K");
        SDB onlyPositions = d2.toSDB();
        SDB scaledPositions = onlyPositions.scalePosition("SP");
        SDB noPosition = sdb.project(filename+"noPosition",HomePath.homePath + "libpl/removePosition.pl");
        String[] array = {noPosition.fullName,scaledPositions.fullName};
        SDB patched = new SDB(filename, array); 
    }
    
}
