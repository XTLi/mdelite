package MDELite.dot2sdb;

import CoreMDELite.HomePath;
import CoreMDELite.GProlog;

public class SDB extends GProlog {

    @Override
    public String fileType() {
        return ".sdb.pl";
    }

    @Override
    public String partialFileType() {
        return ".sdb";
    }

    public SDB(String filename) {
        super(filename);
    }

    public SDB(String filename, String[] files) {
        super(filename, files);
    }

    /**
     * **************** methods/transformations of SDB objects ***********
     */
    public Dot toDot() {
        return toDot("");
    }

    public Dot toDot(String extra) {
        Dot result = new Dot(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/p2dot.vm");
        return result;
    }

    public SDB project(String outputFilename, String plfile) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            this.fullName, HomePath.homePath + "libpl/sdb.schema.pl", plfile, HomePath.homePath + "libpl/print.pl"};
        SDB tmp = new SDB("tmp", list);
        SDB result = new SDB(outputFilename);
        tmp.executeProlog(result);
        tmp.delete();
        // remember: don't test conformance as it won't pass
        // reason: the result of this operatin is some part of an SDB database
        return result;
    }

    public SDB scalePosition(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
                         HomePath.homePath + "libpl/print.pl", this.fullName, HomePath.homePath + "libpl/scalePosition.pl"};
        SDB tmp = new SDB("tmp", list);
        SDB result = new SDB(filename + extra);
        tmp.executeProlog(result);
        tmp.delete();
        // don't test conformance as nothing will have changed
        // remember: only tuples of the position table are updated
        // so no logical constraints have been violated
        return result;
    }
    
    @Override
    public void conform(){
    }

}
