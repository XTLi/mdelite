package MDELite;

import CoreMDELite.*;

public class yEDUMLpl extends GProlog {

    @Override
    public String fileType() {
        return ".yeduml.pl";
    }

    @Override
    public String partialFileType() {
        return ".yeduml";
    }

    public yEDUMLpl(String filename) {
        super(filename);
    }

    public yEDUMLpl(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public SDB toSDB() {
        return toSDB("");
    }

    public SDB toSDB(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/yeduml2sdb.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/sdb.schema.pl",
            HomePath.homePath + "libpl/sdb.run.pl",
            this.fullName};
        yEDUMLpl tmp = new yEDUMLpl("tmp", list);
        SDB result = new SDB(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public yED toyED() {
        return toyED("");
    }

    public yED toyED(String extra) {
        yED result = new yED(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/yedumlXML.vm");
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/yeduml.conform.pl"};
        yEDUMLpl temp = new yEDUMLpl("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
