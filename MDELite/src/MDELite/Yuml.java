package MDELite;

import CoreMDELite.*;

public class Yuml extends MDELiteObject {

    @Override
    public String fileType() {
        return ".yuml";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public Yuml(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public Yumlpl toYumlpl() {
        Yumlpl result = new Yumlpl(filename);
        String[] args = { partialName };
        MDELite.yumlparser.Main.main(args);
        result.conform();
        return result;
    }

}
