package MDELite.gliffy;

class Inherits extends UmlAssociation {
	public Inherits(String id, String s, String e){
		this.id = id;
		startId = s;
		endId = e;
	}

	public String toProlog(){
		return "gliffy_inherits(" + id + ", " + startId + ", " + endId + ").\n";
	}
}