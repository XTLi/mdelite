package MDELite.gliffy;

class Log {
	public enum Level { DEBUG, INFO, WARN, ERROR};
	public static Level loggingLevel = Level.INFO;
	
	public static void debug(String msg){
		if(loggingLevel.ordinal() <= Level.DEBUG.ordinal())
			System.err.println(msg);
	}
	public static void info(String msg){
		if(loggingLevel.ordinal() <= Level.INFO.ordinal())
			System.err.println(msg);
	}
	public static void warn(String msg){
		if(loggingLevel.ordinal() <= Level.WARN.ordinal())
			System.err.println(msg);
	}
	public static void error(String msg){
		if(loggingLevel.ordinal() <= Level.ERROR.ordinal())
			System.err.println(msg);
	}
}