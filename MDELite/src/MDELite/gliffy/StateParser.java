package MDELite.gliffy;

import java.util.*;
import java.io.*;
import com.google.gson.*;

public class StateParser {
	private List<Node> nodes = new LinkedList<Node>();
	private List<Transition> transitions = new LinkedList<Transition>();
	private List<Position> positions = new LinkedList<Position>();

    public static void main(String... args) throws Exception {
		Log.loggingLevel = Log.Level.INFO;
	
		File file;
		try {
			file = new File(args[0]);
		}
		catch (Exception e){
			Log.error("Usage: java Gliffy.StateParser <filename.gliffy>");
			return;
		}
		
	
        String json = new Scanner( file, "UTF-8" ).useDelimiter("\\A").next();

		StateParser parser = new StateParser();
		parser.parse(json);
		System.out.print(parser.toProlog());
    }
	
	public void parse(String json) {
		JsonElement jelement = new JsonParser().parse(json);
		JsonObject  jobject = jelement.getAsJsonObject();
		jobject = jobject.getAsJsonObject("stage");
		JsonArray jarray = jobject.getAsJsonArray("objects");
		for(JsonElement j : jarray){
			processElement(j);
		}
	}
	
	private void processElement(JsonElement e){
		JsonObject o = e.getAsJsonObject();
		String uid = o.get("uid").toString();
		String id = o.get("id").toString();
		int x = o.get("x").getAsInt();
		int y = o.get("y").getAsInt();
		Log.debug(id + ": \t" + uid);
		
		if(uid.indexOf("com.gliffy.shape.basic.") >= 0){
			JsonArray children = o.getAsJsonArray("children");
			if(children != null){
				for (JsonElement child : children){
					String childid = child.getAsJsonObject().get("id").toString();
					JsonObject graphic = child.getAsJsonObject().getAsJsonObject("graphic");
					String type = graphic.getAsJsonPrimitive("type").getAsString();
					if ("Text".equals(type)){
						JsonObject text = graphic.getAsJsonObject().getAsJsonObject("Text");
						String html = text.getAsJsonPrimitive("html").getAsString();
						String lable = html.replaceAll("<[^>]*>", "");
						Log.debug(childid + ": \t" + lable);
						nodes.add(new Node(id, lable, "state"));
						positions.add(new Position(id, x, y));
					}
				}
			}
			JsonObject graphic = o.getAsJsonObject("graphic");
			String type = graphic.getAsJsonPrimitive("type").getAsString();
			if ("Line".equals(type)){
				JsonObject line = graphic.getAsJsonObject().getAsJsonObject("Line");
				int startArrow = line.getAsJsonPrimitive("startArrow").getAsInt();
				int endEnd = line.getAsJsonPrimitive("endArrow").getAsInt();
				
				JsonObject constraints = o.getAsJsonObject("constraints");
				String startId = constraints.getAsJsonObject("startConstraint").getAsJsonObject("StartPositionConstraint").getAsJsonPrimitive("nodeId").getAsString();
				String endId = constraints.getAsJsonObject("endConstraint").getAsJsonObject("EndPositionConstraint").getAsJsonPrimitive("nodeId").getAsString();
				
				Log.debug("\t" + startId + " -> " + endId);
				transitions.add(new Transition(id, startId, endId));
			}
		}
	}

	public String toProlog(){
		String prolog = "";
		prolog += "dbase(gliffy_fsm,[gliffy_node,gliffy_transition,gliffy_position]).\n";
		prolog += ":- dynamic gliffy_node/3.";
		prolog += "\ntable(gliffy_node,[nodeid,\"name\",type]).\n";
		for(Node n : nodes){
			prolog += n.toProlog();
		}
		prolog += ":- dynamic gliffy_transition/3.";
		prolog += "\ntable(gliffy_transition,[transid,startsAt,endsAt]).\n";
		for(Transition t : transitions){
			prolog += t.toProlog();
		}
		prolog += ":- dynamic gliffy_position/3.";
		prolog += "\ntable(gliffy_position,[id,x,y]).\n";
		for(Position p : positions){
			prolog += p.toProlog();
		}
		
		return prolog;
	}
}