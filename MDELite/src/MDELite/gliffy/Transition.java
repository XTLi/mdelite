package MDELite.gliffy;

public class Transition {
    private String id;
    private String startsAt;
    private String endsAt;
    private String quote = "\'";
    
    public Transition(String id, String startsAt, String endsAt){
        this.id = id;
        this.startsAt = startsAt;
        this.endsAt = endsAt;
    }
	
	public String toProlog(){
		return "gliffy_transition(" + id + ", " + startsAt + ", " + endsAt + ").\n";
	}
    
    public String getId(){
        return this.id;
    }
    
    public String getStartsAt(){
        return this.startsAt;
    }
    
    public String getEndsAt(){
        return this.endsAt;
    }
}
