package MDELite.gliffy;

public class Node {
    private String id;
    private String name;
    private String type;
    private String quote = "\'";
    
    public Node(String id, String name, String type){
        this.id = id;
        this.name = quote + name + quote;
        this.type = type;
    }
    
	public String toProlog(){
		return "gliffy_node(" + id + ", " + name + ", " + type + ").\n";
	}
	
    public String getId(){
        return this.id;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getType(){
        return this.type;
    }
}
