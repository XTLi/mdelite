package MDELite.gliffy;

import java.util.*;
import java.io.*;
import com.google.gson.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UmlParser {
	private List<UmlObject> objects = new LinkedList<UmlObject>();
	private List<UmlAssociation> associations = new LinkedList<UmlAssociation>();
	private List<Position> positions = new LinkedList<Position>();

    public static void main(String... args) {
            try {
                Log.loggingLevel = Log.Level.ERROR;
        
                File file;
                try {
                        file = new File(args[0]+".gliffy");
                }
                catch (Exception e){
                        Log.error("Usage: java Gliffy.StateParser <filename.gliffy>");
                        return;
                }
                
                Log.debug("Reading " + args[0] + "...");
	
        
        String json = new Scanner( file, "UTF-8" ).useDelimiter("\\A").next();

                UmlParser parser = new UmlParser();
                parser.parse(json);
                
                PrintStream out = new PrintStream(new File(args[0]+".gliffy.pl"));
                out.print(parser.toProlog());
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(UmlParser.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
	
	public void parse(String json) {
		JsonElement jelement = new JsonParser().parse(json);
		JsonObject  jobject = jelement.getAsJsonObject();
		jobject = jobject.getAsJsonObject("stage");
		JsonArray jarray = jobject.getAsJsonArray("objects");
		for(JsonElement j : jarray){
			processElement(j);
		}
	}
	
	private void processElement(JsonElement e){
		JsonObject o = e.getAsJsonObject();
		String uid = o.get("uid").toString();
		String id = o.get("id").toString();
		Log.debug(id + ": \t" + uid);
		
		
		int x = o.get("x").getAsInt();
		int y = o.get("y").getAsInt();
		
		if(uid.indexOf("com.gliffy.shape.uml.") >= 0){
			String type = "";
			JsonObject graphic = o.getAsJsonObject("graphic");
			if (graphic != null)
				type = graphic.getAsJsonPrimitive("type").getAsString();
			//Log.debug("type: " + type);
			if ("Line".equals(type)){
				// Get association endpoints
				JsonObject line = graphic.getAsJsonObject().getAsJsonObject("Line");
				int startArrow = line.getAsJsonPrimitive("startArrow").getAsInt();
				int endArrow = line.getAsJsonPrimitive("endArrow").getAsInt();
				
				JsonObject constraints = o.getAsJsonObject("constraints");
				String startId = constraints.getAsJsonObject("startConstraint").getAsJsonObject("StartPositionConstraint").getAsJsonPrimitive("nodeId").getAsString();
				String endId = constraints.getAsJsonObject("endConstraint").getAsJsonObject("EndPositionConstraint").getAsJsonPrimitive("nodeId").getAsString();
				
				Log.debug("\t" + startId + " -> " + endId);
				
				// Get association lables
				String closeRole = "";
				double closeDist = Double.MAX_VALUE;
				String farRole = "";
				double farDist = Double.MAX_VALUE;
				
				JsonArray children = o.getAsJsonArray("children");
				if(children != null){
					for (JsonElement child : children){
					
					
						// Distance calculation is broken. Check if parent and child use the same scale for x and y?
					
					
						String childid = child.getAsJsonObject().get("id").toString();
						//int childx = child.getAsJsonObject().get("x").getAsInt();
						//int childy = child.getAsJsonObject().get("y").getAsInt();
						JsonObject childgraphic = child.getAsJsonObject().getAsJsonObject("graphic");
						String childtype = childgraphic.getAsJsonPrimitive("type").getAsString();
						if ("Text".equals(childtype)){
							JsonObject text = childgraphic.getAsJsonObject().getAsJsonObject("Text");
							double dist = text.get("lineTValue").getAsDouble();
							String html = text.getAsJsonPrimitive("html").getAsString();
							String lable = html.replaceAll("<br />", " ").replaceAll("<[^>]*>", "");
							Log.debug(childid + ": \t" + lable);
							//nodes.add(new Node(id, lable, "state"));
							//double dist = Math.sqrt(Math.pow(childx-x,2)+Math.pow(childy-y,2));
							if (dist < closeDist){
								farRole = closeRole;
								farDist = closeDist;
								closeRole = lable;
								closeDist = dist;
							}
							else if (dist < farDist){
								farRole = lable;
								farDist = dist;
							}
						}
					}
				}
				if (uid.indexOf("implements") >= 0 || uid.indexOf("generalization") >= 0)
					associations.add(new Inherits(id, startId, endId));
				if (uid.indexOf("aggregation") >= 0 || uid.indexOf("association") >= 0)
					associations.add(new Association(id, startId, closeRole, startArrow!=0, endId, farRole, endArrow!=0));
			}
			// Not an association
			else {
				JsonArray children = o.getAsJsonArray("children");
				if(children != null){
					String name = "", fields = "", methods ="";
					for (int i=0; i < children.size(); i++){
						JsonElement child = children.get(i);
						String childid = child.getAsJsonObject().get("id").toString();
						JsonObject childgraphic = child.getAsJsonObject().getAsJsonObject("graphic");
						String childtype = childgraphic.getAsJsonPrimitive("type").getAsString();
						if ("Shape".equals(childtype)){
							JsonArray children2 = child.getAsJsonObject().getAsJsonArray("children");
							
							// Use UID to process process Text elements 
								
							for (JsonElement c : children2){
								childid = c.getAsJsonObject().get("id").toString();
								childgraphic = c.getAsJsonObject().getAsJsonObject("graphic");
								childtype = childgraphic.getAsJsonPrimitive("type").getAsString();
								if ("Text".equals(childtype)){
									JsonObject text = childgraphic.getAsJsonObject().getAsJsonObject("Text");
									String html = text.getAsJsonPrimitive("html").getAsString();
									String lable = html.replaceAll("<br />", ";").replaceAll("<[^>]*>", "").replaceAll("&lt;&lt;interface&gt;&gt;", "");
									if (lable.charAt(lable.length()-1) == ';')
										lable = lable.substring(0, lable.length()-1);
									Log.debug(childid + ": \t" + lable);
									
									if( i == 0 ){
										name += lable;
									}
									if( i == 1 ){
										fields += lable;
									}
									if( i == 2 ){
										methods += lable;
									}
								}
							}
			
						}
					}
				
					if (uid.indexOf("interface") >= 0){
						objects.add(new UmlInterface(id, name, methods));
						positions.add(new Position(id, x, y));
					}
					if (uid.endsWith("class\"") || uid.endsWith("class2\"")){
						objects.add(new UmlClass(id, name, fields, methods));
						positions.add(new Position(id, x, y));
					}
				}
			}
		}
	}

	
	// TODO: change all to "Gliffy" prolog, not SDB
	
	public String toProlog(){
		String prolog = "";
		prolog += ":-style_check(-discontiguous).\n";
		
		prolog += "\ndbase(gliffy_uml,[gliffy_class,gliffy_association,gliffy_interface,gliffy_inherits,gliffy_position]).";
		prolog += "\ntable(gliffy_class,[id,\"name\",\"fields\",\"methods\"]).\n" +
			"table(gliffy_association,[id, cid1,\"role1\",arrow1,cid2,\"role2\",arrow2]).\n" +
			"table(gliffy_interface,[id,\"name\",\"methods\"]).\n" +
			"table(gliffy_inherits,[id, cid,iid]).\n" +
			"table(gliffy_position,[id,x,y]).\n";
		prolog += ":- dynamic gliffy_class/4.\n" +
			":- dynamic gliffy_interface/3.\n" +
			":- dynamic gliffy_association/7.\n" +
			":- dynamic gliffy_inherits/3.\n" +
			":- dynamic gliffy_position/3.\n";
		for(UmlObject o : objects){
			prolog += o.toProlog();
		}
		for(UmlAssociation a : associations){
			prolog += a.toProlog();
		}
		for(Position p : positions){
			prolog += p.toProlog();
		}
		
		return prolog;
	}
}