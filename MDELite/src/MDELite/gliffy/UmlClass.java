package MDELite.gliffy;

class UmlClass extends UmlObject {
	public String fields, methods;
	
	public UmlClass(String id, String name, String fields, String methods){
		this.id = id;
		this.name = name;
		this.fields = fields;
		this.methods = methods;
	}
	
	public String toProlog(){
		return "gliffy_class(" + id + ", " + quote(name) + ", " + quote(fields) + ", " + quote(methods) + ").\n";
	}
}