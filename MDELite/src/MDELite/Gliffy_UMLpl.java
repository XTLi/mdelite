package MDELite;

import CoreMDELite.*;

public class Gliffy_UMLpl extends GProlog {

    @Override
    public String fileType() {
        return ".gliffy.pl";
    }

    @Override
    public String partialFileType() {
        return ".gliffy";
    }

    public Gliffy_UMLpl(String filename) {
        super(filename);
    }

    public Gliffy_UMLpl(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public SDB toSDB() {
        return toSDB("");
    }

    public SDB toSDB(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/gliffy2sdb.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/sdb.schema.pl",
            HomePath.homePath + "libpl/sdb.run.pl",
            this.fullName};
        Gliffy_UMLpl tmp = new Gliffy_UMLpl("tmp", list);
        SDB result = new SDB(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public Gliffy toGliffy() {
        return toGliffy("");
    }

    public Gliffy toGliffy(String extra) {
        Gliffy result = new Gliffy(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/gliffy_uml.vm");
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/gliffy_uml.conform.pl"};
        Gliffy_UMLpl temp = new Gliffy_UMLpl("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
