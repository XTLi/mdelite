/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MDELite.yedparser;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.DocumentException;

/**
 *
 * @author Hang
 */
public class Main {
        public static void marquee() {
        System.out.println("Usage: YEDUMLParser <YEDUML xml_file>");
        System.exit(1);
    }
    
    public static void main(String[] args) {
        if (args.length != 1) {
            marquee();
        }
        try {
                ClassParser.generatePL(args[0]);
            } catch (DocumentException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}
