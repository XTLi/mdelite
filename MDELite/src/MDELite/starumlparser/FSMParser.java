package MDELite.starumlparser;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 *
 * @author marknv
 */
public class FSMParser {

    public static void generatePL(String fileName) throws DocumentException, IOException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(fileName + ".xml");
        StringBuilder sb = new StringBuilder();

        // declare db and tables
        sb.append("dbase(staruml,[node,transition,position]).\n");
        sb.append("table(starnode,[nodeid,name,type]).\n");
        sb.append("table(startransition,[transid,startsAt,endsAt]).\n");
        sb.append("table(starposition,[id,x,y]).\n");
        sb.append("\n");

        // building node predicates
        List<Element> nodes = document.selectNodes("//UML:CompositeState.subvertex/*");
        for (Element node : nodes) {
            String nodeId = KlassParser.idParse(node.valueOf("@xmi.id"));
            String nodeName = KlassParser.quotes(node.valueOf("@name").toLowerCase());
            String nodeType = goodName(node.getName().toLowerCase());
            String nodePredicate = "starnode" + "(" + nodeId + ", " + nodeName + ", " + nodeType + ")" + "." + "\n";
            sb.append(nodePredicate);
        }
        sb.append("\n");

        // building transition predicates
        List<Element> transitions = document.selectNodes("//UML:Transition");
        for (Element node : transitions) {
            String transId = KlassParser.idParse(node.valueOf("@xmi.id"));
            String transSource = KlassParser.idParse(node.valueOf("@source"));
            String transTarget = KlassParser.idParse(node.valueOf("@target"));
            String transitionPredicate = "startransition" + "(" + transId + ", " + transSource + ", " + transTarget + ")" + "." + "\n";
            sb.append(transitionPredicate);
        }
        sb.append("\n");

        // building positions
        List<Element> positions = document.selectNodes("//UML:DiagramElement");
        for (Element position : positions) {
            String id = KlassParser.idParse(position.valueOf("@subject"));
            String x = position.valueOf("@geometry").split(",")[0].trim();
            String y = position.valueOf("@geometry").split(",")[1].trim();
            x = String.valueOf(Integer.parseInt(x) / 2);
            y = String.valueOf(Integer.parseInt(y) / 2);

            String genPredicate = "starposition" + "(" + id + ", " + x + ", " + y + ")" + "." + "\n";
            sb.append(genPredicate);
        }

        //write to file
        writeFile(fileName, sb.toString());
    }

    private static void writeFile(String fileName, String content) throws IOException {
        File file = new File(fileName + ".staruml.pl");
        if (!file.exists()) {
            file.createNewFile();
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(content);
            bufferedWriter.flush();
        }
    }

    private static String goodName(String name) {
        switch (name) {
            case "simplestate":
                return "state";
            case "pseudostate":
                return "start";
            case "finalstate":
                return "stop";
        }
        return name;
    }

    private static String badName(String name) {
        switch (name) {
            case "state":
                return "simplestate";
            case "start":
                return "pseudostate";
            case "stop":
                return "finalstate";
        }
        return name;
    }

    public static void main(String[] args) throws DocumentException {
        try {
            FSMParser.generatePL(args[0]);
        } catch (IOException ex) {
            Logger.getLogger(FSMParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
