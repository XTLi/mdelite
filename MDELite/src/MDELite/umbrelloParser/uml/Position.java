/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MDELite.umbrelloParser.uml;

import org.dom4j.Node;

/**
 *
 * @author troy
 */
public class Position {
    String id;
    String x;
    String y;
    
    public Position(String id, String x, String y){
        this.id = "\'i" + id.toLowerCase() + "\'";
        this.x = x;
        this.y = y;
    }
}
