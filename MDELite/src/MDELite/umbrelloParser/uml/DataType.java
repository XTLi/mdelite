/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MDELite.umbrelloParser.uml;

/**
 *
 * @author troy
 */
public class DataType {
    public static String getType(String id){
        String type = "";
        switch(id){
            case "5GdBmyEkBaVB" : type = "int"; break;
            case "aoz77y57tHYQ" : type = "char"; break;
            case "YLWE6EGBVtLF" : type = "bool"; break;
            case "8YoUqqmyesVD" : type = "string"; break;
            default : break;
        }
        return type;
    }
}
