/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MDELite.umbrelloParser.uml;

import java.util.List;
import org.dom4j.Node;

/**
 *
 * @author troy
 */
public class Association {
    String cid1;
    String role1;
    String arrow1;
    String cid2;
    String role2;
    String arrow2;
    
    public Association(List ends){
        Node end1 = (Node)ends.get(0);
        this.cid1 = "\'i" + end1.valueOf("@type").toLowerCase() + "\'";
        if(end1.valueOf("@name").isEmpty() || end1.valueOf("@multiplicity").isEmpty()){
            this.role1="\'\'";
        }else{
            this.role1 = "\'" + end1.valueOf("@name") + " " + end1.valueOf("@multiplicity") + "\'";
        }
        this.arrow1 = getArrow(end1.valueOf("@aggregation"));
        Node end2 = (Node)ends.get(1);
        this.cid2 = "\'i" + end2.valueOf("@type").toLowerCase() + "\'";
        if(end2.valueOf("@name").isEmpty() || end2.valueOf("@multiplicity").isEmpty()){
            this.role2="\'\'";
        }else{
            this.role2 = "\'" + end2.valueOf("@name") + " " + end2.valueOf("@multiplicity") + "\'";
        }        
        this.arrow2 = getArrow(end2.valueOf("@aggregation"));
        
        if(end1.valueOf("@isNavigable").equalsIgnoreCase("false")){
            this.arrow2 = "arrow";
        }
        if(end2.valueOf("@isNavigable").equalsIgnoreCase("false")){
            this.arrow1 = "arrow";
        }
    }
    
    private String getArrow(String s){
        String arrow;
        switch(s){
            case "none": arrow = "none"; break;
            case "composite": arrow = "comp";break;
            case "aggregate": arrow = "agg";break;
            default: arrow = "none";break;
        }
        return arrow;
    }
}