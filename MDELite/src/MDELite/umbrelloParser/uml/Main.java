/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MDELite.umbrelloParser.uml;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;

/**
 *
 * @author troy
 */
public class Main {
    public static void main(String[] args){
        try {
            XMIParser parser = new XMIParser();
            Document doc = parser.parse(args[0]+".xmi");
            ClassDiagram d = parser.createDiagram(doc);
            d.generateVioletDB(args[0]+".sdb.pl");
        } catch (DocumentException ex) {
            Logger.getLogger(XMIParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
