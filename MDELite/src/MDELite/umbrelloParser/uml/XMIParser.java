package MDELite.umbrelloParser.uml;

import java.util.Iterator;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class XMIParser{
    public static Document parse(String path) throws DocumentException{
        SAXReader reader  = new SAXReader(); 
        Document document = reader.read(path);
       
        return document;
    }
    
    public ClassDiagram createDiagram(Document doc){
        ClassDiagram diagram = new ClassDiagram();
        
        // Get all classes
        List classNodes = doc.selectNodes("//UML:Class");
        
        for (Iterator iter = classNodes.iterator(); iter.hasNext(); ) {
            Node classNode = (Node)iter.next();
            ClassNode c = Node2Class(classNode);
            diagram.addClass(c);
        }
        
        // Get all interfaces
        List interfaces = doc.selectNodes("//UML:Interface");
        
        for(Iterator itr = interfaces.iterator(); itr.hasNext();){
            Node iNode = (Node)itr.next();
            InterfaceNode c = Node2Interface(iNode);
            diagram.addInterface(c);
        }
        
        // Get all generalization
        List gens = doc.selectNodes("//UML:Generalization[@namespace='Logical View']");
        
        for(Iterator itr = gens.iterator(); itr.hasNext();){
            Node gen = (Node)itr.next();
            diagram.addGeneralization(gen);
        }
        // Get all abstraction
        List abs = doc.selectNodes("//UML:Abstraction[@namespace='Logical View']");
        
        for(Iterator itr = abs.iterator(); itr.hasNext(); ){
            Node abstraction = (Node)itr.next();
            diagram.addAbstraction(abstraction);
        }
        
        //Get all associations
        List associations = doc.selectNodes("//UML:Association[@namespace='Logical View']");
        
        for(Iterator itr = associations.iterator(); itr.hasNext(); ){
            Node ass = (Node)itr.next();
            List ends = ass.selectNodes("UML:Association.connection/UML:AssociationEnd");
            Association association = new Association(ends);
            diagram.addAssociation(association);
        }
        
        //Get all positions
        List classwidgets = doc.selectNodes("//widgets/classwidget");
        
        for(Iterator itr = classwidgets.iterator(); itr.hasNext(); ){
            Node classwidget = (Node)itr.next();
            String id = classwidget.valueOf("@xmi.id");
            String x = classwidget.valueOf("@x");
            String y = classwidget.valueOf("@y");
            Position p = new Position(id, x, y);
            diagram.addPosition(p);
        }
        
        List interfacewidgets = doc.selectNodes("//widgets/interfacewidget");
        
        for(Iterator itr = interfacewidgets.iterator(); itr.hasNext(); ){
            Node interfacewidget = (Node)itr.next();
            String id = interfacewidget.valueOf("@xmi.id");
            String x = interfacewidget.valueOf("@x");
            String y = interfacewidget.valueOf("@y");
            Position p = new Position(id, x, y);
            diagram.addPosition(p);
        }
        
        return diagram;
    }
    
    private ClassNode Node2Class(Node node) {
        String name = node.valueOf("@name");
        String id = node.valueOf("@xmi.id");
        List fields = node.selectNodes("UML:Classifier.feature/UML:Attribute");
        List methods = node.selectNodes("UML:Classifier.feature/UML:Operation");
        
        ClassNode c = new ClassNode(id, name, fields, methods);
        return c;   
    }

    private InterfaceNode Node2Interface(Node node) {
        String name = node.valueOf("@name");
        String id = node.valueOf("@xmi.id");
        List methods = node.selectNodes("UML:Classifier.feature/UML:Operation");
        
        InterfaceNode i = new InterfaceNode(id, name, methods);
        //System.out.println(i.toString());
        return i;
    }
}