/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MDELite.umbrelloParser.uml;

import java.util.List;
import org.dom4j.Node;

/**
 *
 * @author troy
 */
public class InterfaceNode {
    String id;
    String name;
    String methods ;
    
    public InterfaceNode(String id, String name, List methods){
        this.id = "\'i" + id.toLowerCase() + "\'";
        this.name = "\'" + name + "\'";
        this.methods = "";
        
        if(methods.size() != 0){
            Node method = (Node)methods.get(0);
            this.methods = method.valueOf("@name");
            for(int i = 1; i < methods.size(); i++){
                method = (Node)methods.get(i);
                this.methods += ";" + method.valueOf("@name");
            }
        }   
    }
    
    public String toString(){
        return id + name + methods;
    }
}
