/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MDELite.umbrelloParser.uml;

import java.util.List;
import org.dom4j.Node;

/**
 *
 * @author troy
 */
public class ClassNode {
    String id;
    String name;
    String fields;
    String methods;
    String superid;
    
    public ClassNode(String id, String name, List fields, List methods){
        this.id = "\'i" + id.toLowerCase() + "\'";
        this.name = "\'" + name + "\'";
        this.fields = "";
        this.methods = "";
        this.superid = "null";
        
        if(fields.size() != 0){
            Node field = (Node)fields.get(0);
            String fieldName = field.valueOf("@name");
            String type = DataType.getType(field.valueOf("@type"));
            this.fields = type + " " + fieldName;
            for(int i = 1; i < fields.size(); i++){
                field = (Node)fields.get(i);
                fieldName = field.valueOf("@name");
                type = DataType.getType(field.valueOf("@type"));
                this.fields += ";" + type + " " + fieldName;
            }
        }
       
        if(methods.size() != 0){
            Node method = (Node)methods.get(0);
            this.methods = method.valueOf("@name") + "()";
            for(int i = 1; i < methods.size(); i++){
                method = (Node)methods.get(i);
                this.methods += ";" + method.valueOf("@name") + "()";
            }
        }   
    }
    
    public String toString(){
        return id + name + fields + methods + superid;
    }
    
}
