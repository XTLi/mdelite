/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MDELite.umbrelloParser.uml;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import org.dom4j.Node;

/**
 *
 * @author troy
 */
public class ClassDiagram {
    public ArrayList<ClassNode> classes;
    public ArrayList<Association> associations;
    public ArrayList<InterfaceNode> interfaces;
    public ArrayList<InterfaceExtends> inExtends;
    public ArrayList<ClassImplements> cImpements;
    public ArrayList<Position> positions;
    
    public ClassDiagram(){
        this.classes = new ArrayList<ClassNode>();
        this.associations = new ArrayList<Association>();
        this.interfaces = new ArrayList<InterfaceNode>();
        this.cImpements = new ArrayList<ClassImplements>();
        this.inExtends = new ArrayList<InterfaceExtends>();
        this.positions = new ArrayList<Position>();
    }
    
    public void addClass(ClassNode n){
        classes.add(n);
    }
    
    public void addAssociation(Association a){
        associations.add(a);
    }

    void addInterface(InterfaceNode c) {
        interfaces.add(c);
    }
    
    void addGeneralization(Node node){
         String parent = node.valueOf("@parent").toLowerCase();
         String child = node.valueOf("@child").toLowerCase();
         ClassNode childClass = null;
         
         for(int i = 0; i < classes.size() ; i ++){
             ClassNode c = classes.get(i);
             if(c.id.equals("\'i" + child + "\'")){
                 childClass = c;
                 break;
             }
         }
         
         for(int i = 0; i < classes.size(); i ++){
             ClassNode c = classes.get(i);
             if(c.id.equals("\'i" + parent + "\'")){
                 childClass.superid = "i" + parent;
                 break;
             }
         }
    }
    
    void addAbstraction(Node node){
        String supplier = node.valueOf("@supplier").toLowerCase();
        String client = node.valueOf("@client").toLowerCase();
        
        for(int i = 0; i < classes.size() ; i ++){
            ClassNode c = classes.get(i);
            if(c.id.equals("\'i" + client + "\'")){
                ClassImplements ci = new ClassImplements(client, supplier);
                cImpements.add(ci);
                return;
            }
        }
        
        for(int i = 0; i < interfaces.size() ; i ++){
            InterfaceNode in = interfaces.get(i);
            if(in.id.equals("\'i" + client + "\'")){
                InterfaceExtends inEx = new InterfaceExtends(client, supplier);
                inExtends.add(inEx);
                return;
            }
        }
    }
    
    public void generateVioletDB(String target) {
        try {
            FileWriter fstream = new FileWriter(target);
            BufferedWriter out = new BufferedWriter(fstream);
            
            out.write(":-style_check(-discontiguous).\n");

            //Output database definition
            out.write("\ndbase(sdb,[class,association,interface,classImplements,interfaceExtends,position]).\n");

            //Ouput Nodes in node class 
            out.write("\ntable(class,[id,\"name\",\"fields\",\"methods\",superid]).\n");
            
            if (classes.size() > 0) {
                for (ClassNode c : classes) {
                    out.write("class(" + c.id + "," + c.name + "," + "\'" + c.fields + "\',\'" + c.methods + "\',\'" + c.superid + "\').\n");
                }
            }else out.write(":- dynamic class/5.\n");

            //Ouput Associations in association table 
            out.write("\ntable(association,[cid1,\"role1\",arrow1,cid2,\"role2\",arrow2]).\n");
            
            if (associations.size() > 0) {
                for (Association a : associations) {
                    out.write("association(" + a.cid1 + "," + a.role1+ "," + a.arrow1 + "," + a.cid2 + "," + a.role2 + "," + a.arrow2 + ").\n");
                }
            }else out.write(":- dynamic association/6.\n");
            
            //Output interface in interface table
            out.write("\ntable(interface,[id,\"name\",\"methods\"]).\n");
            
            if(interfaces.size() > 0){
                for(InterfaceNode i : interfaces) {
                    out.write("interface(" + i.id + "," + i.name + ",\'" + i.methods + "\').\n");
                }
            }else out.write(":- dynamic interface/3.\n");
            
            //Output classImplements
            out.write("\ntable(classImplements,[cid,iid]).\n");
            
            if(cImpements.size() > 0){
                for(ClassImplements i : cImpements){
                    out.write("classImplements(" + i.cid + "," + i.iid + ").\n");
                }
            }else out.write(":- dynamic classImplements/2.\n");
            
            //Output interfaceExtends
            out.write("\ntable(interfaceExtends,[id,idx]).\n");
            
            if(inExtends.size() > 0){
                for(InterfaceExtends i : inExtends){
                    out.write("interfaceExtends(" + i.id + "," + i.idx + ").\n");
                }
            }else out.write(":- dynamic interfaceExtends/2.\n");
            
            //Output Position
            out.write("\ntable(position,[id,x,y]).\n");
            
            if(positions.size() > 0){
                for(Position p : positions){
                    out.write("position(" + p.id + "," + p.x + "," + p.y + ").\n");
                }
            }else out.write(":- dynamic position/3.\n");
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void addPosition(Position p){
        positions.add(p);
    }
}
