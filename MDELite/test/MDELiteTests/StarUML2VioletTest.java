package MDELiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StarUML2VioletTest {
    
    private static String dir = "StarUML2Violet";
    
    public StarUML2VioletTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
     public void theWork(String filename) {
         String result = filename + ".violet";
         String[] args = {"staruml", "violet", filename};
         MDELite.Convert.main(args);
         RegTest.Utility.validate(result, "Correct/"+result,false);
     }
    
    @Test
    public void test1() {
        theWork("TestData/"+dir+"/UMLTest");
    }

    @Test
    public void test2() {
        theWork("TestData/"+dir+"/UMLTestDetails");
    }

}
