package MDELiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author troy
 */
public class Yuml2UmbrelloTest {

    private static String dir = "Yuml2Umbrello";

    public Yuml2UmbrelloTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    public void theWork(String filename) {
        String result = filename + ".xmi";
        String[] args = {"yuml", "umbrello", filename};
        MDELite.Convert.main(args);
        RegTest.Utility.validate(result, "Correct/" + result, false);
    }

    @Test
    public void test1() {
        theWork("TestData/" + dir + "/UMLTest");
    }

    @Test
    public void test2() {
        theWork("TestData/" + dir + "/UMLTestDetails");
    }
}
