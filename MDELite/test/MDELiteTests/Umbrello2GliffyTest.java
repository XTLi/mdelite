package MDELiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Umbrello2GliffyTest {
    
    private static String dir = "Umbrello2Gliffy";
    
    public Umbrello2GliffyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
     public void theWork(String filename) {
         String result = filename + ".gliffy";
         String[] args = {"umbrello", "gliffy", filename};
         MDELite.Convert.main(args);
         RegTest.Utility.validate(result, "Correct/"+result,false);
     }
    
    @Test
    public void test1() {
        theWork("TestData/"+dir+"/UMLTest");
    }

    @Test
    public void test2() {
        theWork("TestData/"+dir+"/UMLTestDetails");
    }

}
