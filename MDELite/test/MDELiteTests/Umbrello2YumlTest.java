package MDELiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Umbrello2YumlTest {
    
    private static String dir = "Umbrello2Yuml";
    
    public Umbrello2YumlTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
     public void theWork(String filename) {
         String result = filename + ".yuml";
         String[] args = {"umbrello", "yuml", filename};
         MDELite.Convert.main(args);
         RegTest.Utility.validate(result, "Correct/"+result,false);
     }
    
    @Test
    public void test1() {
        theWork("TestData/"+dir+"/UMLTest");
    }

    @Test
    public void test2() {
        theWork("TestData/"+dir+"/UMLTestDetails");
    }

}
