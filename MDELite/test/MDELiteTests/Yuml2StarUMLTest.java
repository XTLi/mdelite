package MDELiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Yuml2StarUMLTest {
    
    private static String dir = "Yuml2StarUML";
    
    public Yuml2StarUMLTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
     public void theWork(String filename) {
         String result = filename + ".xml";
         String[] args = {"yuml", "staruml", filename};
         MDELite.Convert.main(args);
         RegTest.Utility.validate(result, "Correct/"+result,false);
     }
    
    @Test
    public void test1() {
        theWork("TestData/"+dir+"/UMLTest");
    }

    @Test
    public void test2() {
        theWork("TestData/"+dir+"/UMLTestDetails");
    }

}
