package MDELiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConformTest {
    
    public ConformTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    public void theWork(String toolName, String fileName) {
        fileName = "TestData/" + toolName + "/" + fileName;
        String[] args = {toolName.toLowerCase(), fileName};
        MDELite.Conform.main(args);
    }

    @Test
    public void test1() {
        theWork("StarUML", "UMLTest");
        theWork("StarUML", "UMLTestDetails");
    }
    
    @Test
    public void test2() {
        theWork("Gliffy", "uml2");
        //theWork("Gliffy", "UMLTest");
        //theWork("Gliffy", "UMLTestDetails");
    }
    
    @Test
    public void test3() {
        theWork("Umbrello", "UMLTest");
        theWork("Umbrello", "UMLTestDetails");
    }

    @Test
    public void test4() {
        theWork("yED", "UMLTest");
        theWork("yED", "UMLTestDetails");
    }    

    @Test
    public void test5() {
        theWork("Violet", "UMLTest.class");
        theWork("Violet", "UMLTestDetails.class");
    }

    @Test
    public void test6() {
        theWork("Yuml", "UMLTest");
        theWork("Yuml", "UMLTestDetails");
    }    

}
