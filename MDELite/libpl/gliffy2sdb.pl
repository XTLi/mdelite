:-style_check(-discontiguous).

%dbase(gliffy_uml,[gliffy_class,gliffy_association,gliffy_interface,gliffy_inherits,gliffy_position]).
%table(gliffy_class,[id,"name","fields","methods"]).
%table(gliffy_association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
%table(gliffy_interface,[id,"name","methods"]).
%table(gliffy_inherits,[cid,iid]).
%table(gliffy_position, [id,x,y]).

inherit(N,N1):- gliffy_inherits(_, N, N1).
inherit(_, null).
isClass(C) :- gliffy_class(C,_,_,_).
class(I,N,F,M,S):-gliffy_class(I,N,F,M),inherit(I,S),isClass(S).
class(I,N,F,M,null):-gliffy_class(I,N,F,M), findall(S, inherit(I,S), Ss), forall(member(S1,Ss), not(isClass(S1))). 
interface(I,N,M):-gliffy_interface(I,N,M).


gliffy_to_sdb(false, 'none').
gliffy_to_sdb(true, 'arrow').

association(I1,R1,A1_2,I2,R2,A2_2):-gliffy_association(_,I1,R1,A1_1,I2,R2,A2_1), gliffy_to_sdb(A1_1, A1_2), gliffy_to_sdb(A2_1, A2_2).
classImplements(C,I):-inherit(C,I),interface(I,_,_),class(C,_,_,_,_).
interfaceExtends(C,I):-inherit(C,I),interface(I,_,_),interface(C,_,_).

position(I,X,Y):-gliffy_position(I,X,Y).