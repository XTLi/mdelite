/* file: staruml2sdb.pl */

:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
table(starinterface,[id,"name","methods"]).
table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
table(stargeneralization,[id,child,parent]).
table(starimpl,[id,client,supplier]).
table(starposition,[id,x,y]).

xlate(none,none).
xlate(composite,comp).
xlate(aggregate,agg).
xlate(arrow,arrow).

class(I,N,F,M,S):-starclass(I,N,F,M,S).
interface(I,N,M):-starinterface(I,N,M).
association(I1,R1,A1,I2,R2,A2):-starassociation(_,I1,_,R1,G1,I2,_,R2,G2),xlate(G1,A1),xlate(G2,A2).
classImplements(C,S):-starimpl(_,C,S).
interfaceExtends(C,P):-stargeneralization(_,C,P,interface).
position(I,X,Y):-starposition(I,X,Y).
