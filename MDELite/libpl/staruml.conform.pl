/* file staruml.conform.pl */

isClass(I,N):-starclass(I,N,_,_,_).
isIntf(I,N):-starinterface(I,N,_).
ename(I,N):-isIntf(I,N);isClass(I,N).

findPair(I1,I2,N):-ename(I1,N),ename(I2,N),I1 @< I2.
uniqueNames:-forall(findPair(_,_,N),isError('unique names constraint violated: ',N)).

testcycleinheritance(_,C,P):-stargeneralization(_,C,P,_),stargeneralization(_,P,C,_).
inheritancecycle:-forall(testcycleinheritance(_,C,P),isError('circular inheritance: ',[C,P])).

testcycleimpl(C,P):-starimpl(_,C,P),starimpl(_,P,C).
implcycle:-forall(testcycleimpl(C,P),isError('circular inheritance: ',[C,P])).

testdoubleend(I):-starassociation(I,_,E1,_,_,_,E2,_,_), E1=E2.
doubleends:-forall(testdoubleend(I), isError('association has two same ends',I)).

run:-uniqueNames, inheritancecycle, implcycle, doubleends.
