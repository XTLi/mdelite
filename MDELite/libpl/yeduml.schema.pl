/* yeduml.schema.pl -- YEDUML schema definition */

dbase(yeduml,[yedclass,yedinterface,yedassociation,yedImplements,yedInterfaceExtends]).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
table(yedinterface,[id,"name","methods",x,y]).
table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
table(yedImplements,[cid,iid]).
table(yedInterfaceExtends,[id,idx]).

tuple(yedclass,L):-yedclass(I,N,F,M,S,X,Y),L=[I,N,F,M,S,X,Y].
tuple(yedinterface,L):-yedinterface(I,N,M,X,Y),L=[I,N,M,X,Y].
tuple(yedassociation,L):-yedassociation(C1,R1,A1,C2,R2,A2),L=[C1,R1,A1,C2,R2,A2].
tuple(yedImplements,L):-yedImplements(C,I),L=[C,I].
tuple(yedInterfaceExtends,L):-yedInterfaceExtends(I,X),L=[I,X].

