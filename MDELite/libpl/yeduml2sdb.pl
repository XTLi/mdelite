/* yeduml2sdb.pl */

:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
table(yedinterface,[id,"name","methods",x,y]).
table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
table(yedImplements,[cid,iid]).
table(yedInterfaceExtends,[id,idx]).

%class(I,N,F,M,S):-yedclass(I,N,F,M,S,_,_).

xlate('none',none).
%xlate('white_delta','inherit').
xlate('diamond',comp).
xlate('white_diamond',agg).
xlate('short',arrow).

%class(I,N,F,M,S):-yedclass(I,N,F,M,S,_,_).
class(I,N,F,M,S):-yedclass(I,N,F,M,null,_,_), yedassociation(S,_,'white_delta',I,_,_).
class(I,N,F,M,S):-yedclass(I,N,F,M,null,_,_), yedassociation(I,_,_,S,_,'white_delta').
class(I,N,F,M,null):-yedclass(I,N,F,M,null,_,_),forall(yedassociation(_,_,A1,I,_,_),not(A1=='white_delta')),forall(yedassociation(I,_,_,_,_,A2),not(A2=='white_delta')).
interface(I,N,M):-yedinterface(I,N,M,_,_).
association(I1,R1,A1,I2,R2,A2):-yedassociation(I1,R1,C1,I2,R2,C2),xlate(C1,A1),xlate(C2,A2).
classImplements(C,I):-yedImplements(C,I).
interfaceExtends(I,X):-yedInterfaceExtends(I,X).
position(I,X,Y):-yedclass(I,_,_,_,_,X,Y).
position(I,X,Y):-yedinterface(I,_,_,X,Y).






