/* file staruml.schema.pl -- declares the StarUML schema */

dbase(staruml,[starclass,starinterface,starassociation,stargeneralization,starimpl,starposition]).

table(starclass,[id,"name","fields","methods",superid]).
table(starinterface,[id,"name","methods"]).
table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
table(stargeneralization,[id,child,parent,type]).
table(starimpl,[id,client,supplier]).
table(starposition,[id,x,y]).

tuple(starclass,L):-starclass(I,N,F,M,S),L=[I,N,F,M,S].
tuple(starinterface,L):-starinterface(I,N,M),L=[I,N,M].
tuple(starassociation,L):-starassociation(I,C1,E1,R1,A1,C2,E2,R2,A2),L=[I,C1,E1,R1,A1,C2,E2,R2,A2].
tuple(stargeneralization,L):-stargeneralization(I,C,P,T),L=[I,C,P,T].
tuple(starimpl,L):-starimpl(I,C,S),L=[I,C,S].
tuple(starposition,L):-starposition(I,X,Y),L=[I,X,Y].
