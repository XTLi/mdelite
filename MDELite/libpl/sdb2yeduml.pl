/* sdb2yeduml.pl */

table(yedclass,[id,"name","fields","methods",superid,x,y]).
table(yedinterface,[id,"name","methods",x,y]).
table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
table(yedImplements,[cid,iid]).
table(yedInterfaceExtends,[id,idx]).

yedclass(I,N,F,M,null,X,Y):-class(I,N,F,M,_),position(I,X,Y).

yedinterface(I,N,M,X,Y):-interface(I,N,M),position(I,X,Y).

yedImplements(C,I):-classImplements(C,I).

yedInterfaceExtends(I,X):-interfaceExtends(I,X).

xlate('','none').
xlate(none,'none').
xlate(comp,'diamond').
xlate(agg,'white_diamond').
xlate('inherit','white_delta').
xlate(arrow,'short').

yedassociation(I1,R1,C1,I2,R2,C2):-association(I1,R1,A1,I2,R2,A2),xlate(A1,C1),xlate(A2,C2).
yedassociation(I1,'',none,I2,'','white_delta'):-class(I1,_,_,_,I2),class(I2,_,_,_,_).

