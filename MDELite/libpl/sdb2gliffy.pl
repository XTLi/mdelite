:-style_check(-discontiguous).

%dbase(gliffy_uml,[gliffy_class,gliffy_association,gliffy_interface,gliffy_inherits,gliffy_position]).
%table(gliffy_class,[id,"name","fields","methods"]).
%table(gliffy_interface,[id,"name","methods"]).
%table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
%table(gliffy_inherits,[id,cid1,cid2]).
%table(gliffy_position,[id,x,y]). 

% Need to convert each id to an int. Do as follow:
% Make a list of all class and interface ids. I2 is I1's position in that list.
to_gliffy_id(I1, I2) :- findall(I, class(I,_,_,_,_), L1), findall(I, interface(I,_,_), L2), findall(inherit(I,J), classImplements(I,J), L3), 
							findall(inherit(I,J), interfaceExtends(I,J), L4), findall(association(I1,R1,A1,I2,R2,A2), association(I1,R1,A1,I2,R2,A2), L5),
							append(L1, L2, L12), append(L3, L4, L34), append(L5, [-9000], L56), append(L12, L34, L1234), append(L1234, L56, L), nth0(I2, L, I1).
% max_id(I) :- findall(I, class(I,_,_,_,_), L1), findall(I, interface(I,_,_), L2), append(L1, L2, L), length(L, I).

gliffy_class(I2,N,F,M):-class(I1,N,F,M,_), to_gliffy_id(I1, I2).
gliffy_interface(I2,N,M):-interface(I1,N,M), to_gliffy_id(I1, I2).

gliffy_inherits(I, ID2,IDX2):-classImplements(ID1,IDX1), to_gliffy_id(ID1, ID2), to_gliffy_id(IDX1, IDX2), to_gliffy_id(inherit(ID1,IDX1),I).
gliffy_inherits(I, IID2,IDX2):-interfaceExtends(IID1,IDX1), to_gliffy_id(IID1, IID2), to_gliffy_id(IDX1, IDX2), to_gliffy_id(inherit(IID1,IDX1),I).


sdb_to_gliffy(arrow, true).
sdb_to_gliffy(agg, true).
sdb_to_gliffy(comp, true).
sdb_to_gliffy(implem, true).
sdb_to_gliffy(inherit, true).
sdb_to_gliffy(_, false).

gliffy_association(1, I1_2,R1,A1_2,I2_2,R2,A2_2):- association(I1_1,R1,A1_1,I2_1,R2,A2_1), to_gliffy_id(I1_1, I1_2), to_gliffy_id(I2_1, I2_2), sdb_to_gliffy(A1_1, A1_2), sdb_to_gliffy(A2_1, A2_2).

gliffy_position(I2,X,Y) :- position(I1,X,Y), to_gliffy_id(I1, I2).