:-style_check(-discontiguous).

dbase(gliffy_uml,[gliffy_class,gliffy_association,gliffy_interface,gliffy_inherits,gliffy_position]).

table(gliffy_class,[id,"name","fields","methods"]).
table(gliffy_interface,[id,"name","methods"]).
table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
table(gliffy_inherits,[id,cid1,cid2]).
table(gliffy_position,[id,x,y]). 

tuple(gliffy_class, L) :- gliffy_class(I,N,F,M), L=[I,N,F,M].
tuple(gliffy_interface, L) :- gliffy_interface(I,N,M), L=[I,N,M].
tuple(gliffy_association, L) :- gliffy_association(I,I1,R1,A1,I2,C2,A2), L=[I,I1,R1,A1,I2,C2,A2].
tuple(gliffy_inherits, L) :- gliffy_inherits(I,I1,I2), L=[I,I1,I2].
tuple(gliffy_position, L) :- gliffy_position(I,X,Y), L=[I,X,Y].