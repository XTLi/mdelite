/* file: sdb2staruml.pl */

table(starclass,[id,"name","fields","methods",superid]).
table(starinterface,[id,"name","methods"]).
table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
table(stargeneralization,[id,child,parent,type]).
table(starimpl,[id,client,supplier]).
table(starposition,[id,x,y]).

count:-nb_linkval(counter, 1).
flag(TT,T):-T is (TT + 1),nb_linkval(counter,T).
asid(I,C,S):-atomic_concat(C,S,II),atomic_concat(as_,II,I).
aseid1(I):-nb_getval(counter,CC),flag(CC,C),atomic_concat(umlassociationend_,C,I).
aseid2(I):-nb_getval(counter,CC),flag(CC,C),atomic_concat(umlassociationend_,C,I).
genid(I,C,S):-atomic_concat(C,S,II),atomic_concat(gen_,II,I).
implid(I,C,S):-atomic_concat(C,S,II),atomic_concat(real_,II,I).

xlate('',none).
xlate(none,none).
xlate(comp,composite).
xlate(agg,aggregate).
xlate(arrow,arrow).

starclass(I,N,F,M,S):-class(I,N,F,M,S).
starinterface(I,N,M):-interface(I,N,M).
stargeneralization(I,C,S,class):-class(C,_,_,_,S),S \= null,genid(I,C,S).
stargeneralization(I,C,S,interface):-interfaceExtends(C,S),genid(I,C,S).
starassociation(I,C1,E1,R1,A1,C2,E2,R2,A2):-association(C1,R1,G1,C2,R2,G2),xlate(G1,A1),xlate(G2,A2),aseid1(E1),aseid2(E2),asid(I,E1,E2).
starimpl(I,C,S):-classImplements(C,S),implid(I,C,S).
starposition(I,X,Y):-position(I,X,Y).
