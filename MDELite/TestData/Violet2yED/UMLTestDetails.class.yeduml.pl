:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(classnode0,'Book','String title; int amount;','int getAmount(); String getTitle();',null,250,235).
yedclass(classnode1,'Author','String name;','',null,568,262).
yedclass(classnode2,'Address','','',null,806,262).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(classnode0,'writes *',none,classnode1,'writtenBy 1..*',none).
yedassociation(classnode1,'',none,classnode2,'livesAt 1',short).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

