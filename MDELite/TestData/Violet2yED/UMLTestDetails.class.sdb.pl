:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(classnode0,'Book','String title; int amount;','int getAmount(); String getTitle();',null).
class(classnode1,'Author','String name;','',null).
class(classnode2,'Address','','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(classnode0,'writes *','',classnode1,'writtenBy 1..*','').
association(classnode1,'','',classnode2,'livesAt 1','arrow').

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position(classnode0,250,235).
position(classnode1,568,262).
position(classnode2,806,262).

