:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(classnode0,'Realization','','',null,238,344).
yedclass(classnode1,'Superclass','','',null,502,342).
yedclass(classnode2,'Subclass','','',null,501,508).
yedclass(classnode3,'Aggregation','','',null,745,340).
yedclass(classnode4,'Component','','',null,499,164).

table(yedinterface,[id,"name","methods",x,y]).
yedinterface(interfacenode0,'SubInterface','',240,211).
yedinterface(interfacenode1,'BaseInterface','',241,68).

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(classnode0,'',none,classnode1,'',short).
yedassociation(classnode1,'',white_diamond,classnode3,'',none).
yedassociation(classnode4,'',diamond,classnode1,'',none).
yedassociation(classnode2,'',none,classnode1,'',white_delta).

table(yedImplements,[cid,iid]).
yedImplements(classnode0,interfacenode0).

table(yedInterfaceExtends,[id,idx]).
yedInterfaceExtends(interfacenode0,interfacenode1).

