:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(umlclass_19,'Book','+String title;+int amount','+int getAmount();+String getTitle()',null,190,203).
violetClass(umlclass_24,'Author','+String name','',null,520,204).
violetClass(umlclass_29,'Address','','',null,769,206).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(umlclass_19,'writes *','','classnode',umlclass_24,'writtenBy 1..*','','classnode','').
violetAssociation(umlclass_24,'','','classnode',umlclass_29,'livesAt 1','V','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

