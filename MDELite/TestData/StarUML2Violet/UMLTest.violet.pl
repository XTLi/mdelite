:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(umlclass_28,'Superclass','','',null,391,338).
violetClass(umlclass_29,'Component','','',null,388,218).
violetClass(umlclass_30,'Aggregation','','',null,630,332).
violetClass(umlclass_31,'Subclass','','',null,394,476).
violetClass(umlclass_41,'Realization','','',null,172,338).

table(violetInterface,[id,"name","methods",x,y]).
violetInterface(umlinterface_27,'SubInterface','',171,225).
violetInterface(umlinterface_39,'BaseInterface','',170,105).

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(umlclass_41,'','','classnode',umlinterface_27,'','TRIANGLE','interfacenode','DOTTED').
violetAssociation(umlinterface_27,'','','interfacenode',umlinterface_39,'','TRIANGLE','interfacenode','').
violetAssociation(umlclass_28,'','','classnode',umlclass_29,'','BLACK_DIAMOND','classnode','').
violetAssociation(umlclass_30,'','','classnode',umlclass_28,'','DIAMOND','classnode','').
violetAssociation(umlclass_41,'','','classnode',umlclass_28,'','V','classnode','').
violetAssociation(umlclass_28,'','TRIANGLE','classnode',umlclass_31,'','','classnode','').

table(violetInterfaceExtends,[id,idx]).
violetInterfaceExtends(umlinterface_27,umlinterface_39).

table(violetClassImplements,[cid,iid]).
violetClassImplements(umlclass_41,umlinterface_27).

