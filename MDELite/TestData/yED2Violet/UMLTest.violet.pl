:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(4,'Subclass','','',null,616,664).
violetClass(0,'Realization','','',null,406,467).
violetClass(1,'Superclass','','',null,616,467).
violetClass(2,'Aggregation','','',null,836,467).
violetClass(3,'Component','','',null,610,270).

table(violetInterface,[id,"name","methods",x,y]).
violetInterface(5,'SubInterface','',401,292).
violetInterface(6,'BaseInterface','',401,141).

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(0,'','','classnode',5,'','TRIANGLE','interfacenode','DOTTED').
violetAssociation(5,'','','interfacenode',6,'','TRIANGLE','interfacenode','').
violetAssociation(1,'','DIAMOND','classnode',2,'','','classnode','').
violetAssociation(3,'','BLACK_DIAMOND','classnode',1,'','','classnode','').
violetAssociation(0,'','','classnode',1,'','V','classnode','').
violetAssociation(1,'','TRIANGLE','classnode',4,'','','classnode','').

table(violetInterfaceExtends,[id,idx]).
violetInterfaceExtends(5,6).

table(violetClassImplements,[cid,iid]).
violetClassImplements(0,5).

