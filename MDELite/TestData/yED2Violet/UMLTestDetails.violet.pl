:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(0,'Book','String title; int amount;','int getAmount(); String getTitle();',null,171,269).
violetClass(1,'Author','String name;','',null,482,269).
violetClass(2,'Address','','',null,713,269).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(0,'writes *','','classnode',1,'writtenBy 1..*','','classnode','').
violetAssociation(1,'','','classnode',2,'livesAt 1','V','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

