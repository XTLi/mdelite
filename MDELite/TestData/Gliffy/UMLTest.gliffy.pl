:-style_check(-discontiguous).

dbase(gliffy_uml,[gliffy_class,gliffy_association,gliffy_interface,gliffy_inherits,gliffy_position]).
table(gliffy_class,[id,"name","fields","methods"]).
table(gliffy_association,[id, cid1,"role1",arrow1,cid2,"role2",arrow2]).
table(gliffy_interface,[id,"name","methods"]).
table(gliffy_inherits,[id, cid,iid]).
table(gliffy_position,[id,x,y]).
:- dynamic gliffy_class/4.
:- dynamic gliffy_interface/3.
:- dynamic gliffy_association/7.
:- dynamic gliffy_inherits/3.
:- dynamic gliffy_position/3.
gliffy_class(43, 'Realization', '?', '?').
gliffy_class(50, 'Superclass', '?', '?').
gliffy_class(58, 'Component', '?', '?').
gliffy_class(65, 'Subclass', '?', '?').
gliffy_class(72, 'Aggregation', '?', '?').
gliffy_class(80, 'BaseInterface', '?', '?').
gliffy_class(95, 'BaseInterface ', '?', '?').
gliffy_association(27, 43, '', false, 50, '', true).
gliffy_inherits(26, 65, 50).
gliffy_association(24, 72, '', false, 50, '', true).
gliffy_inherits(15, 43, 95).
gliffy_inherits(14, 95, 80).
gliffy_position(43, 198, 410).
gliffy_position(50, 540, 410).
gliffy_position(58, 540, 213).
gliffy_position(65, 540, 590).
gliffy_position(72, 850, 410).
gliffy_position(80, 201, 60).
gliffy_position(95, 201, 213).
