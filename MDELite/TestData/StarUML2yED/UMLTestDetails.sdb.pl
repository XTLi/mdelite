:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(umlclass_19,'Book','+String title;+int amount','+int getAmount();+String getTitle()',null).
class(umlclass_24,'Author','+String name','',null).
class(umlclass_29,'Address','','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(umlclass_19,'writes *',none,umlclass_24,'writtenBy 1..*',none).
association(umlclass_24,'',none,umlclass_29,'livesAt 1',arrow).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position(umlclass_19,190,203).
position(umlclass_24,520,204).
position(umlassociation_26,357,202).
position(umlclass_29,769,206).
position(umlassociation_30,642,204).

