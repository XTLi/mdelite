:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(umlclass_28,'Superclass','','',null).
class(umlclass_29,'Component','','',null).
class(umlclass_30,'Aggregation','','',null).
class(umlclass_31,'Subclass','','',umlclass_28).
class(umlclass_41,'Realization','','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(umlclass_28,'',none,umlclass_29,'',comp).
association(umlclass_30,'',none,umlclass_28,'',agg).
association(umlclass_41,'',none,umlclass_28,'',arrow).

table(interface,[id,"name","methods"]).
interface(umlinterface_27,'SubInterface','').
interface(umlinterface_39,'BaseInterface','').

table(classImplements,[cid,iid]).
classImplements(umlclass_41,umlinterface_27).

table(interfaceExtends,[id,idx]).
interfaceExtends(umlinterface_27,umlinterface_39).

table(position,[id,x,y]).
position(umlinterface_27,171,225).
position(umlclass_28,391,338).
position(umlclass_29,388,218).
position(umlclass_30,630,332).
position(umlclass_31,394,476).
position(umlassociation_32,387,277).
position(umlassociation_35,513,334).
position(umlgeneralization_38,392,406).
position(umlinterface_39,170,105).
position(umlgeneralization_40,169,165).
position(umlclass_41,172,338).
position(umlassociation_42,279,337).
position(umlrealization_45,170,264).

