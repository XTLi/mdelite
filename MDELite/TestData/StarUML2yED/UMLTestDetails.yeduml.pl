:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(umlclass_19,'Book','+String title;+int amount','+int getAmount();+String getTitle()',null,190,203).
yedclass(umlclass_24,'Author','+String name','',null,520,204).
yedclass(umlclass_29,'Address','','',null,769,206).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(umlclass_19,'writes *',none,umlclass_24,'writtenBy 1..*',none).
yedassociation(umlclass_24,'',none,umlclass_29,'livesAt 1',short).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

