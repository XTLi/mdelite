:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(umlclass_28,'Superclass','','',null,391,338).
yedclass(umlclass_29,'Component','','',null,388,218).
yedclass(umlclass_30,'Aggregation','','',null,630,332).
yedclass(umlclass_31,'Subclass','','',null,394,476).
yedclass(umlclass_41,'Realization','','',null,172,338).

table(yedinterface,[id,"name","methods",x,y]).
yedinterface(umlinterface_27,'SubInterface','',171,225).
yedinterface(umlinterface_39,'BaseInterface','',170,105).

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(umlclass_28,'',none,umlclass_29,'',diamond).
yedassociation(umlclass_30,'',none,umlclass_28,'',white_diamond).
yedassociation(umlclass_41,'',none,umlclass_28,'',short).
yedassociation(umlclass_31,'',none,umlclass_28,'',white_delta).

table(yedImplements,[cid,iid]).
yedImplements(umlclass_41,umlinterface_27).

table(yedInterfaceExtends,[id,idx]).
yedInterfaceExtends(umlinterface_27,umlinterface_39).

