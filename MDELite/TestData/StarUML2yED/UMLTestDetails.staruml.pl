dbase(staruml,[starclass,starinterface,starassociation,stargeneralization,starimpl]).
table(starclass,[id,"name","fields","methods",superid]).
table(starinterface,[id,"name","methods"]).
table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
table(stargeneralization,[id,child,parent,type]).
table(starimpl,[id,client,supplier]).
table(starposition,[id,x,y]).

starclass(umlclass_19, 'Book', '+String title;+int amount', '+int getAmount();+String getTitle()', null).
starclass(umlclass_24, 'Author', '+String name', '', null).
starclass(umlclass_29, 'Address', '', '', null).

:- dynamic starinterface/3.

starassociation(umlassociation_26, umlclass_19, umlassociationend_27, 'writes *', none, umlclass_24, umlassociationend_28, 'writtenBy 1..*', none).
starassociation(umlassociation_30, umlclass_24, umlassociationend_31, '', none, umlclass_29, umlassociationend_32, 'livesAt 1', arrow).

:- dynamic stargeneralization/4.

:- dynamic starimpl/3.

starposition(umlclass_19, 190, 203).
starposition(umlclass_24, 520, 204).
starposition(umlassociation_26, 357, 202).
starposition(umlclass_29, 769, 206).
starposition(umlassociation_30, 642, 204).
