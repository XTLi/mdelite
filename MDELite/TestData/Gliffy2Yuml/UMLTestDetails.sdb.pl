:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(0,'Book','String title;int amount','int getAmount();String getTitle()',null).
class(7,'Author','String name','?',null).
class(14,'Address','?','?',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(7,'livesAt 1 ',none,14,'',none).
association(0,'writes * ',none,7,'writtenBy 1..* ',none).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position(0,180,140).
position(7,490,147).
position(14,790,147).

