:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(0,'Book','String title;int amount','int getAmount();String getTitle()').
yumlClass(7,'Author','String name','?').
yumlClass(14,'Address','?','?').

table(yumlInterface,[id,"name","methods"]).
:- dynamic yumlInterface/3.

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Author','livesAt 1 ','','Address','','').
yumlAssociation('Book','writes * ','','Author','writtenBy 1..* ','').

