:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(43,'Realization','?','?').
yumlClass(65,'Subclass','?','?').
yumlClass(95,'BaseInterface ','?','?').
yumlClass(50,'Superclass','?','?').
yumlClass(58,'Component','?','?').
yumlClass(72,'Aggregation','?','?').
yumlClass(80,'BaseInterface','?','?').

table(yumlInterface,[id,"name","methods"]).
:- dynamic yumlInterface/3.

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Realization','','','Superclass','','>').
yumlAssociation('Aggregation','','','Superclass','','>').
yumlAssociation('BaseInterface ','','^','Realization','','').
yumlAssociation('Superclass','','^','Subclass','','').
yumlAssociation('BaseInterface','','^','BaseInterface ','','').

