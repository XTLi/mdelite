:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(48,'LinkedList','first','head();tail()').
yumlClass(64,'Node','value',' ').
yumlClass(89,'Collection','size','isEmpty()add(int value);remove(int value);contains(int value)').

table(yumlInterface,[id,"name","methods"]).
:- dynamic yumlInterface/3.

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Node','previous ','','Node','next ','').
yumlAssociation('LinkedList','owner ','','Node','contents ','').
yumlAssociation('Collection','','^','LinkedList','','').

