:-style_check(-discontiguous).

table(gliffy_class,[id,"name","fields","methods"]).
gliffy_class(0,'Subclass','','').
gliffy_class(1,'Realization','','').
gliffy_class(2,'Superclass','','').
gliffy_class(3,'Aggregation','','').
gliffy_class(4,'Component','','').

table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
gliffy_association(1,2,'',true,3,'',false).
gliffy_association(1,2,'',false,3,'',false).
gliffy_association(1,4,'',true,2,'',false).
gliffy_association(1,4,'',false,2,'',false).
gliffy_association(1,1,'',false,2,'',true).
gliffy_association(1,1,'',false,2,'',false).

table(gliffy_interface,[id,"name","methods"]).
gliffy_interface(5,'SubInterface','').
gliffy_interface(6,'BaseInterface','').

table(gliffy_inherits,[id,cid1,cid2]).
gliffy_inherits(7,1,5).
gliffy_inherits(8,5,6).

table(gliffy_position,[id,x,y]).
gliffy_position(1,406,467).
gliffy_position(2,616,467).
gliffy_position(3,836,467).
gliffy_position(4,610,270).
gliffy_position(0,616,664).
gliffy_position(5,401,292).
gliffy_position(6,401,141).

