table(class,[id,"name","fields","methods",superid]).
class(c0,'Book','String title; int amount;','int getAmount(); String getTitle();',null).
class(c2,'Address','','',null).
class(c1,'Author','String name;','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(c0,'writes *',none,c1,'writtenBy 1..*',none).
association(c1,'',none,c2,'livesAt 1',arrow).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position(c0,350,350).
position(c2,140,140).
position(c1,140,350).

