:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(c0,'Book','String title; int amount;','int getAmount(); String getTitle();',null,350,350).
yedclass(c2,'Address','','',null,140,140).
yedclass(c1,'Author','String name;','',null,140,350).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(c0,'writes *',none,c1,'writtenBy 1..*',none).
yedassociation(c1,'',none,c2,'livesAt 1',short).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

