:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(c6,'Subclass','','',null,560,560).
yedclass(c0,'Interface;SubInterface','','',null,140,140).
yedclass(c2,'Realization','','',null,350,140).
yedclass(c4,'Aggregation','','',null,140,560).
yedclass(c3,'Superclass','','',null,350,560).
yedclass(c1,'Interface;BaseInterface','','',null,140,350).
yedclass(c5,'Component','','',null,350,770).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(c2,'',none,c3,'',short).
yedassociation(c3,'',white_diamond,c4,'',none).
yedassociation(c5,'',diamond,c3,'',none).
yedassociation(c6,'',none,c3,'',white_delta).
yedassociation(c0,'',none,c1,'',white_delta).
yedassociation(c2,'',none,c0,'',white_delta).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

