:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(4,'Subclass','','',1).
starclass(0,'Realization','','',null).
starclass(1,'Superclass','','',null).
starclass(2,'Aggregation','','',null).
starclass(3,'Component','','',null).

table(starinterface,[id,"name","methods"]).
starinterface(5,'SubInterface','').
starinterface(6,'BaseInterface','').

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,1,umlassociationend_4,'',aggregate,2,umlassociationend_5,'',none).
starassociation(as_umlassociationend_6umlassociationend_7,3,umlassociationend_6,'',composite,1,umlassociationend_7,'',none).
starassociation(as_umlassociationend_8umlassociationend_9,0,umlassociationend_8,'',none,1,umlassociationend_9,'',arrow).

table(stargeneralization,[id,child,parent,type]).
stargeneralization(gen_41,4,1,class).
stargeneralization(gen_56,5,6,interface).

table(starimpl,[id,client,supplier]).
starimpl(real_05,0,5).

table(starposition,[id,x,y]).
starposition(0,406,467).
starposition(1,616,467).
starposition(2,836,467).
starposition(3,610,270).
starposition(4,616,664).
starposition(5,401,292).
starposition(6,401,141).

