dbase(yeduml,[yedclass,yedinterface,yedassociation,yedImplements,yedInterfaceExtends]).
table(yedclass,[id,"name","fields","methods",superid,x,y]).
table(yedinterface,[id,"name","methods",x,y]).
table(yedassociation,[cid1, "role1",arrow1,cid2,"role2",arrow2]).
table(yedImplements,[cid,iid]).
table(yedInterfaceExtends,[id,idx]).

yedclass(0, 'Book', 'String title; int amount;', 'int getAmount(); String getTitle();', null, 171, 269).
yedclass(1, 'Author', 'String name;', '', null, 482, 269).
yedclass(2, 'Address', '', '', null, 713, 269).

:- dynamic yedinterface/5.

yedassociation(0, 'writes *', none, 1, 'writtenBy 1..*', none).
yedassociation(1, '', none, 2, 'livesAt 1', short).

:- dynamic yedImplements/2.

:- dynamic yedInterfaceExtends/2.

