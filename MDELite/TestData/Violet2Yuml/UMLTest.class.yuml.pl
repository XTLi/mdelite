:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(classnode0,'Realization','','').
yumlClass(classnode1,'Superclass','','').
yumlClass(classnode2,'Subclass','','').
yumlClass(classnode3,'Aggregation','','').
yumlClass(classnode4,'Component','','').

table(yumlInterface,[id,"name","methods"]).
yumlInterface(interfacenode0,'SubInterface','').
yumlInterface(interfacenode1,'BaseInterface','').

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Realization','','','Superclass','','>').
yumlAssociation('Superclass','','<>','Aggregation','','').
yumlAssociation('Component','','++','Superclass','','').
yumlAssociation('SubInterface','','^','Realization','','').
yumlAssociation('SubInterface','','','BaseInterface','','^').
yumlAssociation('Superclass','','^','Subclass','','').

