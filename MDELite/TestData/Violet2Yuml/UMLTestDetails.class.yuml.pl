:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(classnode0,'Book','String title; int amount;','int getAmount(); String getTitle();').
yumlClass(classnode1,'Author','String name;','').
yumlClass(classnode2,'Address','','').

table(yumlInterface,[id,"name","methods"]).
:- dynamic yumlInterface/3.

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Book','writes *','','Author','writtenBy 1..*','').
yumlAssociation('Author','','','Address','livesAt 1','>').

