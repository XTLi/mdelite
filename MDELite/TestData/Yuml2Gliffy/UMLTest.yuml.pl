table(yumlClass,[id,"name","fields","methods"]).
yumlClass(c6,'Subclass','','').
yumlClass(c0,'Interface;SubInterface','','').
yumlClass(c4,'Aggregation','','').
yumlClass(c3,'Superclass','','').
yumlClass(c1,'Interface;BaseInterface','','').
yumlClass(c5,'Component','','').
yumlClass(c2,'Realization','','').

table(yumlInterface,[id,"name","methods"]).
:- dynamic yumlInterface/3.

table(yumlAssociation,["name1","role1",end1,"name2","role2",end2]).
yumlAssociation('Realization','','','Superclass','','>').
yumlAssociation('Superclass','','<>','Aggregation','','').
yumlAssociation('Component','','++','Superclass','','').
yumlAssociation('Interface;SubInterface','','^','Realization','.-','').
yumlAssociation('Interface;SubInterface','','','Interface;BaseInterface','','^').
yumlAssociation('Superclass','','^','Subclass','','').

