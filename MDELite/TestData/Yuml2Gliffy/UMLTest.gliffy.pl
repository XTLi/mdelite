:-style_check(-discontiguous).

table(gliffy_class,[id,"name","fields","methods"]).
gliffy_class(0,'Subclass','','').
gliffy_class(1,'Interface;SubInterface','','').
gliffy_class(2,'Realization','','').
gliffy_class(3,'Aggregation','','').
gliffy_class(4,'Superclass','','').
gliffy_class(5,'Interface;BaseInterface','','').
gliffy_class(6,'Component','','').

table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
gliffy_association(1,2,'',false,4,'',true).
gliffy_association(1,2,'',false,4,'',false).
gliffy_association(1,4,'',true,3,'',false).
gliffy_association(1,4,'',false,3,'',false).
gliffy_association(1,6,'',true,4,'',false).
gliffy_association(1,6,'',false,4,'',false).

table(gliffy_interface,[id,"name","methods"]).
:- dynamic gliffy_interface/3.

table(gliffy_inherits,[id,cid1,cid2]).
:- dynamic gliffy_inherits/3.

table(gliffy_position,[id,x,y]).
gliffy_position(0,560,560).
gliffy_position(1,140,140).
gliffy_position(2,350,140).
gliffy_position(3,140,560).
gliffy_position(4,350,560).
gliffy_position(5,140,350).
gliffy_position(6,350,770).

