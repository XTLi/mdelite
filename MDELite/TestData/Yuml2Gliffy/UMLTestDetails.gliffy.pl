:-style_check(-discontiguous).

table(gliffy_class,[id,"name","fields","methods"]).
gliffy_class(0,'Book','String title; int amount;','int getAmount(); String getTitle();').
gliffy_class(1,'Address','','').
gliffy_class(2,'Author','String name;','').

table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
gliffy_association(1,0,'writes *',false,2,'writtenBy 1..*',false).
gliffy_association(1,2,'',false,1,'livesAt 1',true).
gliffy_association(1,2,'',false,1,'livesAt 1',false).

table(gliffy_interface,[id,"name","methods"]).
:- dynamic gliffy_interface/3.

table(gliffy_inherits,[id,cid1,cid2]).
:- dynamic gliffy_inherits/3.

table(gliffy_position,[id,x,y]).
gliffy_position(0,350,350).
gliffy_position(1,140,140).
gliffy_position(2,140,350).

