table(class,[id,"name","fields","methods",superid]).
class(c6,'Subclass','','',c3).
class(c0,'Interface;SubInterface','','',c1).
class(c2,'Realization','','',c0).
class(c4,'Aggregation','','',null).
class(c3,'Superclass','','',null).
class(c1,'Interface;BaseInterface','','',null).
class(c5,'Component','','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(c2,'',none,c3,'',arrow).
association(c3,'',agg,c4,'',none).
association(c5,'',comp,c3,'',none).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

