:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(ia4q95e0tcnmu,'Realization','','',null,225,376).
violetClass(ixvwtf6lfpnq0,'Superclass','','',null,413,376).
violetClass(i9swx6wiwpku6,'Aggregation','','',null,604,368).
violetClass(i9zexqf0cwyec,'Subclass','','',null,416,519).
violetClass(i6nnyntkr40ln,'Component','','',null,411,231).

table(violetInterface,[id,"name","methods",x,y]).
violetInterface(iinpzikfwb7hf,'SubInterface','',216,249).
violetInterface(imd3kxvf8odhs,'BaseInterface','',212,128).

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(ia4q95e0tcnmu,'','','classnode',iinpzikfwb7hf,'','TRIANGLE','interfacenode','DOTTED').
violetAssociation(iinpzikfwb7hf,'','','interfacenode',imd3kxvf8odhs,'','TRIANGLE','interfacenode','').
violetAssociation(ia4q95e0tcnmu,'','','classnode',ixvwtf6lfpnq0,'','V','classnode','').
violetAssociation(i6nnyntkr40ln,'','BLACK_DIAMOND','classnode',ixvwtf6lfpnq0,'','','classnode','').
violetAssociation(ixvwtf6lfpnq0,'','DIAMOND','classnode',i9swx6wiwpku6,'','','classnode','').
violetAssociation(ixvwtf6lfpnq0,'','TRIANGLE','classnode',i9zexqf0cwyec,'','','classnode','').

table(violetInterfaceExtends,[id,idx]).
violetInterfaceExtends(iinpzikfwb7hf,imd3kxvf8odhs).

table(violetClassImplements,[cid,iid]).
violetClassImplements(ia4q95e0tcnmu,iinpzikfwb7hf).

