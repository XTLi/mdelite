:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(iljxvsr7pxm81,'Book',' title; amount','getAmount();getTitle()',null,143,259).
violetClass(iwayd76xibadl,'Author',' name','',null,407,270).
violetClass(iizxxqyodz0bo,'Address','','',null,624,274).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(iljxvsr7pxm81,'writes *','','classnode',iwayd76xibadl,'writtenBy 1..*','','classnode','').
violetAssociation(iwayd76xibadl,'','','classnode',iizxxqyodz0bo,'livesAt 1','V','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

