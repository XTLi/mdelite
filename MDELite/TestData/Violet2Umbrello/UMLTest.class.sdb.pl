:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(classnode0,'Realization','','',null).
class(classnode1,'Superclass','','',null).
class(classnode2,'Subclass','','',classnode1).
class(classnode3,'Aggregation','','',null).
class(classnode4,'Component','','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(classnode0,'','',classnode1,'','arrow').
association(classnode1,'',agg,classnode3,'','').
association(classnode4,'',comp,classnode1,'','').

table(interface,[id,"name","methods"]).
interface(interfacenode0,'SubInterface','').
interface(interfacenode1,'BaseInterface','').

table(classImplements,[cid,iid]).
classImplements(classnode0,interfacenode0).

table(interfaceExtends,[id,idx]).
interfaceExtends(interfacenode0,interfacenode1).

table(position,[id,x,y]).
position(classnode0,238,344).
position(classnode1,502,342).
position(classnode2,501,508).
position(classnode3,745,340).
position(classnode4,499,164).
position(interfacenode0,240,211).
position(interfacenode1,241,68).

