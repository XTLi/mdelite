:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(4,'Subclass','','').
yumlClass(0,'Realization','','').
yumlClass(1,'Superclass','','').
yumlClass(2,'Aggregation','','').
yumlClass(3,'Component','','').

table(yumlInterface,[id,"name","methods"]).
yumlInterface(5,'SubInterface','').
yumlInterface(6,'BaseInterface','').

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Superclass','','<>','Aggregation','','').
yumlAssociation('Component','','++','Superclass','','').
yumlAssociation('Realization','','','Superclass','','>').
yumlAssociation('SubInterface','','^','Realization','','').
yumlAssociation('SubInterface','','','BaseInterface','','^').
yumlAssociation('Superclass','','^','Subclass','','').

