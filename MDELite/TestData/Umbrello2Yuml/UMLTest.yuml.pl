:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(ia4q95e0tcnmu,'Realization','','').
yumlClass(ixvwtf6lfpnq0,'Superclass','','').
yumlClass(i9swx6wiwpku6,'Aggregation','','').
yumlClass(i9zexqf0cwyec,'Subclass','','').
yumlClass(i6nnyntkr40ln,'Component','','').

table(yumlInterface,[id,"name","methods"]).
yumlInterface(iinpzikfwb7hf,'SubInterface','').
yumlInterface(imd3kxvf8odhs,'BaseInterface','').

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Realization','','','Superclass','','>').
yumlAssociation('Component','','++','Superclass','','').
yumlAssociation('Superclass','','<>','Aggregation','','').
yumlAssociation('SubInterface','','^','Realization','','').
yumlAssociation('SubInterface','','','BaseInterface','','^').
yumlAssociation('Superclass','','^','Subclass','','').

