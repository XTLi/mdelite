:-style_check(-discontiguous).

dbase(sdb,[class,association,interface,classImplements,interfaceExtends,position]).

table(class,[id,"name","fields","methods",superid]).
class('iljxvsr7pxm81','Book',' title; amount','getAmount();getTitle()','null').
class('iwayd76xibadl','Author',' name','','null').
class('iizxxqyodz0bo','Address','','','null').

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association('iljxvsr7pxm81','writes *',none,'iwayd76xibadl','writtenBy 1..*',none).
association('iwayd76xibadl','',none,'iizxxqyodz0bo','livesAt 1',arrow).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position('iljxvsr7pxm81',143,259).
position('iwayd76xibadl',407,270).
position('iizxxqyodz0bo',624,274).
