:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(iljxvsr7pxm81,'Book',' title; amount','getAmount();getTitle()').
yumlClass(iwayd76xibadl,'Author',' name','').
yumlClass(iizxxqyodz0bo,'Address','','').

table(yumlInterface,[id,"name","methods"]).
:- dynamic yumlInterface/3.

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Book','writes *','','Author','writtenBy 1..*','').
yumlAssociation('Author','','','Address','livesAt 1','>').

