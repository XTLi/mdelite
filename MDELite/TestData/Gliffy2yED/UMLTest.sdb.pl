:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(43,'Realization','?','?',95).
class(65,'Subclass','?','?',50).
class(95,'BaseInterface ','?','?',80).
class(50,'Superclass','?','?',null).
class(58,'Component','?','?',null).
class(72,'Aggregation','?','?',null).
class(80,'BaseInterface','?','?',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(43,'',none,50,'',arrow).
association(72,'',none,50,'',arrow).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position(43,198,410).
position(50,540,410).
position(58,540,213).
position(65,540,590).
position(72,850,410).
position(80,201,60).
position(95,201,213).

