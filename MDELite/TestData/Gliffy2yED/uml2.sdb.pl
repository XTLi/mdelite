:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(48,'LinkedList','first','head();tail()',89).
class(64,'Node','value',' ',null).
class(89,'Collection','size','isEmpty()add(int value);remove(int value);contains(int value)',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(64,'previous ',none,64,'next ',none).
association(48,'owner ',none,64,'contents ',none).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position(48,120,220).
position(64,500,204).
position(89,111,41).

