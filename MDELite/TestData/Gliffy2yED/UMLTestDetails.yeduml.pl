:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(0,'Book','String title;int amount','int getAmount();String getTitle()',null,180,140).
yedclass(7,'Author','String name','?',null,490,147).
yedclass(14,'Address','?','?',null,790,147).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(7,'livesAt 1 ',none,14,'',none).
yedassociation(0,'writes * ',none,7,'writtenBy 1..* ',none).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

