:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(43,'Realization','?','?',null,198,410).
yedclass(65,'Subclass','?','?',null,540,590).
yedclass(95,'BaseInterface ','?','?',null,201,213).
yedclass(50,'Superclass','?','?',null,540,410).
yedclass(58,'Component','?','?',null,540,213).
yedclass(72,'Aggregation','?','?',null,850,410).
yedclass(80,'BaseInterface','?','?',null,201,60).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(43,'',none,50,'',short).
yedassociation(72,'',none,50,'',short).
yedassociation(43,'',none,95,'',white_delta).
yedassociation(65,'',none,50,'',white_delta).
yedassociation(95,'',none,80,'',white_delta).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

