:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(48,'LinkedList','first','head();tail()',null,120,220).
yedclass(64,'Node','value',' ',null,500,204).
yedclass(89,'Collection','size','isEmpty()add(int value);remove(int value);contains(int value)',null,111,41).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(64,'previous ',none,64,'next ',none).
yedassociation(48,'owner ',none,64,'contents ',none).
yedassociation(48,'',none,89,'',white_delta).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

