:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(iljxvsr7pxm81,'Book',' title; amount','getAmount();getTitle()',null).
starclass(iwayd76xibadl,'Author',' name','',null).
starclass(iizxxqyodz0bo,'Address','','',null).

table(starinterface,[id,"name","methods"]).
:- dynamic starinterface/3.

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,iljxvsr7pxm81,umlassociationend_4,'writes *',none,iwayd76xibadl,umlassociationend_5,'writtenBy 1..*',none).
starassociation(as_umlassociationend_6umlassociationend_7,iwayd76xibadl,umlassociationend_6,'',none,iizxxqyodz0bo,umlassociationend_7,'livesAt 1',arrow).

table(stargeneralization,[id,child,parent,type]).
:- dynamic stargeneralization/4.

table(starimpl,[id,client,supplier]).
:- dynamic starimpl/3.

table(starposition,[id,x,y]).
starposition(iljxvsr7pxm81,143,259).
starposition(iwayd76xibadl,407,270).
starposition(iizxxqyodz0bo,624,274).

