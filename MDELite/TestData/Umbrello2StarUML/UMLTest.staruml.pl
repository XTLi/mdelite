:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(ia4q95e0tcnmu,'Realization','','',null).
starclass(ixvwtf6lfpnq0,'Superclass','','',null).
starclass(i9swx6wiwpku6,'Aggregation','','',null).
starclass(i9zexqf0cwyec,'Subclass','','',ixvwtf6lfpnq0).
starclass(i6nnyntkr40ln,'Component','','',null).

table(starinterface,[id,"name","methods"]).
starinterface(iinpzikfwb7hf,'SubInterface','').
starinterface(imd3kxvf8odhs,'BaseInterface','').

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,ia4q95e0tcnmu,umlassociationend_4,'',none,ixvwtf6lfpnq0,umlassociationend_5,'',arrow).
starassociation(as_umlassociationend_6umlassociationend_7,i6nnyntkr40ln,umlassociationend_6,'',composite,ixvwtf6lfpnq0,umlassociationend_7,'',none).
starassociation(as_umlassociationend_8umlassociationend_9,ixvwtf6lfpnq0,umlassociationend_8,'',aggregate,i9swx6wiwpku6,umlassociationend_9,'',none).

table(stargeneralization,[id,child,parent,type]).
stargeneralization(gen_i9zexqf0cwyecixvwtf6lfpnq0,i9zexqf0cwyec,ixvwtf6lfpnq0,class).
stargeneralization(gen_iinpzikfwb7hfimd3kxvf8odhs,iinpzikfwb7hf,imd3kxvf8odhs,interface).

table(starimpl,[id,client,supplier]).
starimpl(real_ia4q95e0tcnmuiinpzikfwb7hf,ia4q95e0tcnmu,iinpzikfwb7hf).

table(starposition,[id,x,y]).
starposition(ia4q95e0tcnmu,225,376).
starposition(ixvwtf6lfpnq0,413,376).
starposition(i9swx6wiwpku6,604,368).
starposition(i9zexqf0cwyec,416,519).
starposition(i6nnyntkr40ln,411,231).
starposition(iinpzikfwb7hf,216,249).
starposition(imd3kxvf8odhs,212,128).

