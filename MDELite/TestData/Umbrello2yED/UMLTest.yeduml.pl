:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(ia4q95e0tcnmu,'Realization','','',null,225,376).
yedclass(ixvwtf6lfpnq0,'Superclass','','',null,413,376).
yedclass(i9swx6wiwpku6,'Aggregation','','',null,604,368).
yedclass(i9zexqf0cwyec,'Subclass','','',null,416,519).
yedclass(i6nnyntkr40ln,'Component','','',null,411,231).

table(yedinterface,[id,"name","methods",x,y]).
yedinterface(iinpzikfwb7hf,'SubInterface','',216,249).
yedinterface(imd3kxvf8odhs,'BaseInterface','',212,128).

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(ia4q95e0tcnmu,'',none,ixvwtf6lfpnq0,'',short).
yedassociation(i6nnyntkr40ln,'',diamond,ixvwtf6lfpnq0,'',none).
yedassociation(ixvwtf6lfpnq0,'',white_diamond,i9swx6wiwpku6,'',none).
yedassociation(i9zexqf0cwyec,'',none,ixvwtf6lfpnq0,'',white_delta).

table(yedImplements,[cid,iid]).
yedImplements(ia4q95e0tcnmu,iinpzikfwb7hf).

table(yedInterfaceExtends,[id,idx]).
yedInterfaceExtends(iinpzikfwb7hf,imd3kxvf8odhs).

