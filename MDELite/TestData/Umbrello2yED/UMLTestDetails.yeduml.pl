:-style_check(-discontiguous).

table(yedclass,[id,"name","fields","methods",superid,x,y]).
yedclass(iljxvsr7pxm81,'Book',' title; amount','getAmount();getTitle()',null,143,259).
yedclass(iwayd76xibadl,'Author',' name','',null,407,270).
yedclass(iizxxqyodz0bo,'Address','','',null,624,274).

table(yedinterface,[id,"name","methods",x,y]).
:- dynamic yedinterface/5.

table(yedassociation,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
yedassociation(iljxvsr7pxm81,'writes *',none,iwayd76xibadl,'writtenBy 1..*',none).
yedassociation(iwayd76xibadl,'',none,iizxxqyodz0bo,'livesAt 1',short).

table(yedImplements,[cid,iid]).
:- dynamic yedImplements/2.

table(yedInterfaceExtends,[id,idx]).
:- dynamic yedInterfaceExtends/2.

