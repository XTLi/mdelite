:-style_check(-discontiguous).

table(gliffy_class,[id,"name","fields","methods"]).
gliffy_class(0,'Superclass','','').
gliffy_class(1,'Component','','').
gliffy_class(2,'Aggregation','','').
gliffy_class(3,'Subclass','','').
gliffy_class(4,'Realization','','').

table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
gliffy_association(1,0,'',false,1,'',true).
gliffy_association(1,0,'',false,1,'',false).
gliffy_association(1,2,'',false,0,'',true).
gliffy_association(1,2,'',false,0,'',false).
gliffy_association(1,4,'',false,0,'',true).
gliffy_association(1,4,'',false,0,'',false).

table(gliffy_interface,[id,"name","methods"]).
gliffy_interface(5,'SubInterface','').
gliffy_interface(6,'BaseInterface','').

table(gliffy_inherits,[id,cid1,cid2]).
gliffy_inherits(7,4,5).
gliffy_inherits(8,5,6).

table(gliffy_position,[id,x,y]).
gliffy_position(5,171,225).
gliffy_position(0,391,338).
gliffy_position(1,388,218).
gliffy_position(2,630,332).
gliffy_position(3,394,476).
gliffy_position(6,170,105).
gliffy_position(4,172,338).

