:-style_check(-discontiguous).

dbase(gliffy_uml,[gliffy_class,gliffy_association,gliffy_interface,gliffy_inherits,gliffy_position]).
table(gliffy_class,[id,"name","fields","methods"]).
table(gliffy_association,[id, cid1,"role1",arrow1,cid2,"role2",arrow2]).
table(gliffy_interface,[id,"name","methods"]).
table(gliffy_inherits,[id, cid,iid]).
table(gliffy_position,[id,x,y]).
:- dynamic gliffy_class/4.
:- dynamic gliffy_interface/3.
:- dynamic gliffy_association/7.
:- dynamic gliffy_inherits/3.
:- dynamic gliffy_position/3.
gliffy_class(0, 'Book', 'String title;int amount', 'int getAmount();String getTitle()').
gliffy_class(7, 'Author', 'String name', '?').
gliffy_class(14, 'Address', '?', '?').
gliffy_association(24, 7, 'livesAt 1 ', false, 14, '', false).
gliffy_association(21, 0, 'writes * ', false, 7, 'writtenBy 1..* ', false).
gliffy_position(0, 180, 140).
gliffy_position(7, 490, 147).
gliffy_position(14, 790, 147).
