:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(48,'LinkedList','first','head();tail()',89).
starclass(64,'Node','value',' ',null).
starclass(89,'Collection','size','isEmpty()add(int value);remove(int value);contains(int value)',null).

table(starinterface,[id,"name","methods"]).
:- dynamic starinterface/3.

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,64,umlassociationend_4,'previous ',none,64,umlassociationend_5,'next ',none).
starassociation(as_umlassociationend_6umlassociationend_7,48,umlassociationend_6,'owner ',none,64,umlassociationend_7,'contents ',none).

table(stargeneralization,[id,child,parent,type]).
stargeneralization(gen_4889,48,89,class).

table(starimpl,[id,client,supplier]).
:- dynamic starimpl/3.

table(starposition,[id,x,y]).
starposition(48,120,220).
starposition(64,500,204).
starposition(89,111,41).

