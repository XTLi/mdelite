:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(43,'Realization','?','?',95).
starclass(65,'Subclass','?','?',50).
starclass(95,'BaseInterface ','?','?',80).
starclass(50,'Superclass','?','?',null).
starclass(58,'Component','?','?',null).
starclass(72,'Aggregation','?','?',null).
starclass(80,'BaseInterface','?','?',null).

table(starinterface,[id,"name","methods"]).
:- dynamic starinterface/3.

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,43,umlassociationend_4,'',none,50,umlassociationend_5,'',arrow).
starassociation(as_umlassociationend_6umlassociationend_7,72,umlassociationend_6,'',none,50,umlassociationend_7,'',arrow).

table(stargeneralization,[id,child,parent,type]).
stargeneralization(gen_4395,43,95,class).
stargeneralization(gen_6550,65,50,class).
stargeneralization(gen_9580,95,80,class).

table(starimpl,[id,client,supplier]).
:- dynamic starimpl/3.

table(starposition,[id,x,y]).
starposition(43,198,410).
starposition(50,540,410).
starposition(58,540,213).
starposition(65,540,590).
starposition(72,850,410).
starposition(80,201,60).
starposition(95,201,213).

