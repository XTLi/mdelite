:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(0,'Book','String title;int amount','int getAmount();String getTitle()',null).
starclass(7,'Author','String name','?',null).
starclass(14,'Address','?','?',null).

table(starinterface,[id,"name","methods"]).
:- dynamic starinterface/3.

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,7,umlassociationend_4,'livesAt 1 ',none,14,umlassociationend_5,'',none).
starassociation(as_umlassociationend_6umlassociationend_7,0,umlassociationend_6,'writes * ',none,7,umlassociationend_7,'writtenBy 1..* ',none).

table(stargeneralization,[id,child,parent,type]).
:- dynamic stargeneralization/4.

table(starimpl,[id,client,supplier]).
:- dynamic starimpl/3.

table(starposition,[id,x,y]).
starposition(0,180,140).
starposition(7,490,147).
starposition(14,790,147).

