:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(c6,'Subclass','','',null,560,560).
violetClass(c0,'Interface;SubInterface','','',null,140,140).
violetClass(c2,'Realization','','',null,350,140).
violetClass(c4,'Aggregation','','',null,140,560).
violetClass(c3,'Superclass','','',null,350,560).
violetClass(c1,'Interface;BaseInterface','','',null,140,350).
violetClass(c5,'Component','','',null,350,770).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(c2,'','','classnode',c3,'','V','classnode','').
violetAssociation(c3,'','DIAMOND','classnode',c4,'','','classnode','').
violetAssociation(c5,'','BLACK_DIAMOND','classnode',c3,'','','classnode','').
violetAssociation(c3,'','TRIANGLE','classnode',c6,'','','classnode','').
violetAssociation(c1,'','TRIANGLE','classnode',c0,'','','classnode','').
violetAssociation(c0,'','TRIANGLE','classnode',c2,'','','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

