:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(c0,'Book','String title; int amount;','int getAmount(); String getTitle();',null,350,350).
violetClass(c2,'Address','','',null,140,140).
violetClass(c1,'Author','String name;','',null,140,350).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(c0,'writes *','','classnode',c1,'writtenBy 1..*','','classnode','').
violetAssociation(c1,'','','classnode',c2,'livesAt 1','V','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

