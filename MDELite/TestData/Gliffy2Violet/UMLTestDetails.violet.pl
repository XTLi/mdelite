:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(0,'Book','String title;int amount','int getAmount();String getTitle()',null,180,140).
violetClass(7,'Author','String name','?',null,490,147).
violetClass(14,'Address','?','?',null,790,147).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(7,'livesAt 1 ','','classnode',14,'','','classnode','').
violetAssociation(0,'writes * ','','classnode',7,'writtenBy 1..* ','','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

