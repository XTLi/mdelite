:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(43,'Realization','?','?',null,198,410).
violetClass(65,'Subclass','?','?',null,540,590).
violetClass(95,'BaseInterface ','?','?',null,201,213).
violetClass(50,'Superclass','?','?',null,540,410).
violetClass(58,'Component','?','?',null,540,213).
violetClass(72,'Aggregation','?','?',null,850,410).
violetClass(80,'BaseInterface','?','?',null,201,60).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(43,'','','classnode',50,'','V','classnode','').
violetAssociation(72,'','','classnode',50,'','V','classnode','').
violetAssociation(95,'','TRIANGLE','classnode',43,'','','classnode','').
violetAssociation(50,'','TRIANGLE','classnode',65,'','','classnode','').
violetAssociation(80,'','TRIANGLE','classnode',95,'','','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

