:-style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(48,'LinkedList','first','head();tail()',null,120,220).
violetClass(64,'Node','value',' ',null,500,204).
violetClass(89,'Collection','size','isEmpty()add(int value);remove(int value);contains(int value)',null,111,41).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(64,'previous ','','classnode',64,'next ','','classnode','').
violetAssociation(48,'owner ','','classnode',64,'contents ','','classnode','').
violetAssociation(89,'','TRIANGLE','classnode',48,'','','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.

