:- style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(classnode0,'Realization','','',null,238,344).
violetClass(classnode1,'Superclass','','',null,502,342).
violetClass(classnode2,'Subclass','','',null,501,508).
violetClass(classnode3,'Aggregation','','',null,745,340).
violetClass(classnode4,'Component','','',null,499,164).

table(violetInterface,[id,"name","methods",x,y]).
violetInterface(interfacenode0,'SubInterface','',240,211).
violetInterface(interfacenode1,'BaseInterface','',241,68).

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetInterfaceExtends(interfacenode0,interfacenode1).
violetAssociation(interfacenode0,'','','interfacenode',interfacenode1,'','TRIANGLE','interfacenode','').
violetClassImplements(classnode0,interfacenode0).
violetAssociation(classnode0,'','','classnode',interfacenode0,'','TRIANGLE','interfacenode','DOTTED').
violetAssociation(classnode0,'','','classnode',classnode1,'','V','classnode','').
violetAssociation(classnode2,'','','classnode',classnode1,'','TRIANGLE','classnode','').
violetAssociation(classnode1,'','DIAMOND','classnode',classnode3,'','','classnode','').
violetAssociation(classnode4,'','BLACK_DIAMOND','classnode',classnode1,'','','classnode','').

table(violetInterfaceExtends,[id,idx]).

table(violetClassImplements,[cid,iid]).
