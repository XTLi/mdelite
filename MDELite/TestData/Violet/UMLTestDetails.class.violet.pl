:- style_check(-discontiguous).

table(violetClass,[id,"name","fields","methods",superid,x,y]).
violetClass(classnode0,'Book','String title; int amount;','int getAmount(); String getTitle();',null,250,235).
violetClass(classnode1,'Author','String name;','',null,568,262).
violetClass(classnode2,'Address','','',null,806,262).

table(violetInterface,[id,"name","methods",x,y]).
:- dynamic violetInterface/5.

table(violetAssociation,[cid1,"role1",arrow1,type1,cid2,"role2",arrow2,type2,lineStyle]).
violetAssociation(classnode0,'writes *','','classnode',classnode1,'writtenBy 1..*','','classnode','').
violetAssociation(classnode1,'','','classnode',classnode2,'livesAt 1','V','classnode','').

table(violetInterfaceExtends,[id,idx]).
:- dynamic violetInterfaceExtends/2.

table(violetClassImplements,[cid,iid]).
:- dynamic violetClassImplements/2.
