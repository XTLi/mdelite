:-style_check(-discontiguous).

dbase(gliffy_uml,[gliffy_class,gliffy_association,gliffy_interface,gliffy_inherits,gliffy_position]).
table(gliffy_class,[id,"name","fields","methods"]).
table(gliffy_association,[id, cid1,"role1",arrow1,cid2,"role2",arrow2]).
table(gliffy_interface,[id,"name","methods"]).
table(gliffy_inherits,[id, cid,iid]).
table(gliffy_position,[id,x,y]).
:- dynamic gliffy_class/4.
:- dynamic gliffy_interface/3.
:- dynamic gliffy_association/7.
:- dynamic gliffy_inherits/3.
:- dynamic gliffy_position/3.
gliffy_class(48, 'LinkedList', 'first', 'head();tail()').
gliffy_class(64, 'Node', 'value', ' ').
gliffy_class(89, 'Collection', 'size', 'isEmpty()add(int value);remove(int value);contains(int value)').
gliffy_inherits(96, 48, 89).
gliffy_association(84, 64, 'previous ', false, 64, 'next ', false).
gliffy_association(79, 48, 'owner ', false, 64, 'contents ', false).
gliffy_position(48, 120, 220).
gliffy_position(64, 500, 204).
gliffy_position(89, 111, 41).
