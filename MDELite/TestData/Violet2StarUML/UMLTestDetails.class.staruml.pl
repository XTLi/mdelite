:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(classnode0,'Book','String title; int amount;','int getAmount(); String getTitle();',null).
starclass(classnode1,'Author','String name;','',null).
starclass(classnode2,'Address','','',null).

table(starinterface,[id,"name","methods"]).
:- dynamic starinterface/3.

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,classnode0,umlassociationend_4,'writes *',none,classnode1,umlassociationend_5,'writtenBy 1..*',none).
starassociation(as_umlassociationend_6umlassociationend_7,classnode1,umlassociationend_6,'',none,classnode2,umlassociationend_7,'livesAt 1',arrow).

table(stargeneralization,[id,child,parent,type]).
:- dynamic stargeneralization/4.

table(starimpl,[id,client,supplier]).
:- dynamic starimpl/3.

table(starposition,[id,x,y]).
starposition(classnode0,250,235).
starposition(classnode1,568,262).
starposition(classnode2,806,262).

