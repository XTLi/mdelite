:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(classnode0,'Realization','','',null).
starclass(classnode1,'Superclass','','',null).
starclass(classnode2,'Subclass','','',classnode1).
starclass(classnode3,'Aggregation','','',null).
starclass(classnode4,'Component','','',null).

table(starinterface,[id,"name","methods"]).
starinterface(interfacenode0,'SubInterface','').
starinterface(interfacenode1,'BaseInterface','').

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,classnode0,umlassociationend_4,'',none,classnode1,umlassociationend_5,'',arrow).
starassociation(as_umlassociationend_6umlassociationend_7,classnode1,umlassociationend_6,'',aggregate,classnode3,umlassociationend_7,'',none).
starassociation(as_umlassociationend_8umlassociationend_9,classnode4,umlassociationend_8,'',composite,classnode1,umlassociationend_9,'',none).

table(stargeneralization,[id,child,parent,type]).
stargeneralization(gen_classnode2classnode1,classnode2,classnode1,class).
stargeneralization(gen_interfacenode0interfacenode1,interfacenode0,interfacenode1,interface).

table(starimpl,[id,client,supplier]).
starimpl(real_classnode0interfacenode0,classnode0,interfacenode0).

table(starposition,[id,x,y]).
starposition(classnode0,238,344).
starposition(classnode1,502,342).
starposition(classnode2,501,508).
starposition(classnode3,745,340).
starposition(classnode4,499,164).
starposition(interfacenode0,240,211).
starposition(interfacenode1,241,68).

