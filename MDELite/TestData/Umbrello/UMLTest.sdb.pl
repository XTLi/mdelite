:-style_check(-discontiguous).

dbase(sdb,[class,association,interface,classImplements,interfaceExtends,position]).

table(class,[id,"name","fields","methods",superid]).
class('ia4q95e0tcnmu','Realization','','','null').
class('ixvwtf6lfpnq0','Superclass','','','null').
class('i9swx6wiwpku6','Aggregation','','','null').
class('i9zexqf0cwyec','Subclass','','','ixvwtf6lfpnq0').
class('i6nnyntkr40ln','Component','','','null').

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association('ia4q95e0tcnmu','',none,'ixvwtf6lfpnq0','',arrow).
association('i6nnyntkr40ln','',comp,'ixvwtf6lfpnq0','',none).
association('ixvwtf6lfpnq0','',agg,'i9swx6wiwpku6','',none).

table(interface,[id,"name","methods"]).
interface('iinpzikfwb7hf','SubInterface','').
interface('imd3kxvf8odhs','BaseInterface','').

table(classImplements,[cid,iid]).
classImplements('ia4q95e0tcnmu','iinpzikfwb7hf').

table(interfaceExtends,[id,idx]).
interfaceExtends('iinpzikfwb7hf','imd3kxvf8odhs').

table(position,[id,x,y]).
position('ia4q95e0tcnmu',225,376).
position('ixvwtf6lfpnq0',413,376).
position('i9swx6wiwpku6',604,368).
position('i9zexqf0cwyec',416,519).
position('i6nnyntkr40ln',411,231).
position('iinpzikfwb7hf',216,249).
position('imd3kxvf8odhs',212,128).
