:-style_check(-discontiguous).

table(yumlClass,[id,"name","fields","methods"]).
yumlClass(umlclass_28,'Superclass','','').
yumlClass(umlclass_29,'Component','','').
yumlClass(umlclass_30,'Aggregation','','').
yumlClass(umlclass_31,'Subclass','','').
yumlClass(umlclass_41,'Realization','','').

table(yumlInterface,[id,"name","methods"]).
yumlInterface(umlinterface_27,'SubInterface','').
yumlInterface(umlinterface_39,'BaseInterface','').

table(yumlAssociation,["name1","role1","end1","name2","role2","end2"]).
yumlAssociation('Superclass','','','Component','','++').
yumlAssociation('Aggregation','','','Superclass','','<>').
yumlAssociation('Realization','','','Superclass','','>').
yumlAssociation('SubInterface','','^','Realization','','').
yumlAssociation('SubInterface','','','BaseInterface','','^').
yumlAssociation('Superclass','','^','Subclass','','').

