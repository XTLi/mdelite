dbase(staruml,[starclass,starinterface,starassociation,stargeneralization,starimpl]).
table(starclass,[id,"name","fields","methods",superid]).
table(starinterface,[id,"name","methods"]).
table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
table(stargeneralization,[id,child,parent,type]).
table(starimpl,[id,client,supplier]).
table(starposition,[id,x,y]).

starclass(umlclass_28, 'Superclass', '', '', null).
starclass(umlclass_29, 'Component', '', '', null).
starclass(umlclass_30, 'Aggregation', '', '', null).
starclass(umlclass_31, 'Subclass', '', '', umlclass_28).
starclass(umlclass_41, 'Realization', '', '', null).

starinterface(umlinterface_27, 'SubInterface', '').
starinterface(umlinterface_39, 'BaseInterface', '').

starassociation(umlassociation_32, umlclass_28, umlassociationend_33, '', none, umlclass_29, umlassociationend_34, '', composite).
starassociation(umlassociation_35, umlclass_30, umlassociationend_36, '', none, umlclass_28, umlassociationend_37, '', aggregate).
starassociation(umlassociation_42, umlclass_41, umlassociationend_43, '', none, umlclass_28, umlassociationend_44, '', arrow).

stargeneralization(umlgeneralization_38, umlclass_31, umlclass_28,class).
stargeneralization(umlgeneralization_40, umlinterface_27, umlinterface_39,interface).

starimpl(umlrealization_45, umlclass_41, umlinterface_27).

starposition(umlinterface_27, 171, 225).
starposition(umlclass_28, 391, 338).
starposition(umlclass_29, 388, 218).
starposition(umlclass_30, 630, 332).
starposition(umlclass_31, 394, 476).
starposition(umlassociation_32, 387, 277).
starposition(umlassociation_35, 513, 334).
starposition(umlgeneralization_38, 392, 406).
starposition(umlinterface_39, 170, 105).
starposition(umlgeneralization_40, 169, 165).
starposition(umlclass_41, 172, 338).
starposition(umlassociation_42, 279, 337).
starposition(umlrealization_45, 170, 264).
