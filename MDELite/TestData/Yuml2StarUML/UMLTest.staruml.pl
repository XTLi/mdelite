:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(c6,'Subclass','','',c3).
starclass(c0,'Interface;SubInterface','','',c1).
starclass(c2,'Realization','','',c0).
starclass(c4,'Aggregation','','',null).
starclass(c3,'Superclass','','',null).
starclass(c1,'Interface;BaseInterface','','',null).
starclass(c5,'Component','','',null).

table(starinterface,[id,"name","methods"]).
:- dynamic starinterface/3.

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,c2,umlassociationend_4,'',none,c3,umlassociationend_5,'',arrow).
starassociation(as_umlassociationend_6umlassociationend_7,c3,umlassociationend_6,'',aggregate,c4,umlassociationend_7,'',none).
starassociation(as_umlassociationend_8umlassociationend_9,c5,umlassociationend_8,'',composite,c3,umlassociationend_9,'',none).

table(stargeneralization,[id,child,parent,type]).
stargeneralization(gen_c6c3,c6,c3,class).
stargeneralization(gen_c0c1,c0,c1,class).
stargeneralization(gen_c2c0,c2,c0,class).

table(starimpl,[id,client,supplier]).
:- dynamic starimpl/3.

table(starposition,[id,x,y]).
starposition(c6,560,560).
starposition(c0,140,140).
starposition(c2,350,140).
starposition(c4,140,560).
starposition(c3,350,560).
starposition(c1,140,350).
starposition(c5,350,770).

