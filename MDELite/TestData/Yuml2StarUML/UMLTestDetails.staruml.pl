:-style_check(-discontiguous).

table(starclass,[id,"name","fields","methods",superid]).
starclass(c0,'Book','String title; int amount;','int getAmount(); String getTitle();',null).
starclass(c2,'Address','','',null).
starclass(c1,'Author','String name;','',null).

table(starinterface,[id,"name","methods"]).
:- dynamic starinterface/3.

table(starassociation,[id,cid1,eid1,"role1",aggr1,cid2,eid2,"role2",aggr2]).
starassociation(as_umlassociationend_4umlassociationend_5,c0,umlassociationend_4,'writes *',none,c1,umlassociationend_5,'writtenBy 1..*',none).
starassociation(as_umlassociationend_6umlassociationend_7,c1,umlassociationend_6,'',none,c2,umlassociationend_7,'livesAt 1',arrow).

table(stargeneralization,[id,child,parent,type]).
:- dynamic stargeneralization/4.

table(starimpl,[id,client,supplier]).
:- dynamic starimpl/3.

table(starposition,[id,x,y]).
starposition(c0,350,350).
starposition(c2,140,140).
starposition(c1,140,350).

