:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(0,'Book','String title; int amount;','int getAmount(); String getTitle();',null).
class(1,'Author','String name;','',null).
class(2,'Address','','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(0,'writes *',none,1,'writtenBy 1..*',none).
association(1,'',none,2,'livesAt 1',arrow).

table(interface,[id,"name","methods"]).
:- dynamic interface/3.

table(classImplements,[cid,iid]).
:- dynamic classImplements/2.

table(interfaceExtends,[id,idx]).
:- dynamic interfaceExtends/2.

table(position,[id,x,y]).
position(0,171,269).
position(1,482,269).
position(2,713,269).

