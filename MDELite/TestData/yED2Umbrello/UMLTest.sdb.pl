:-style_check(-discontiguous).

table(class,[id,"name","fields","methods",superid]).
class(4,'Subclass','','',1).
class(0,'Realization','','',null).
class(1,'Superclass','','',null).
class(2,'Aggregation','','',null).
class(3,'Component','','',null).

table(association,[cid1,"role1",arrow1,cid2,"role2",arrow2]).
association(1,'',agg,2,'',none).
association(3,'',comp,1,'',none).
association(0,'',none,1,'',arrow).

table(interface,[id,"name","methods"]).
interface(5,'SubInterface','').
interface(6,'BaseInterface','').

table(classImplements,[cid,iid]).
classImplements(0,5).

table(interfaceExtends,[id,idx]).
interfaceExtends(5,6).

table(position,[id,x,y]).
position(0,406,467).
position(1,616,467).
position(2,836,467).
position(3,610,270).
position(4,616,664).
position(5,401,292).
position(6,401,141).

