dbase(yeduml,[yedclass,yedinterface,yedassociation,yedImplements,yedInterfaceExtends]).
table(yedclass,[id,"name","fields","methods",superid,x,y]).
table(yedinterface,[id,"name","methods",x,y]).
table(yedassociation,[cid1, "role1",arrow1,cid2,"role2",arrow2]).
table(yedImplements,[cid,iid]).
table(yedInterfaceExtends,[id,idx]).

yedclass(0, 'Realization', '', '', null, 406, 467).
yedclass(1, 'Superclass', '', '', null, 616, 467).
yedclass(2, 'Aggregation', '', '', null, 836, 467).
yedclass(3, 'Component', '', '', null, 610, 270).
yedclass(4, 'Subclass', '', '', null, 616, 664).

yedinterface(5, 'SubInterface', '', 401, 292).
yedinterface(6, 'BaseInterface', '', 401, 141).

yedassociation(1, '', white_diamond, 2, '', none).
yedassociation(4, '', none, 1, '', white_delta).
yedassociation(3, '', diamond, 1, '', none).
yedassociation(0, '', none, 1, '', short).

yedImplements(0, 5).

yedInterfaceExtends(5, 6).

