:-style_check(-discontiguous).

table(gliffy_class,[id,"name","fields","methods"]).
gliffy_class(0,'Book','String title; int amount;','int getAmount(); String getTitle();').
gliffy_class(1,'Author','String name;','').
gliffy_class(2,'Address','','').

table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
gliffy_association(1,0,'writes *',false,1,'writtenBy 1..*',false).
gliffy_association(1,1,'',false,2,'livesAt 1',true).
gliffy_association(1,1,'',false,2,'livesAt 1',false).

table(gliffy_interface,[id,"name","methods"]).
:- dynamic gliffy_interface/3.

table(gliffy_inherits,[id,cid1,cid2]).
:- dynamic gliffy_inherits/3.

table(gliffy_position,[id,x,y]).
gliffy_position(0,250,235).
gliffy_position(1,568,262).
gliffy_position(2,806,262).

