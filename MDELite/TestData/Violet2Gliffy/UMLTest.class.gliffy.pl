:-style_check(-discontiguous).

table(gliffy_class,[id,"name","fields","methods"]).
gliffy_class(0,'Realization','','').
gliffy_class(1,'Superclass','','').
gliffy_class(2,'Subclass','','').
gliffy_class(3,'Aggregation','','').
gliffy_class(4,'Component','','').

table(gliffy_association,[id,cid1,"role1",arrow1,cid2,"role2",arrow2]).
gliffy_association(1,0,'',false,1,'',true).
gliffy_association(1,0,'',false,1,'',false).
gliffy_association(1,1,'',true,3,'',false).
gliffy_association(1,1,'',false,3,'',false).
gliffy_association(1,4,'',true,1,'',false).
gliffy_association(1,4,'',false,1,'',false).

table(gliffy_interface,[id,"name","methods"]).
gliffy_interface(5,'SubInterface','').
gliffy_interface(6,'BaseInterface','').

table(gliffy_inherits,[id,cid1,cid2]).
gliffy_inherits(7,0,5).
gliffy_inherits(8,5,6).

table(gliffy_position,[id,x,y]).
gliffy_position(0,238,344).
gliffy_position(1,502,342).
gliffy_position(2,501,508).
gliffy_position(3,745,340).
gliffy_position(4,499,164).
gliffy_position(5,240,211).
gliffy_position(6,241,68).

