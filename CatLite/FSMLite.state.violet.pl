dbase(category,[category,domain,transition]).

table(category,[name]).
category('FSMLite').

table(domain,[id,name,filetype]).
domain('StateNode0','StarUML','.xml').
domain('StateNode1','SDB','.sdb.pl').
domain('StateNode2','StarUMLpl','.staruml.pl').
domain('StateNode3','Umbrello','.xmi').
domain('StateNode4','Gliffy_FSMpl','.gliffy.pl').
domain('StateNode5','Gliffy','.gliffy').
domain('StateNode6','yEDFSMpl','.yedfsm.pl').
domain('StateNode7','yED','.xgml').

table(transition,[transid,name,type,file,startsAt,endsAt]).
transition('t0','toStarUMLpl','Java','FSMLite.starumlparser.Main','StateNode0','StateNode2').
transition('t1','toStarUML','M2T','starumlFSM.vm','StateNode2','StateNode0').
transition('t2','toSDB','M2M','staruml2sdb.pl','StateNode2','StateNode1').
transition('t3','toStarUMLpl','M2M','sdb2staruml.pl','StateNode1','StateNode2').
transition('t4','toSDB','Java','FSMLite.umbrelloParser.fsm.Main','StateNode3','StateNode1').
transition('t5','toUmbrello','M2T','FSM2XMI.vm','StateNode1','StateNode3').
transition('t6','toGliffy','M2T','gliffy_fsm.vm','StateNode4','StateNode5').
transition('t7','toGliffy_FSMpl','Java','FSMLite.gliffy.StateParser','StateNode5','StateNode4').
transition('t8','toGliffy_FSMpl','M2M','fsm2gliffy.pl','StateNode1','StateNode4').
transition('t9','toSDB','M2M','gliffy2fsm.pl','StateNode4','StateNode1').
transition('t10','toyEDFSMpl','M2M','fsm2yed.pl','StateNode1','StateNode6').
transition('t11','toSDB','M2M','yed2fsm.pl','StateNode6','StateNode1').
transition('t12','toyED','M2T','yedFSM.vm','StateNode6','StateNode7').
transition('t13','toyEDFSMpl','Java','FSMLite.yedparser.Main','StateNode7','StateNode6').
