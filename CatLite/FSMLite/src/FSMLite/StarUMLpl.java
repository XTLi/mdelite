package FSMLite;

import CoreMDELite.*;

public class StarUMLpl extends GProlog {

    @Override
    public String fileType() {
        return ".staruml.pl";
    }

    @Override
    public String partialFileType() {
        return ".staruml";
    }

    public StarUMLpl(String filename) {
        super(filename);
    }

    public StarUMLpl(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public StarUML toStarUML() {
        return toStarUML("");
    }

    public StarUML toStarUML(String extra) {
        StarUML result = new StarUML(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/starumlFSM.vm");
        return result;
    }

    public SDB toSDB() {
        return toSDB("");
    }

    public SDB toSDB(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/staruml2sdb.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/sdb.schema.pl",
            HomePath.homePath + "libpl/sdb.run.pl",
            this.fullName};
        StarUMLpl tmp = new StarUMLpl("tmp", list);
        SDB result = new SDB(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/staruml.conform.pl"};
        StarUMLpl temp = new StarUMLpl("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
