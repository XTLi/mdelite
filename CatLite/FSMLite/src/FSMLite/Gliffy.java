package FSMLite;

import CoreMDELite.*;

public class Gliffy extends MDELiteObject {

    @Override
    public String fileType() {
        return ".gliffy";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public Gliffy(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public Gliffy_FSMpl toGliffy_FSMpl() {
        Gliffy_FSMpl result = new Gliffy_FSMpl(filename);
        String[] args = { partialName };
        FSMLite.gliffy.StateParser.main(args);
        result.conform();
        return result;
    }

}
