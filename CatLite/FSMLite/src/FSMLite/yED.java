package FSMLite;

import CoreMDELite.*;

public class yED extends MDELiteObject {

    @Override
    public String fileType() {
        return ".xgml";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public yED(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public yEDFSMpl toyEDFSMpl() {
        yEDFSMpl result = new yEDFSMpl(filename);
        String[] args = { partialName };
        FSMLite.yedparser.Main.main(args);
        result.conform();
        return result;
    }

}
