package FSMLite;

import CoreMDELite.*;

public class yEDFSMpl extends GProlog {

    @Override
    public String fileType() {
        return ".yedfsm.pl";
    }

    @Override
    public String partialFileType() {
        return ".yedfsm";
    }

    public yEDFSMpl(String filename) {
        super(filename);
    }

    public yEDFSMpl(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public SDB toSDB() {
        return toSDB("");
    }

    public SDB toSDB(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/yed2fsm.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/sdb.schema.pl",
            HomePath.homePath + "libpl/sdb.run.pl",
            this.fullName};
        yEDFSMpl tmp = new yEDFSMpl("tmp", list);
        SDB result = new SDB(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public yED toyED() {
        return toyED("");
    }

    public yED toyED(String extra) {
        yED result = new yED(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/yedFSM.vm");
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/yedfsm.conform.pl"};
        yEDFSMpl temp = new yEDFSMpl("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
