package FSMLite;

public class Conform {

    public static void marquee() {
        System.out.println("Usage: Conform <type> <filename(not including dot extension)>");
        System.out.println("       <type> := staruml | umbrello | gliffy | yed");
        System.exit(1);
    }

    public static void main(String args[]) {
        if (args.length != 2) {
            marquee();
        }
        args[0] = args[0].toLowerCase();
        if (args[0].equals("staruml")) {
            StarUML staruml = new StarUML(args[1]);
            staruml.toStarUMLpl();
            return;
        }
        if (args[0].equals("umbrello")) {
            Umbrello umbrello = new Umbrello(args[1]);
            umbrello.toSDB();
            return;
        }
        if (args[0].equals("gliffy")) {
            Gliffy gliffy = new Gliffy(args[1]);
            gliffy.toGliffy_FSMpl().toSDB();
            return;
        }
        if (args[0].equals("yed")) {
            yED yed = new yED(args[1]);
            yed.toyEDFSMpl();
            return;
        }
        System.err.println("unsupported type " + args[0]);
    }

}
