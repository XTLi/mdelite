:- discontiguous toHub/3.
:- discontiguous spoke/3.
:- discontiguous path/3.
:- discontiguous conform/2.
dbase(specifiation, [category, toHub, spoke, path, conform]).
table(category, [name]).
table(toHub, [start, hub, arrows]).
table(spoke, [hub, end, arrows]).
table(path, [start, end, arrows]).
table(conform, [domain, arrows]).
category('MDELite').
toHub('Violet', 'SDB', '.toVioletpl().toSDB()').
toHub('Yuml', 'SDB', '.toYumlpl().toSDB().toDot().toSDB()').
toHub('StarUML', 'SDB', '.toStarUMLpl().toSDB()').
toHub('Umbrello', 'SDB', '.toSDB()').
toHub('yED', 'SDB', '.toyEDUMLpl().toSDB()').
toHub('Gliffy', 'SDB', '.toGliffy_UMLpl().toSDB()').
spoke('SDB', 'Violet', '.toVioletpl().toViolet()').
spoke('SDB', 'Yuml', '.toYumlpl().toYuml()').
spoke('SDB', 'StarUML', '.toStarUMLpl().toStarUML()').
spoke('SDB', 'Umbrello', '.toUmbrello()').
spoke('SDB', 'yED', '.toyEDUMLpl().toyED()').
spoke('SDB', 'Gliffy', '.toGliffy_UMLpl().toGliffy()').
conform('Violet', '.toVioletpl()').
conform('Yuml', '.toYumlpl()').
conform('StarUML', '.toStarUMLpl()').
conform('Umbrello', '.toSDB()').
conform('yED', '.toyEDUMLpl()').
conform('Gliffy', '.toGliffy_UMLpl().toSDB()').
