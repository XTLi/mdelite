package CatLite;

import CoreMDELite.*;

public class VioletPL extends GProlog {

    @Override
    public String fileType() {
        return ".state.violet.pl";
    }

    @Override
    public String partialFileType() {
        return ".state.violet";
    }

    public VioletPL(String filename) {
        super(filename);
    }

    public VioletPL(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public JavaCode toJavaCode() {
        return toJavaCode("");
    }

    public JavaCode toJavaCode(String extra) {
        JavaCode result = new JavaCode(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/category.vm");
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", fullName, HomePath.homePath + "libpl/violet.conform.pl"};
        VioletPL temp = new VioletPL("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
