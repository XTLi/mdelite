package CatLite;

import CoreMDELite.*;

public class VioletDiagram extends MDELiteObject {

    @Override
    public String fileType() {
        return ".state.violet";
    }

    @Override
    public String partialFileType() {
        return ".state";
    }

    public VioletDiagram(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public VioletPL toVioletPL() {
        VioletPL result = new VioletPL(filename);
        String[] args = {filename+fileType()};
        VioletParser.Main.main(args);
        result.conform();
        return result;
    }

}
