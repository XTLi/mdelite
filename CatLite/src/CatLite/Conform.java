package CatLite;

public class Conform {
     
    public static void marquee() {
        System.out.println("Usage: Conform <filename(not including dot extension)>");
        System.exit(1);        
    }
    
    public static void main(String args[]) {
        if (args.length != 1) {
            marquee();
        }
        VioletDiagram v = new VioletDiagram(args[0]);
        VioletPL p = v.toVioletPL();
    }

}
