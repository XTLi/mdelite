package CatLite;

import CoreMDELite.*;

public class Specification extends MDELiteObject {

    @Override
    public String fileType() {
        return ".specs";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public Specification(String filename) {
        super(filename);
    }
    
    /* the following are transformations */

    public SpecificationPL toSpecificationPL() {
        SpecificationPL result = new SpecificationPL(filename);
        String[] args = {partialName};
        specsParser.Main.main(args);
        result.conform();
        return result;
    }
    

}
