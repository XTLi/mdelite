package CatLite;

public class Convert {

    public static void marquee() {
        System.out.println("Usage: Convert <filename(not including dot extension)>");
        System.exit(1);
    }
    
    public static void main(String args[]) {
        if (args.length != 1) {
            marquee();
        }
        VioletDiagram v = new VioletDiagram(args[0]);
        VioletPL p = v.toVioletPL();
        p.toJavaCode();
        Specification specs = new Specification(args[0]);
        specs.toSpecificationPL().toJavaCode();
    }

}
