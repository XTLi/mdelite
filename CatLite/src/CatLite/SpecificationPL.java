package CatLite;

import CoreMDELite.*;

public class SpecificationPL extends GProlog {

    @Override
    public String fileType() {
        return ".specs.pl";
    }

    @Override
    public String partialFileType() {
        return ".specs";
    }

    public SpecificationPL(String filename) {
        super(filename);
    }

    public SpecificationPL(String filename, String[] files) {
        super(filename, files);
    }
    
    /* the following are transformations */

    public JavaCode toJavaCode() {
        return toJavaCode("");
    }

    public JavaCode toJavaCode(String extra) {
        JavaCode result = new JavaCode(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/specification.vm");
        return result;
    }
    
    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", fullName, HomePath.homePath + "libpl/specification.conform.pl"};
        SpecificationPL temp = new SpecificationPL("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
