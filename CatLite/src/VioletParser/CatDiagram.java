/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VioletParser;

import java.util.ArrayList;

/**
 *
 * @author troy
 */
public class CatDiagram {
    public ArrayList<Domain> domains ;
    public ArrayList<Transition> trans ;
    public String name;
    
    public CatDiagram(){
        this.domains = new ArrayList<>();
        this.trans = new ArrayList<>(); 
    }
    
    public void addState(Domain s){
        domains.add(s);
    }
    
    public void addTransition(Transition e){
        trans.add(e);
    }
}
