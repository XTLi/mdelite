/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VioletParser;

/**
 *
 * @author troy
 */
public class Transition {
    private String id;
    private String name;
    private String type;
    private String file;
    private String startsAt;
    private String endsAt;
    private String quote = "\'";
    
    public Transition(String id, String name, String type, String file, String startsAt, String endsAt){
        this.id = quote + id + quote;
        this.name = quote + name + quote;
        this.type = quote + type + quote;
        this.file = quote + file + quote;
        this.startsAt = quote + startsAt + quote;
        this.endsAt = quote + endsAt + quote;
    }
    
    public String getId(){
        return this.id;
    }
    
    public String getName(){
        return this.name;
    } 
    
    public String getType(){
        return this.type;
    }
    
    public String getFile(){
        return this.file;
    }
    public String getStartsAt(){
        return this.startsAt;
    }
    
    public String getEndsAt(){
        return this.endsAt;
    }
    
}
