/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VioletParser;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;

/**
 *
 * @author troy
 */
public class Main {
    
    public static void main(String[] args) {
        try {
            String input_File = args[0];
            VioletParser v = new VioletParser();
            Document doc = v.parser(input_File);            
            //Create State Diagram based on DOM tree
            CatDiagram d = v.createDiagram(doc);
            d.name = "\'" + input_File.split("\\.")[0] + "\'";
            String output_File = input_File + ".pl";
            DBWriter writer = new DBWriter(output_File);
            writer.generateVioletDB(d);
        } catch (DocumentException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

}
