/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VioletParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author troy
 */
public class DBWriter {
    private String inputFile;
    
    public DBWriter(String file){
        this.inputFile = file;
    }
    
     public void generateVioletDB(CatDiagram cd) {
        try {
            FileWriter fstream = new FileWriter(inputFile);
            BufferedWriter out = new BufferedWriter(fstream);

            //Output database definition
            out.write("dbase(category,[category,domain,transition]).\n");

            //Output Category Name in category table
            out.write("\ntable(category,[name]).\n");
            out.write("category(" + cd.name + ").\n");
            
            //Ouput Domains in domain table 
            out.write("\ntable(domain,[id,name,filetype]).\n");
            ArrayList<Domain> states = cd.domains;
            if (states.size() > 0) {
                for (Domain s : states) {
                    out.write("domain(" + s.getId() + "," + s.getName() + "," + s.getFiletype()+ ").\n");
                }
            }

            //Ouput TRansitions in transition table 
            out.write("\ntable(transition,[transid,name,type,file,startsAt,endsAt]).\n");
            ArrayList<Transition> trans = cd.trans;
            if (trans.size() > 0) {
                for (Transition t : trans) {
                    out.write("transition(" + t.getId() + "," + t.getName() + "," + t.getType() + "," + t.getFile() + "," + t.getStartsAt()+ "," + t.getEndsAt() + ").\n");
                }
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
