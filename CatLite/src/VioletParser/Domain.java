/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package VioletParser;

/**
 *
 * @author troy
 */
public class Domain {
    
    static private String quote = "\'";
    
    private String id;
    private String name;
    private String filetype;
    
    public Domain(String id, String name, String filetype){
        this.id = quote + id + quote;
        this.name = quote + name + quote;
        this.filetype = quote + filetype + quote;
    }
    
    public String getId(){
        return this.id;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getFiletype() {
        return this.filetype;
    }
    
}
