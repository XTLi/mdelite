        
package VioletParser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 *
 * @author troy
 */
public class VioletParser {
    
    private Map<String,String> typeMap = new HashMap<String,String>(50);
    private Map<String,String> fileMap = new HashMap<String,String>(50);
    
    public Document parser(String path) throws DocumentException {
        SAXReader reader  = new SAXReader(); 
        Document document = reader.read(path);
        
        return document;
        
        //Node start = document.selectSingleNode("//book[1]");
        //System.out.println(start.getName());
        //String name = start.selectSingleNode("title").getText();
        //System.out.println(name);
                
    }
    
    public CatDiagram createDiagram(Document doc){
        CatDiagram s = new CatDiagram();
      
        //add domain nodes
        List domainNodes = doc.selectNodes("//object[@class='com.horstmann.violet.StateNode']");
        for (Iterator iter = domainNodes.iterator(); iter.hasNext(); ) {
            Node node = (Node)iter.next();
            Domain state = Node2Domain(node); 
            s.addState(state);
        }
        
        List noteNodes = doc.selectNodes("//object[@class='com.horstmann.violet.NoteNode']");
        for (Iterator iter = noteNodes.iterator(); iter.hasNext();){
            Node node = (Node)iter.next();
            noteHandler(node);
        }
        //add transitions
        List transNodes = doc.selectNodes("//void[@method='connect']");
        int i = 0;
        for (Iterator iter = transNodes.iterator(); iter.hasNext(); ) {
            Node node = (Node)iter.next();
            Transition trans = (Transition)Node2Trans(node, i++); 
            s.addTransition(trans);
        }
        
        return s;
    }
    
    public Domain Node2Domain(Node n){
        String id = n.valueOf("@id");
        String name = n.selectSingleNode("void[@property='name']/void[@property='text']/string").getText().replaceAll("\\s","");
        String filetype = name.substring(name.indexOf('(')+1, name.lastIndexOf(')'));
        name = name.substring(0, name.indexOf('('));
        return new Domain(id, name, filetype);
    }
    
    public Transition Node2Trans(Node n, int i){
        String id = "t" + i;
        String name = n.selectSingleNode("object[@class='com.horstmann.violet.StateTransitionEdge']/void[@property='label']/string").getText();
        String type = typeMap.get(name);
        String file = fileMap.get(name);
        String startsAt = n.selectSingleNode("object[2]").valueOf("@idref");
        String endsAt = n.selectSingleNode("object[3]").valueOf("@idref");
        name = name.substring(name.indexOf(')')+1, name.length());
        
        return new Transition(id, name, type, file, startsAt, endsAt);
    }
    
    public void noteHandler(Node n){
        String note = n.selectSingleNode("void[@property='text']/void[@property='text']/string").getText();
        String[] parts = note.split("\\s+");
        for(int i=0; i < parts.length; i+=3){
            typeMap.put(parts[i], parts[i+1]);
            fileMap.put(parts[i], parts[i+2]);
        }
    }   
}

