package specsParser;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    
    private static String quote = "\'";

    private static String supportedTypes[] = {"toHub", "spoke", "path", "conform"};
    private static String currentType = "path";
    
    private static PrintStream out;
    private static BufferedReader in;
    private static int lineCount = 0;
    private static String fileName;
    
    public static void main(String[] args) {
        try {
            fileName = args[0];
            String input = args[0] + ".specs";
            String output = input + ".pl";
            in = new BufferedReader(new FileReader(input));
            out = new PrintStream(output);
            printDef();
            printData();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static boolean supported(String newType) {
        for (int typeItr=0; typeItr<supportedTypes.length; typeItr++)
            if (supportedTypes[typeItr].equals(newType))
                return true;
        return false;
    }

    private static void reportError(String msg) {
        System.err.println(msg + " on line" + lineCount);
        System.exit(1);
    }

    private static void printTuple(String start, String end, String arrows) {
        start = quote + start + quote;
        end = quote + end + quote;
        arrows = quote + arrows + quote;
        if (currentType.equals("conform")) {
            if (arrows.equals(quote + quote))
                arrows = quote + ".conform()" + quote;
            printLine(currentType, start + ", " + arrows);
        }
        else
            printLine(currentType, start + ", " + end + ", " + arrows);
    }

    private static void printLine(String tableName, String tuple) {
        out.println(tableName+"("+tuple+").");
    }

    private static void printData() throws IOException {
        while (true) {
            String line = in.readLine();
            if (line == null)
                break;
            lineCount++;
            line = line.trim();
            if (line.equals(""))
                continue;
            if (line.startsWith("[") && line.endsWith("]")) {
                String newType = line.substring(1, line.length()-1);
                if (supported(newType))
                    currentType = newType;
                else
                    reportError("Unsupported Type " + newType);
            }
            else {
                String[] atoms =  line.split("(?=(--|->))");
                for (int atomItr=0; atomItr<atoms.length; atomItr++)
                    atoms[atomItr] = atoms[atomItr].trim();
                String start = atoms[0];
                String end = atoms[atoms.length - 1];
                if (end.startsWith("->")||end.startsWith("--"))
                    end = end.substring(2).trim();
                String arrows = new String();
                for (int atomItr=1; atomItr<atoms.length; atomItr++) {
                    if (atoms[atomItr].startsWith("->")) 
                        arrows = arrows.concat(".to" + atoms[atomItr].substring(2).trim() + "()");
                    else
                        if (atoms[atomItr].startsWith("--")) {
                            arrows = arrows.concat("." + atoms[atomItr].substring(2).trim() + "()");
                            atomItr++;
                        }
                }
                printTuple(start, end ,arrows);
            }
        }
    }

    private static void printDef() {
        printDiscontiguous("toHub", 3);
        printDiscontiguous("spoke", 3);
        printDiscontiguous("path", 3);
        printDiscontiguous("conform", 2);
        printLine("dbase", "specifiation, [category, toHub, spoke, path, conform]");
        printLine("table", "category, [name]");        
        printLine("table", "toHub, [start, hub, arrows]");
        printLine("table", "spoke, [hub, end, arrows]");
        printLine("table", "path, [start, end, arrows]");
        printLine("table", "conform, [domain, arrows]");
        printLine("category", "'" + fileName + "'");
    }

    private static void printDiscontiguous(String tableName, int elements) {
        out.println(":- discontiguous " + tableName + "/" + elements + ".");
    }
    
}
