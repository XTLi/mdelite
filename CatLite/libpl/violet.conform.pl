% No two domains may have the same name
% Transitions must specify a valid start and end domain
% Transitions must specify a valid type and filename
% No two transitions may have the same name, start domain
% Each domain must have at least one transition connected to it


% No two domains may have the same name
dup_domain(Name) :- domain(Id1, Name, _), domain(Id2, Name, _), Id1 \== Id2.
dup_domain_print(Names) :- print('Duplicate domain name(s): '), print(Names), print('\n').
dup_helper(Names) :- (Names \== []) -> dup_domain_print(Names) ; true.
dup_domains(Names) :- findall(Name, dup_domain(Name), Names), dup_helper(Names).

% No two transitions may have the same name, start domain and end domain
dup_transition(Name, Start) :- transition(_, Name, _, _, Start, _), findall(Id, transition(Id, Name, _, _, Start, _), Ids), not(length(Ids, 1)), print('Duplicate transition - '), print(Name), print(': '),
        print(Start), print('->'), print('?'), print(' \n').
dup_transitions :- findall((Name, Start), dup_transition(Name, Start), Duplicates), Duplicates \== [].

% Transitions must specify a valid start and end domain
% Transitions must specify a valid type and filename
no_start(Id, Name):- transition(Id, Name, _, _, Start, _), not(domain(Start, _, _)).
no_end(Id, Name) :- transition(Id, Name, _, _, _, End), not(domain(End, _, _)).
no_type(Id, Name) :- transition(Id, Name, Type, _, _, _), Type \== 'M2M', Type \== 'M2T', Type \== 'Java'.
no_file(Id, Name) :- transition(Id, Name, _, File, _, _), atom_length(File, Len), Len == 0.

check_1(Id) :- no_start(Id, Name), print('Transition \"'), print(Name), print('\" has no start domain.\n').
check_2(Id) :- no_end(Id, Name), print('Transition \"'), print(Name), print('\" has no end domain.\n').
check_3(Id) :- no_type(Id, Name), print('Transition \"'), print(Name), print('\" does not have a valid type.\n').
check_4(Id) :- no_file(Id, Name), print('Transition \"'), print(Name), print('\" does not have a valid filename.\n').
check_5(Id) :- transition(Id, Name, Type, _, _, 'Boolean'), Type \= 'M2M', print('Transition \"'), print(Name), print('\" should be of type M2M inorder to end at Boolean\n').

invalid_transition(Id) :- check_1(Id) ; check_2(Id) ; check_3(Id) ; check_4(Id) ; check_5(Id).
invalid_transitions :- findall(Id, invalid_transition(Id), Ids), Ids \== [].

% Each domain must have at least one transition connected to it
isolated_domain(Id) :- domain(Id, Name, _), not(transition(_, _, _, _, Id, _)), not(transition(_, _, _, _, _, Id)), print('Domain \"'), print(Name), print('\" is not connected to any transition.\n').

all_constraints :- dup_domains(_), findall((Name, Start), dup_transition(Name, Start), _), findall(Id, check_1(Id), _), findall(Id, check_2(Id), _),
        findall(Id, check_3(Id), _), findall(Id, check_4(Id), _), findall(Id, check_5(Id), _), findall(Id, isolated_domain(Id), _).

run :- all_constraints.




























