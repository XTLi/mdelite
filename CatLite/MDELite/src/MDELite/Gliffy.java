package MDELite;

import CoreMDELite.*;

public class Gliffy extends MDELiteObject {

    @Override
    public String fileType() {
        return ".gliffy";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public Gliffy(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public Gliffy_UMLpl toGliffy_UMLpl() {
        Gliffy_UMLpl result = new Gliffy_UMLpl(filename);
        String[] args = { partialName };
        MDELite.gliffy.UmlParser.main(args);
        result.conform();
        return result;
    }

}
