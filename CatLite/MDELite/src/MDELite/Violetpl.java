package MDELite;

import CoreMDELite.*;

public class Violetpl extends GProlog {

    @Override
    public String fileType() {
        return ".violet.pl";
    }

    @Override
    public String partialFileType() {
        return ".violet";
    }

    public Violetpl(String filename) {
        super(filename);
    }

    public Violetpl(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public Violet toViolet() {
        return toViolet("");
    }

    public Violet toViolet(String extra) {
        Violet result = new Violet(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/violetXml.vm");
        return result;
    }

    public SDB toSDB() {
        return toSDB("");
    }

    public SDB toSDB(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/violet2sdb.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/sdb.schema.pl",
            HomePath.homePath + "libpl/sdb.run.pl",
            this.fullName};
        Violetpl tmp = new Violetpl("tmp", list);
        SDB result = new SDB(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/violet.conform.pl"};
        Violetpl temp = new Violetpl("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
