package MDELite;

import CoreMDELite.*;

public class Yumlpl extends GProlog {

    @Override
    public String fileType() {
        return ".yuml.pl";
    }

    @Override
    public String partialFileType() {
        return ".yuml";
    }

    public Yumlpl(String filename) {
        super(filename);
    }

    public Yumlpl(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public Yuml toYuml() {
        return toYuml("");
    }

    public Yuml toYuml(String extra) {
        Yuml result = new Yuml(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/yumlpl2yuml.vm");
        return result;
    }

    public SDB toSDB() {
        return toSDB("");
    }

    public SDB toSDB(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/yuml2sdb.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/sdb.schema.pl",
            HomePath.homePath + "libpl/sdb.run.pl",
            this.fullName};
        Yumlpl tmp = new Yumlpl("tmp", list);
        SDB result = new SDB(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/yuml.conform.pl"};
        Yumlpl temp = new Yumlpl("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
