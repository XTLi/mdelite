package MDELite;

import CoreMDELite.*;

public class Dot extends MDELiteObject {

    @Override
    public String fileType() {
        return ".dot";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public Dot(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public SDB toSDB() {
        SDB result = new SDB(filename);
        String[] args = { partialName };
        MDELite.dot2sdb.Main.main(args);
        result.conform();
        return result;
    }

}
