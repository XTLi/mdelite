package MDELite;

public class Convert {

        public static void marquee() {
            System.out.println("Usage: Convert <from> <to> <filename(not including dot extension)>");
            System.out.println("       <from> <to> := violet yuml");
            System.out.println("                    | violet staruml");
            System.out.println("                    | violet umbrello");
            System.out.println("                    | violet yed");
            System.out.println("                    | violet gliffy");
            System.out.println("                    | yuml violet");
            System.out.println("                    | yuml staruml");
            System.out.println("                    | yuml umbrello");
            System.out.println("                    | yuml yed");
            System.out.println("                    | yuml gliffy");
            System.out.println("                    | staruml violet");
            System.out.println("                    | staruml yuml");
            System.out.println("                    | staruml umbrello");
            System.out.println("                    | staruml yed");
            System.out.println("                    | staruml gliffy");
            System.out.println("                    | umbrello violet");
            System.out.println("                    | umbrello yuml");
            System.out.println("                    | umbrello staruml");
            System.out.println("                    | umbrello yed");
            System.out.println("                    | umbrello gliffy");
            System.out.println("                    | yed violet");
            System.out.println("                    | yed yuml");
            System.out.println("                    | yed staruml");
            System.out.println("                    | yed umbrello");
            System.out.println("                    | yed gliffy");
            System.out.println("                    | gliffy violet");
            System.out.println("                    | gliffy yuml");
            System.out.println("                    | gliffy staruml");
            System.out.println("                    | gliffy umbrello");
            System.out.println("                    | gliffy yed");
            System.exit(1);
        }
        
        public static void main(String args[]) {
            if (args.length!=3)
                marquee();
            args[0] = args[0].toLowerCase();
            args[1] = args[1].toLowerCase();
            if (args[0].equals("violet") && args[1].equals("yuml")) {
                Violet violet = new Violet(args[2]);
                violet.toVioletpl().toSDB().toYumlpl().toYuml();
                return;
            }
            if (args[0].equals("violet") && args[1].equals("staruml")) {
                Violet violet = new Violet(args[2]);
                violet.toVioletpl().toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("violet") && args[1].equals("umbrello")) {
                Violet violet = new Violet(args[2]);
                violet.toVioletpl().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("violet") && args[1].equals("yed")) {
                Violet violet = new Violet(args[2]);
                violet.toVioletpl().toSDB().toyEDUMLpl().toyED();
                return;
            }
            if (args[0].equals("violet") && args[1].equals("gliffy")) {
                Violet violet = new Violet(args[2]);
                violet.toVioletpl().toSDB().toGliffy_UMLpl().toGliffy();
                return;
            }
            if (args[0].equals("yuml") && args[1].equals("violet")) {
                Yuml yuml = new Yuml(args[2]);
                yuml.toYumlpl().toSDB().toDot().toSDB().toVioletpl().toViolet();
                return;
            }
            if (args[0].equals("yuml") && args[1].equals("staruml")) {
                Yuml yuml = new Yuml(args[2]);
                yuml.toYumlpl().toSDB().toDot().toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("yuml") && args[1].equals("umbrello")) {
                Yuml yuml = new Yuml(args[2]);
                yuml.toYumlpl().toSDB().toDot().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("yuml") && args[1].equals("yed")) {
                Yuml yuml = new Yuml(args[2]);
                yuml.toYumlpl().toSDB().toDot().toSDB().toyEDUMLpl().toyED();
                return;
            }
            if (args[0].equals("yuml") && args[1].equals("gliffy")) {
                Yuml yuml = new Yuml(args[2]);
                yuml.toYumlpl().toSDB().toDot().toSDB().toGliffy_UMLpl().toGliffy();
                return;
            }
            if (args[0].equals("staruml") && args[1].equals("violet")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toVioletpl().toViolet();
                return;
            }
            if (args[0].equals("staruml") && args[1].equals("yuml")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toYumlpl().toYuml();
                return;
            }
            if (args[0].equals("staruml") && args[1].equals("umbrello")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("staruml") && args[1].equals("yed")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toyEDUMLpl().toyED();
                return;
            }
            if (args[0].equals("staruml") && args[1].equals("gliffy")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toGliffy_UMLpl().toGliffy();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("violet")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toVioletpl().toViolet();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("yuml")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toYumlpl().toYuml();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("staruml")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("yed")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toyEDUMLpl().toyED();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("gliffy")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toGliffy_UMLpl().toGliffy();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("violet")) {
                yED yed = new yED(args[2]);
                yed.toyEDUMLpl().toSDB().toVioletpl().toViolet();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("yuml")) {
                yED yed = new yED(args[2]);
                yed.toyEDUMLpl().toSDB().toYumlpl().toYuml();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("staruml")) {
                yED yed = new yED(args[2]);
                yed.toyEDUMLpl().toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("umbrello")) {
                yED yed = new yED(args[2]);
                yed.toyEDUMLpl().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("gliffy")) {
                yED yed = new yED(args[2]);
                yed.toyEDUMLpl().toSDB().toGliffy_UMLpl().toGliffy();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("violet")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_UMLpl().toSDB().toVioletpl().toViolet();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("yuml")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_UMLpl().toSDB().toYumlpl().toYuml();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("staruml")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_UMLpl().toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("umbrello")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_UMLpl().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("yed")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_UMLpl().toSDB().toyEDUMLpl().toyED();
                return;
            }
            System.err.println("unsupported transformation from " + args[0] + "to " + args[1]);
    }
        
}
