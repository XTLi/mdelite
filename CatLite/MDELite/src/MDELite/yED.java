package MDELite;

import CoreMDELite.*;

public class yED extends MDELiteObject {

    @Override
    public String fileType() {
        return ".xgml";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public yED(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public yEDUMLpl toyEDUMLpl() {
        yEDUMLpl result = new yEDUMLpl(filename);
        String[] args = { partialName };
        MDELite.yedparser.Main.main(args);
        result.conform();
        return result;
    }

}
