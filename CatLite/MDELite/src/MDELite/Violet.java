package MDELite;

import CoreMDELite.*;

public class Violet extends MDELiteObject {

    @Override
    public String fileType() {
        return ".violet";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public Violet(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public Violetpl toVioletpl() {
        Violetpl result = new Violetpl(filename);
        String[] args = { partialName };
        MDELite.violetParser.main.Main.main(args);
        result.conform();
        return result;
    }

}
