package MDELite;

import CoreMDELite.*;

public class SDB extends GProlog {

    @Override
    public String fileType() {
        return ".sdb.pl";
    }

    @Override
    public String partialFileType() {
        return ".sdb";
    }

    public SDB(String filename) {
        super(filename);
    }

    public SDB(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public Yumlpl toYumlpl() {
        return toYumlpl("");
    }

    public Yumlpl toYumlpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/sdb2yuml.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/yuml.schema.pl",
            HomePath.homePath + "libpl/yuml.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        Yumlpl result = new Yumlpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public Violetpl toVioletpl() {
        return toVioletpl("");
    }

    public Violetpl toVioletpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/sdb2violet.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/violet.schema.pl",
            HomePath.homePath + "libpl/violet.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        Violetpl result = new Violetpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public StarUMLpl toStarUMLpl() {
        return toStarUMLpl("");
    }

    public StarUMLpl toStarUMLpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/sdb2staruml.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/staruml.schema.pl",
            HomePath.homePath + "libpl/staruml.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        StarUMLpl result = new StarUMLpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public Dot toDot() {
        return toDot("");
    }

    public Dot toDot(String extra) {
        Dot result = new Dot(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/p2dot.vm");
        return result;
    }

    public Umbrello toUmbrello() {
        return toUmbrello("");
    }

    public Umbrello toUmbrello(String extra) {
        Umbrello result = new Umbrello(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/Umbrello2XML.vm");
        return result;
    }

    public yEDUMLpl toyEDUMLpl() {
        return toyEDUMLpl("");
    }

    public yEDUMLpl toyEDUMLpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/sdb2yeduml.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/yeduml.schema.pl",
            HomePath.homePath + "libpl/yeduml.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        yEDUMLpl result = new yEDUMLpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public Gliffy_UMLpl toGliffy_UMLpl() {
        return toGliffy_UMLpl("");
    }

    public Gliffy_UMLpl toGliffy_UMLpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/sdb2gliffy.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/gliffy_uml.schema.pl",
            HomePath.homePath + "libpl/gliffy_uml.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        Gliffy_UMLpl result = new Gliffy_UMLpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/sdb.conform.pl"};
        SDB temp = new SDB("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
