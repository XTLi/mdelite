#set($MARKER="//----")
##Preprocessing
#foreach($category in $categoryS)
#set($package = ${category.name.replaceAll("\'","")})
#end
#foreach($domain in $domainS)
#set(${domain.name} = ${domain.name.replaceAll("\'","")})
#set(${domain.filetype} = ${domain.filetype.replaceAll("\'","")})
#set(${domain.prolog} = ${domain.filetype.endsWith(".pl")})
#set($lastDot = ${domain.filetype.lastIndexOf('.')})
#if($lastDot < 0)
#set($lastDot = 0)
#end
#set(${domain.partialfiletype} = ${domain.filetype.substring(0, $lastDot)})
#if (${domain.prolog} == true)
#set(${domain.prologname} = ${domain.name.toLowerCase()})
#if(${domain.prologname.endsWith("pl")} == true)
#set($length = ${domain.prologname.length()} - 2)
#set(${domain.prologname} = ${domain.prologname.substring(0, $length)})
#end
#end
#end
#foreach($transition in $transitionS)
#set(${transition.name} = ${transition.name.replaceAll("\'","")})
#set(${transition.type} = ${transition.type.replaceAll("\'","")})
#set(${transition.file} = ${transition.file.replaceAll("\'","")})
#end
#set($SrcDir = $package + "/src")
##Print the source code location and structure
${MARKER}vm2toutput.txt
./$SrcDir
    $package/
#foreach($domain in $domainS)
        ${domain.name}.java
#end
    Main.java
    vm2t.java
    CoreMDELite/
        GProlog.java
        HomePath.java
        MDELiteObject.java
        PCommon.java
        SwiplInstalled.java
        vm2t/
            ContextGenerator.java
            CustomLog.java
            DefaultContextGenerator.java
            Main.java
            Model.java
##Domain classes
#foreach($domain in $domainS)
${MARKER}${SrcDir}/${package}/${domain.name}.java
package $package;

import CoreMDELite.*;

public class ${domain.name} extends #if(${domain.prolog} == true)GProlog#{else}MDELiteObject#end {

    @Override
    public String fileType() {
        return "${domain.filetype}";
    }

    @Override
    public String partialFileType() {
        return "${domain.partialfiletype}";
    }

    public ${domain.name}(String filename) {
        super(filename);
    }

#if(${domain.prolog} == true)
    public ${domain.name}(String filename, String[] files) {
        super(filename, files);
    }

#end
    /* the following are transformations */

#foreach($transition in $transitionS)
#if(${transition.startsAt} == ${domain.id})
#foreach($end in $domainS)
#if(${transition.endsAt} == ${end.id})
#if(${transition.type} == "M2M")
    public ${end.name} ${transition.name}() {
        return ${transition.name}("");
    }

    public ${end.name} ${transition.name}(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/${transition.file}",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/${end.prologname}.schema.pl",
            HomePath.homePath + "libpl/${end.prologname}.run.pl",
            this.fullName};
        ${domain.name} tmp = new ${domain.name}("tmp", list);
        ${end.name} result = new ${end.name}(filename+extra);
        tmp.executeProlog(result);
#if(${end.prolog} == true)
        result.conform();
#end
        tmp.delete();
        return result;
    }
#end
#if(${transition.type} == "Java")
    public ${end.name} ${transition.name}() {
        ${end.name} result = new ${end.name}(filename);
        String[] args = { partialName };
        ${transition.file}.main(args);
#if(${end.prolog} == true)
        result.conform();
#end
        return result;
    }
#end
#if(${transition.type} == "M2T")
    public ${end.name} ${transition.name}() {
        return ${transition.name}("");
    }

    public ${end.name} ${transition.name}(String extra) {
        ${end.name} result = new ${end.name}(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/${transition.file}");
#if(${end.prolog} == true)
        result.conform();
#end
        return result;
    }
#end

#end
#end
#end
#end
#if(${domain.prolog} == true)
    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/${domain.prologname}.conform.pl"};
        ${domain.name} temp = new ${domain.name}("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

#end
}
#end
##Main class for HomePath
${MARKER}${SrcDir}/${package}/Main.java
package $package;

public class Main {

    public static String versionName = "${package} Bootstrapped";
    
    public static void marquee() {
        System.out.println("version "+ versionName);
        System.out.println("Usage: java -jar \"${package}.jar\" <action> <type> [<convert2Type>] <filename(not including dot extension)>");
        System.out.println("       <action> := convert | conform ");
        System.exit(1);
    }
    
    public static void main (String[] args) {

        // check # of arguments
        if (!legalArgs(args))
            marquee();
        
        if (args[0].equals("conform"))
            Conform.main(new String[]{args[1], args[2]});
        else if (args[0].equals("convert"))
            Convert.main(new String[]{args[1], args[2], args[3]});
        else
            marquee();
    }
    
    public static boolean legalArgs(String[] args) {
        if (args.length<3)
            return false;
        if (!args[0].equals("convert") && !args[0].equals("conform"))
            return false;
        if (args[0].equals("conform") && !(args.length==3))
            return false;        
        if (args[0].equals("convert") && !(args.length==4))
            return false;
        return true;
    }
     
}

##Copy vm2t.java
${MARKER}${SrcDir}/${package}/vm2t.java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ${package};
/**
 *
 * @author dsb -- this is a delegate class/facade for MDELite
 */
public class vm2t {
    public static void main(String[] args) {
        CoreMDELite.vm2t.Main.main(args);
    }
}
##Copy CoreMDELite packages
${MARKER}${SrcDir}/CoreMDELite/GProlog.java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CoreMDELite;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;

/**
 *
 * @author dsb
 */
public abstract class GProlog extends MDELiteObject {
    public static final String conformanceFailure = "Conformance Failure";

    public GProlog(String filename, String[] files) {
        super(filename);
        mergeFiles(files);
    }

    // these empty constructors are needed because obscure run-time
    // exceptions are raised otherwise that take 1/2 to track down
    public GProlog(String filename) {
        super(filename);
    }

    private GProlog() {
    }  // shouldn't be called.

    // the result of merged files is the new value of the given object
    protected void mergeFiles(String[] filenames) {
        try {
            PrintStream ps = getPrintStream();
            for (String f : filenames) {
                stuff(f, ps);
            }
            ps.flush();
            ps.close();
        } catch (Exception e) {
            done(e);
        }
    }

    protected static void stuff(String inFileName, PrintStream ps) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(inFileName));
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                ps.println(line);
            }
        } catch (Exception e) {
            done(e);
        }
    }

    // executes a prolog object.  The execution produces a file, whose object is created prior to the call
    // I know this is strange but this is a general problem of run-time objects being paired with files
    // at some point, one will exist before the other
    
    static String scriptFile = "script.txt";
    
    public void executeProlog(MDELiteObject out) {
        
        try {
            PrintStream ps;
            ps = new PrintStream(scriptFile);
            ps.print(":-['" + fullName + "'],tell('" + out.fullName + "'),run,told,halt.");
            ps.flush();
            ps.close();
        } catch (Exception e) {
            done(e);
        }
        String[] cmdarray = {MDELiteObject.configFile.getProperty("SWI_PROLOG_LOCATION"), "--quiet", "-f", scriptFile};
        try {
            execute(cmdarray);
        } catch (Exception e) {
            System.err.println("MDELite halts -- SWI Prolog Errors detected");
            System.err.println("consult Error.txt");
            System.err.println("debug this prolog file:  " + fullName);
            System.exit(1);
        }
        File s = new File(scriptFile);
        s.delete();
    }

    // this method is used to test conformance of a database 
    // with its constraints.  it is coded only for this purpose
    // as it outputs nothing.
    public void executeProlog() throws RuntimeException {
        try {
            PrintStream ps;
            ps = new PrintStream(scriptFile);
            ps.print(":-['" + fullName + "'],tell('conform.txt'),run,told,halt.");
            ps.flush();
            ps.close();
        } catch (Exception e) {
            done(e);
        }
        String[] cmdarray = {MDELiteObject.configFile.getProperty("SWI_PROLOG_LOCATION"), "--quiet", "-f", scriptFile};
        try {
            execute(cmdarray);
        } catch (Exception e) {
            System.err.println("MDELite halts -- SWI Prolog Errors detected");
            System.err.println("consult Error.txt");
            System.err.println("debug this prolog file:   " + fullName);
            System.err.println(conformanceFailure);
            throw new RuntimeException();
        }
        File c = new File("conform.txt");
        if (c.length()!= 0) {
            String msg = conformanceFailure + "\n" + "look at script.txt and conformance errors in conform.txt";
            throw new RuntimeException(msg);
        }
        c.delete();
        File s = new File(scriptFile);
        s.delete();
    }
    
    public abstract void conform();
}
${MARKER}${SrcDir}/CoreMDELite/HomePath.java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CoreMDELite;

import ${package}.Main;

/**
 *
 * @author dsb
 */
public class HomePath {
    public static String homePath = "";

    public static void setHomePath(boolean printMe) {
        int pos;
        Class me = Main.class;
        String tmp = me.getProtectionDomain().getCodeSource().getLocation().getPath();
        if (tmp.startsWith("/") && tmp.contains(":")) {
            tmp = tmp.substring(1);
        }
        if (tmp.contains("build/classes/")) {
            pos = tmp.lastIndexOf("build/classes/");
        } else {
            pos = tmp.lastIndexOf("/")+1;
        }
        homePath = tmp.substring(0, pos);
        if (!homePath.contains(":")) {
            homePath = "/" + homePath;
        }
        if (printMe) {
            System.out.println(homePath);
        }
    }

    public static void main(String[] args) {
        setHomePath(true);
    }
    
}
${MARKER}${SrcDir}/CoreMDELite/MDELiteObject.java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CoreMDELite;

import CoreMDELite.HomePath;
import java.io.*;
import java.util.Properties;

/**
 *
 * @author dsb
 */
public abstract class MDELiteObject {

    public String filename;
    public String partialName;
    public String fullName;
    public static Properties configFile;

    static {
        configFile = new Properties();
        String cp = null;
        try {
            HomePath.setHomePath(false);
            cp = HomePath.homePath + "config.properties";
            configFile.load(new FileInputStream(cp));
        } catch (IOException e) {
            System.err.println("Error in loading config.properties file");
            System.err.println(cp);
            System.err.println("MDELite halts");
            System.exit(1);
        }
    }

    public MDELiteObject(String filename) {
        this.filename = filename;
        this.partialName = filename + partialFileType();
        this.fullName = filename + fileType();
    }

    protected MDELiteObject() {
    } // shouldn't be called

    public abstract String fileType();

    public abstract String partialFileType();

    public static String[] makeArray(MDELiteObject[] array) {
        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i].fullName;
        }
        return result;
    }

    public PrintStream getPrintStream() {
        PrintStream result = null;
        String tmp = fullName;
        try {
            result = new PrintStream(fullName);
        } catch (Exception e) {
            done(e);
        }
        return result;
    }

    public void delete() {
        File f = new File(fullName);
        f.delete();
    }

    // original design of execute
    public static void execute(String[] cmdarray) throws Exception {
        String line;
        String errorLine = "";
        boolean error = false;
        // Runtime rt = Runtime.getRuntime();
        //Process p = rt.exec(cmdarray);
        Process p = new ProcessBuilder(cmdarray).start();
        // assume input, output, error stream is standard input, output, error
        BufferedReader er = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = er.readLine()) != null) {
            error = true;
            errorLine = errorLine + line + "\n";
        }
        while ((line = in.readLine()) != null) {
            System.out.println("SWIProlog Stdout : " + line);
        }
        p.waitFor();
        p.destroy();
        if (error) {
            PrintStream ps;
            ps = new PrintStream("Error.txt");
            ps.print(errorLine);
            ps.flush();
            ps.close();
            throw new Exception("Consult Error.txt");
        }
    }

//    next design of execute
//    see http://www.java-tips.org/java-se-tips/java.util/from-runtime.exec-to-processbuilder.html
//    public static void execute(String[] cmdarray) throws Exception {
//        String line;
//        boolean error = false;
//        try {
//            Runtime rt = Runtime.getRuntime();
//            Process p = rt.exec(cmdarray);
//            BufferedReader er = new BufferedReader(new InputStreamReader(p.getErrorStream()));
//            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
//            while ((line = er.readLine()) != null) {
//                error = true;
//                System.err.println("SWIProlog Stderr : " + line);
//            }
//            while ((line = in.readLine()) != null) {
//                System.out.println("SWIProlog Stdout : " + line);
//            }
//            p.waitFor();
//            p.destroy();
//        } catch (Exception e) {
//            done(e);
//        }
//        if (error) {
//             System.err.print("Errors in executing command array: ");
//             for (String i:cmdarray) System.err.print(" "+i);
//             System.err.println();
//             throw new Exception();
//        }
//    }
//        public static void execute(String[] cmdarray) throws Exception {
//        String line;
//        boolean error = false;
//        try {
//            Process process = new ProcessBuilder(cmdarray).start();
//            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream())); 
//            BufferedReader er = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//            while ((line = er.readLine()) != null) {
//                error = true;
//                System.err.println("SWIProlog Stderr : " + line);
//            }
//            while ((line = in.readLine()) != null) {
//                error = true;
//                System.out.println("SWIProlog Stdout : " + line);
//            }
//            process.waitFor();
//            process.destroy();
//        } catch (Exception e) {
//            done(e);
//        }
//        if (error) {
//             System.err.print("Errors in executing command array: ");
//             for (String i:cmdarray) System.err.print(" "+i);
//             System.err.println();
//             throw new Exception();
//        }
//    }

public void invokeVm2t(MDELiteObject output, String vmfile) {
        try {
            String[] args = {fullName, vmfile};
            CoreMDELite.vm2t.Main.main(args);
            File f = new File("vm2toutput.txt");
            File n = new File(output.fullName);
            n.delete();
            f.renameTo(n);
        } catch (Exception e) {
            done(e);
        }
    }

    public static void done(Exception e) {
        System.err.println(e.getMessage());
        System.exit(1);
    }
}
${MARKER}${SrcDir}/CoreMDELite/PCommon.java
package CoreMDELite;

import java.io.PrintStream;

public class PCommon {

    public static PrintStream ps; // output file
    public static String comma = ",";

    // not currently used
    public static void printTable(String name, String[] fields) {
        ps.println(":- dynamic "+ name + "/"+fields.length+".");
        ps.print("table(" + name + ",[");
        String komma = "";
        for (String i : fields) {
            ps.print(komma + i.replace("'","\""));
            komma = ",";
        }
        ps.println("]).");

        ps.print("tuple(" + name + ",L):-" + name + "(");
        komma = "";
        String result = "";
        for (String i : fields) {
            i = i.replace("\"", "").replace("'","");
            String lead = i.substring(0, 1).toUpperCase();
            result += (komma + lead + i.substring(1));
            komma = ",";
        }
        ps.println(result + "),L=[" + result + "].");
        ps.println("%");
    }

    public static String quote(String x) {
        return "'" + x + "'";
    }
}
${MARKER}${SrcDir}/CoreMDELite/SwiplInstalled.java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CoreMDELite;

import static CoreMDELite.MDELiteObject.done;
import static CoreMDELite.MDELiteObject.execute;
import java.io.*;

/**
 *
 * @author dsb
 */
public class SwiplInstalled  {
    
    public static void  main(String[] args) {
        HomePath.setHomePath(true);
        String swipl = MDELiteObject.configFile.getProperty("SWI_PROLOG_LOCATION");
        String filename = HomePath.homePath+"script.txt";
        
        try {
            PrintStream ps;
            ps = new PrintStream(filename);
            ps.print(":-['" + HomePath.homePath + "libpl/swiplInstalled'],run,halt.");
            ps.flush();
            ps.close();
        } catch (Exception e) {
            MDELiteObject.done(e);
        }
        String[] cmdarray = {MDELiteObject.configFile.getProperty("SWI_PROLOG_LOCATION"), "--quiet", "-f", HomePath.homePath+"script.txt"};
        try {
            execute(cmdarray);
        } catch (Exception e) {
            System.err.println("MDELite halts -- SWI Prolog Errors detected");
            System.err.println("debug this prolog file:  " + filename);
            System.exit(1);
        }
        System.out.println("MDELite Ready to Use!");
    }
}
${MARKER}${SrcDir}/CoreMDELite/vm2t/ContextGenerator.java
package CoreMDELite.vm2t;

import org.apache.velocity.VelocityContext;


public interface ContextGenerator {
  VelocityContext generateContext(Model m);
}
${MARKER}${SrcDir}/CoreMDELite/vm2t/CustomLog.java
package CoreMDELite.vm2t;

import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogChute;

public class CustomLog implements LogChute {

  /**
   *  This is the method that you implement for Velocity to
   *  call with log messages.
   */
  public void log(int level, String message) {
  }

  /**
   *  This is the method that you implement for Velocity to
   *  call with log messages.
   */
  public void log(int level, String message, Throwable t) {
    System.err.println("====================================================");
    System.err.println(message);
  }

  /**
   *  This is the method that you implement for Velocity to
   *  check whether a specified log level is enabled.
   */
  public boolean isLevelEnabled(int level) {
    return true;
  }

  @Override
  public void init(RuntimeServices arg0) throws Exception {
    
  }
}
${MARKER}${SrcDir}/CoreMDELite/vm2t/DefaultContextGenerator.java
package CoreMDELite.vm2t;

import org.apache.velocity.VelocityContext;

public class DefaultContextGenerator implements ContextGenerator {

  @Override
  public VelocityContext generateContext(Model m) {
    VelocityContext context = new VelocityContext();
    for (String tableName : m.tables.keySet()) {
      context.put(tableName, m.tables.get(tableName));
    }
    return context;
  }

}
${MARKER}${SrcDir}/CoreMDELite/vm2t/Main.java
package CoreMDELite.vm2t;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class Main {

    public static void main(String[] args) {
        String prologFileName = null;
        String templateFileName = null;
        String contextGenClassName = null;

        if (args.length == 2) {
            prologFileName = args[0];
            templateFileName = args[1];
        } else if (args.length == 4) {
            if (args[0].equals("-cg")) {
                contextGenClassName = args[1];
            } else {
                System.out.println("Usage: [-cg ContextGeneratorClass] prolog-file template-file");
                System.exit(0);
            }
            prologFileName = args[2];
            templateFileName = args[3];
        } else {
            System.out.println("Usage: [-cg ContextGeneratorClass] prolog-file template-file");
            System.exit(0);
        }

        try {

            File prologFile = new File(prologFileName);
            File templateFile = new File(templateFileName);

            if (!prologFile.exists()) {
                System.out.println("prolog-file does not exist: "+ prologFileName);
                System.exit(1);
            }

            if (!templateFile.exists()) {
                System.out.println("template-file does not exist.");
                System.exit(1);
            }

            Properties p = new Properties();
            String templatePath = ".";
            if (templateFile.getParentFile() != null) {
                templatePath = templateFile.getParentFile().getCanonicalPath();
            }
            p.setProperty("file.resource.loader.path", templatePath);
            Velocity.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM, new CustomLog());
            Velocity.init(p);

            Model m = new Model(prologFile);

            VelocityContext context;
            if (contextGenClassName != null) {
                @SuppressWarnings("unchecked")
                Class<ContextGenerator> cgClass = (Class<ContextGenerator>) Class.forName(contextGenClassName);
                Constructor<ContextGenerator> cgCons = cgClass.getConstructor();
                ContextGenerator cgIns = cgCons.newInstance();
                context = cgIns.generateContext(m);
            } else {
                context = new DefaultContextGenerator().generateContext(m);
            }

            Template template = null;
            template = Velocity.getTemplate(templateFile.getName());
            StringWriter writer = new StringWriter();

            template.merge(context, writer);
            if (context.get("MARKER") == null) {
                System.out.println(writer.toString());
            } else {
                splitter(writer.toString(), context.get("MARKER").toString());
            }
        } catch (ClassNotFoundException e) {
            System.out.println("The context generator class not found. Please make "
                    + "sure it is in the java classpath.");
            System.exit(1);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public static void splitter(String content, String marker) {
        BufferedReader br = new BufferedReader(new StringReader(content));
        String line = null;
        PrintStream out = null;
        String filename = null;

        try {
            while ((line = br.readLine()) != null) {
                if (line.startsWith(marker)) {
                    if (out != null) {
                        out.close();
                    }
                    filename = line.substring(marker.length());
                    File f = new File(filename);
                    if (f.getParentFile() != null && !f.getParentFile().exists()) {
                        f.getParentFile().mkdirs();
                    }
                    out = new PrintStream(f);
                } else {
                    if (out != null) {
                        out.println(line);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (out != null) {  // lines added because the last file was never closed
            out.flush();
            out.close();
        }
    }
}
${MARKER}${SrcDir}/CoreMDELite/vm2t/Model.java
package CoreMDELite.vm2t;

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Model {

    public Map<String, List<Map<String, String>>> tables = new HashMap<String, List<Map<String, String>>>();
    public Map<String, List<String>> schemas = new HashMap<String, List<String>>();

    public Model(File infile) {
        try {
            // 1st pass for tables. Ignore all lines except those that start with '%schema'
            LineNumberReader br = new LineNumberReader(new InputStreamReader(
                    new FileInputStream(infile)));
            String line = null;
            while ((line = br.readLine()) != null) {

                /* START NEW CODE */
                line.trim();
                if (line.startsWith("tuple(")) // ignore tuple facts
                {
                    continue;
                }
//                if (line.startsWith("table(")) // ignore table facts
//                {
//                    continue;
//                }
//                if (line.startsWith("%table(")) {  // parse table facts
                if (line.startsWith("table(")) { // parse table facts
                    StringTokenizer st = new StringTokenizer(line, " \t(),][.\"");
                    try {
                        st.nextToken(); // ignore "table"
                        List<String> columnNames = new ArrayList<String>();
                        while (st.hasMoreTokens()) {
                            String name = st.nextToken().trim();
                            //System.err.println(name);
                            columnNames.add(name);
                        }
                        schemas.put(columnNames.remove(0), columnNames);
                    } catch (Exception e) {
                        System.err.println("unable to parse table on line " + br.getLineNumber() + 
                                " in " + infile.getName());
                    }
                }
            }
            /* END NEW CODE */
            /*
             * START ORIGINAL CODE line = line.trim(); if
             * (line.matches("%\\s*table\\s*:.*")) { String[] names =
             * line.split(":")[1].split(","); List<String> columnNames = new
             * ArrayList<String>(); for (String name: names)
             * columnNames.add(name.trim());
             * tables.put(columnNames.remove(0).trim(), columnNames); } } END
             * ORIGINAL CODE
             */
            // Fill in tables from schema.
            for (String tableName : schemas.keySet()) {
                tables.put(tableName + "S",
                        new ArrayList<Map<String, String>>());
            }
            // 2nd pass for table entries.  Ignore lines that start with 'table(' or 'tuple('
            br = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    infile)));
            line = null;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                // ignore schema and tuple facts and assorted computations
                if (line.startsWith("table(") || line.startsWith("tuple(") 
                    || line.startsWith("%table(") || line.startsWith(":- ")) {
                    continue;
                }
                if (line.matches("\\s*\\w+\\s*(.*)\\.")) {
                    String tableName = line.split("\\(")[0].trim();
                    /*String[] columnNames = line.substring(
                            line.indexOf("(") + 1, line.lastIndexOf(")")).split(",");*/
                    // the following code was added so that commas within a single quote
                    // won't be recognized.  now split on \n characters, not comma.
                    String newline = "";
                    String strippedLine = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));
                    boolean inString = false;
                    for (int i=0; i<strippedLine.length(); i++) {
                        char current = strippedLine.charAt(i);
                        if (current == '\'') {
                            inString=!inString;
                        }
                        if (!inString && current == ',')
                            newline = newline + "@@@";
                        else
                            newline = newline + current;
                    }
                    
                    String[] columnNames = newline.split("@@@");
                    
                    if (!schemas.containsKey(tableName)) {
                        continue;
                    }
                    List<Map<String, String>> entries = tables.get(tableName + "S");
                    Map<String, String> newEntry = new HashMap<String, String>();
                    for (int i = 0; i < schemas.get(tableName).size(); i++) {
                        newEntry.put(schemas.get(tableName).get(i),
                                columnNames[i].trim());
                    }
                    entries.add(newEntry);
                    //System.err.println("adding " + tableName + " tuple");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
