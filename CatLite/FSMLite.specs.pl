:- discontiguous toHub/3.
:- discontiguous spoke/3.
:- discontiguous path/3.
:- discontiguous conform/2.
dbase(specifiation, [category, toHub, spoke, path, conform]).
table(category, [name]).
table(toHub, [start, hub, arrows]).
table(spoke, [hub, end, arrows]).
table(path, [start, end, arrows]).
table(conform, [domain, arrows]).
category('FSMLite').
toHub('StarUML', 'SDB', '.toStarUMLpl().toSDB()').
toHub('Umbrello', 'SDB', '.toSDB()').
toHub('Gliffy', 'SDB', '.toGliffy_FSMpl().toSDB()').
toHub('yED', 'SDB', '.toyEDFSMpl().toSDB()').
spoke('SDB', 'StarUML', '.toStarUMLpl().toStarUML()').
spoke('SDB', 'Umbrello', '.toUmbrello()').
spoke('SDB', 'Gliffy', '.toGliffy_FSMpl().toGliffy()').
spoke('SDB', 'yED', '.toyEDFSMpl().toyED()').
conform('StarUML', '.toStarUMLpl()').
conform('Umbrello', '.toSDB()').
conform('Gliffy', '.toGliffy_FSMpl().toSDB()').
conform('yED', '.toyEDFSMpl()').
