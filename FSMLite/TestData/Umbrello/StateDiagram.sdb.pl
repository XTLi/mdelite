:-style_check(-discontiguous).
dbase(sdb,[node,transition,position]).

table(node,[nodeid,"name",type]).
node('i5ov0kwqiflh4','start',start).
node('ikzjbgvxmjwrd','ready',state).
node('ixc31yihlathr','eat',state).
node('ie7whhiudmasb','drink',state).
node('i5kmjodfq2jcs','pig',state).
node('im6lh8xecvms9','stop',stop).

table(transition,[transid,startsAt,endsAt]).
transition('it0','i5ov0kwqiflh4','ikzjbgvxmjwrd').
transition('it1','ikzjbgvxmjwrd','ixc31yihlathr').
transition('it2','ikzjbgvxmjwrd','ie7whhiudmasb').
transition('it3','ie7whhiudmasb','i5kmjodfq2jcs').
transition('it4','ixc31yihlathr','i5kmjodfq2jcs').
transition('it5','i5kmjodfq2jcs','im6lh8xecvms9').
transition('it6','ie7whhiudmasb','ixc31yihlathr').
transition('it7','ixc31yihlathr','ixc31yihlathr').
transition('it8','ie7whhiudmasb','ie7whhiudmasb').

table(position,[id,x,y]).
position('i5ov0kwqiflh4',529,373).
position('ikzjbgvxmjwrd',697,368).
position('ixc31yihlathr',831,230).
position('ie7whhiudmasb',831,517).
position('i5kmjodfq2jcs',963,373).
position('im6lh8xecvms9',1194,376).
