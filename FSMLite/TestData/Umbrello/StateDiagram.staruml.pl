:-style_check(-discontiguous).

table(starnode,[nodeid,"name",type]).
starnode(i5ov0kwqiflh4,'start',start).
starnode(ikzjbgvxmjwrd,'ready',state).
starnode(ixc31yihlathr,'eat',state).
starnode(ie7whhiudmasb,'drink',state).
starnode(i5kmjodfq2jcs,'pig',state).
starnode(im6lh8xecvms9,'stop',stop).

table(startransition,[transid,startsAt,endsAt]).
startransition(it0,i5ov0kwqiflh4,ikzjbgvxmjwrd).
startransition(it1,ikzjbgvxmjwrd,ixc31yihlathr).
startransition(it2,ikzjbgvxmjwrd,ie7whhiudmasb).
startransition(it3,ie7whhiudmasb,i5kmjodfq2jcs).
startransition(it4,ixc31yihlathr,i5kmjodfq2jcs).
startransition(it5,i5kmjodfq2jcs,im6lh8xecvms9).
startransition(it6,ie7whhiudmasb,ixc31yihlathr).
startransition(it7,ixc31yihlathr,ixc31yihlathr).
startransition(it8,ie7whhiudmasb,ie7whhiudmasb).

table(starposition,[id,x,y]).
starposition(i5ov0kwqiflh4,529,373).
starposition(ikzjbgvxmjwrd,697,368).
starposition(ixc31yihlathr,831,230).
starposition(ie7whhiudmasb,831,517).
starposition(i5kmjodfq2jcs,963,373).
starposition(im6lh8xecvms9,1194,376).

