:-style_check(-discontiguous).

table(gliffy_node,[nodeid,"name",type]).
gliffy_node(0,'start',start).
gliffy_node(1,'ready',state).
gliffy_node(2,'eat',state).
gliffy_node(3,'drink',state).
gliffy_node(4,'pig',state).
gliffy_node(5,'stop',stop).

table(gliffy_transition,[transid,startsAt,endsAt]).
gliffy_transition(6,0,1).
gliffy_transition(7,1,2).
gliffy_transition(8,1,3).
gliffy_transition(9,3,4).
gliffy_transition(10,2,4).
gliffy_transition(11,4,5).
gliffy_transition(12,3,2).
gliffy_transition(13,2,2).
gliffy_transition(14,3,3).

table(gliffy_position,[id,x,y]).
gliffy_position(0,529,373).
gliffy_position(1,697,368).
gliffy_position(2,831,230).
gliffy_position(3,831,517).
gliffy_position(4,963,373).
gliffy_position(5,1194,376).

