:-style_check(-discontiguous).

table(yednode,[nodeid,"name",type,x,y]).
yednode(umlpseudostate_27,'initial1',start,237,357).
yednode(umlcompositestate_28,'ready',state,426,360).
yednode(umlcompositestate_29,'eat',state,606,240).
yednode(umlcompositestate_30,'drink',state,606,486).
yednode(umlcompositestate_31,'pig',state,798,354).
yednode(umlfinalstate_32,'finalstate1',stop,1009,355).

table(yedtransition,[transid,startsAt,endsAt]).
yedtransition(umltransition_33,umlpseudostate_27,umlcompositestate_28).
yedtransition(umltransition_34,umlcompositestate_28,umlcompositestate_29).
yedtransition(umltransition_35,umlcompositestate_28,umlcompositestate_30).
yedtransition(umltransition_36,umlcompositestate_29,umlcompositestate_31).
yedtransition(umltransition_37,umlcompositestate_30,umlcompositestate_31).
yedtransition(umltransition_38,umlcompositestate_31,umlfinalstate_32).
yedtransition(umltransition_39,umlcompositestate_29,umlcompositestate_30).
yedtransition(umltransition_40,umlcompositestate_30,umlcompositestate_29).
yedtransition(umltransition_41,umlcompositestate_29,umlcompositestate_29).
yedtransition(umltransition_42,umlcompositestate_30,umlcompositestate_30).

