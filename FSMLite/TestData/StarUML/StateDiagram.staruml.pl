dbase(staruml,[node,transition,position]).
table(starnode,[nodeid,name,type]).
table(startransition,[transid,startsAt,endsAt]).
table(starposition,[id,x,y]).

starnode(umlpseudostate_27, 'initial1', start).
starnode(umlcompositestate_28, 'ready', state).
starnode(umlcompositestate_29, 'eat', state).
starnode(umlcompositestate_30, 'drink', state).
starnode(umlcompositestate_31, 'pig', state).
starnode(umlfinalstate_32, 'finalstate1', stop).

startransition(umltransition_33, umlpseudostate_27, umlcompositestate_28).
startransition(umltransition_34, umlcompositestate_28, umlcompositestate_29).
startransition(umltransition_35, umlcompositestate_28, umlcompositestate_30).
startransition(umltransition_36, umlcompositestate_29, umlcompositestate_31).
startransition(umltransition_37, umlcompositestate_30, umlcompositestate_31).
startransition(umltransition_38, umlcompositestate_31, umlfinalstate_32).
startransition(umltransition_39, umlcompositestate_29, umlcompositestate_30).
startransition(umltransition_40, umlcompositestate_30, umlcompositestate_29).
startransition(umltransition_41, umlcompositestate_29, umlcompositestate_29).
startransition(umltransition_42, umlcompositestate_30, umlcompositestate_30).

starposition(umlpseudostate_27, 237, 357).
starposition(umlcompositestate_28, 426, 360).
starposition(umlcompositestate_29, 606, 240).
starposition(umlcompositestate_30, 606, 486).
starposition(umlcompositestate_31, 798, 354).
starposition(umlfinalstate_32, 1009, 355).
starposition(umltransition_33, 308, 356).
starposition(umltransition_34, 513, 299).
starposition(umltransition_35, 516, 422).
starposition(umltransition_36, 702, 296).
starposition(umltransition_37, 699, 419).
starposition(umltransition_38, 923, 353).
starposition(umltransition_39, 604, 362).
starposition(umltransition_40, 604, 362).
starposition(umltransition_41, 656, 209).
starposition(umltransition_42, 656, 455).
