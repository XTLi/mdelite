:-style_check(-discontiguous).

table(gliffy_node,[nodeid,"name",type]).
gliffy_node(0,'initial1',start).
gliffy_node(1,'ready',state).
gliffy_node(2,'eat',state).
gliffy_node(3,'drink',state).
gliffy_node(4,'pig',state).
gliffy_node(5,'finalstate1',stop).

table(gliffy_transition,[transid,startsAt,endsAt]).
gliffy_transition(6,0,1).
gliffy_transition(7,1,2).
gliffy_transition(8,1,3).
gliffy_transition(9,2,4).
gliffy_transition(10,3,4).
gliffy_transition(11,4,5).
gliffy_transition(12,2,3).
gliffy_transition(13,3,2).
gliffy_transition(14,2,2).
gliffy_transition(15,3,3).

table(gliffy_position,[id,x,y]).
gliffy_position(0,237,357).
gliffy_position(1,426,360).
gliffy_position(2,606,240).
gliffy_position(3,606,486).
gliffy_position(4,798,354).
gliffy_position(5,1009,355).
gliffy_position(6,308,356).
gliffy_position(7,513,299).
gliffy_position(8,516,422).
gliffy_position(9,702,296).
gliffy_position(10,699,419).
gliffy_position(11,923,353).
gliffy_position(12,604,362).
gliffy_position(13,604,362).
gliffy_position(14,656,209).
gliffy_position(15,656,455).

