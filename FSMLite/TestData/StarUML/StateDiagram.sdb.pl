:-style_check(-discontiguous).

table(node,[nodeid,"name",type]).
node(umlpseudostate_27,'initial1',start).
node(umlcompositestate_28,'ready',state).
node(umlcompositestate_29,'eat',state).
node(umlcompositestate_30,'drink',state).
node(umlcompositestate_31,'pig',state).
node(umlfinalstate_32,'finalstate1',stop).

table(transition,[transid,startsAt,endsAt]).
transition(umltransition_33,umlpseudostate_27,umlcompositestate_28).
transition(umltransition_34,umlcompositestate_28,umlcompositestate_29).
transition(umltransition_35,umlcompositestate_28,umlcompositestate_30).
transition(umltransition_36,umlcompositestate_29,umlcompositestate_31).
transition(umltransition_37,umlcompositestate_30,umlcompositestate_31).
transition(umltransition_38,umlcompositestate_31,umlfinalstate_32).
transition(umltransition_39,umlcompositestate_29,umlcompositestate_30).
transition(umltransition_40,umlcompositestate_30,umlcompositestate_29).
transition(umltransition_41,umlcompositestate_29,umlcompositestate_29).
transition(umltransition_42,umlcompositestate_30,umlcompositestate_30).

table(position,[id,x,y]).
position(umlpseudostate_27,237,357).
position(umlcompositestate_28,426,360).
position(umlcompositestate_29,606,240).
position(umlcompositestate_30,606,486).
position(umlcompositestate_31,798,354).
position(umlfinalstate_32,1009,355).
position(umltransition_33,308,356).
position(umltransition_34,513,299).
position(umltransition_35,516,422).
position(umltransition_36,702,296).
position(umltransition_37,699,419).
position(umltransition_38,923,353).
position(umltransition_39,604,362).
position(umltransition_40,604,362).
position(umltransition_41,656,209).
position(umltransition_42,656,455).

