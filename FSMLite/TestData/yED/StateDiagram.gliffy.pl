:-style_check(-discontiguous).

table(gliffy_node,[nodeid,"name",type]).
gliffy_node(0,'start',start).
gliffy_node(1,'ready',state).
gliffy_node(2,'drink',state).
gliffy_node(3,'eat',state).
gliffy_node(4,'pig',state).
gliffy_node(5,'end',stop).

table(gliffy_transition,[transid,startsAt,endsAt]).
gliffy_transition(6,0,1).
gliffy_transition(7,1,3).
gliffy_transition(8,3,4).
gliffy_transition(9,4,5).
gliffy_transition(10,1,2).
gliffy_transition(11,2,4).
gliffy_transition(12,3,2).
gliffy_transition(13,2,3).
gliffy_transition(14,3,3).
gliffy_transition(15,2,2).

table(gliffy_position,[id,x,y]).
gliffy_position(0,246,274).
gliffy_position(1,360,274).
gliffy_position(2,461,365).
gliffy_position(3,461,191).
gliffy_position(4,577,279).
gliffy_position(5,692,279).

