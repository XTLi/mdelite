:-style_check(-discontiguous).

table(node,[nodeid,"name",type]).
node(0,'start',start).
node(1,'ready',state).
node(2,'drink',state).
node(3,'eat',state).
node(4,'pig',state).
node(5,'end',stop).

table(transition,[transid,startsAt,endsAt]).
transition(edge0,0,1).
transition(edge1,1,3).
transition(edge2,3,4).
transition(edge3,4,5).
transition(edge4,1,2).
transition(edge5,2,4).
transition(edge6,3,2).
transition(edge7,2,3).
transition(edge8,3,3).
transition(edge9,2,2).

table(position,[id,x,y]).
position(0,246,274).
position(1,360,274).
position(2,461,365).
position(3,461,191).
position(4,577,279).
position(5,692,279).

