:-style_check(-discontiguous).

table(starnode,[nodeid,"name",type]).
starnode(0,'start',start).
starnode(1,'ready',state).
starnode(2,'drink',state).
starnode(3,'eat',state).
starnode(4,'pig',state).
starnode(5,'end',stop).

table(startransition,[transid,startsAt,endsAt]).
startransition(edge0,0,1).
startransition(edge1,1,3).
startransition(edge2,3,4).
startransition(edge3,4,5).
startransition(edge4,1,2).
startransition(edge5,2,4).
startransition(edge6,3,2).
startransition(edge7,2,3).
startransition(edge8,3,3).
startransition(edge9,2,2).

table(starposition,[id,x,y]).
starposition(0,246,274).
starposition(1,360,274).
starposition(2,461,365).
starposition(3,461,191).
starposition(4,577,279).
starposition(5,692,279).

