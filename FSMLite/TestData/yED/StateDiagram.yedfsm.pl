dbase(yedfsm,[yednode,yedtransition]).
table(yednode,[nodeid,"name",type,x,y]).
table(yedtransition,[transid,startsAt,endsAt]).

yednode(0, 'start' ,start, 246, 274).
yednode(1, 'ready' ,state, 360, 274).
yednode(2, 'drink' ,state, 461, 365).
yednode(3, 'eat' ,state, 461, 191).
yednode(4, 'pig' ,state, 577, 279).
yednode(5, 'end' ,stop, 692, 279).

yedtransition(edge0, 0, 1).
yedtransition(edge1, 1, 3).
yedtransition(edge2, 3, 4).
yedtransition(edge3, 4, 5).
yedtransition(edge4, 1, 2).
yedtransition(edge5, 2, 4).
yedtransition(edge6, 3, 2).
yedtransition(edge7, 2, 3).
yedtransition(edge8, 3, 3).
yedtransition(edge9, 2, 2).


