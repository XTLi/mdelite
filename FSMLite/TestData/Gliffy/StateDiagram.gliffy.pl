dbase(gliffy_fsm,[gliffy_node,gliffy_transition,gliffy_position]).
:- dynamic gliffy_node/3.
table(gliffy_node,[nodeid,"name",type]).
gliffy_node(16, 'End', state).
gliffy_node(13, 'pig', state).
gliffy_node(11, 'eat', state).
gliffy_node(8, 'drink', state).
gliffy_node(3, 'ready', state).
gliffy_node(1, 'Start', state).
:- dynamic gliffy_transition/3.
table(gliffy_transition,[transid,startsAt,endsAt]).
gliffy_transition(40, 11, 11).
gliffy_transition(38, 13, 16).
gliffy_transition(35, 8, 8).
gliffy_transition(33, 8, 11).
gliffy_transition(31, 11, 8).
gliffy_transition(29, 8, 13).
gliffy_transition(28, 11, 13).
gliffy_transition(24, 3, 8).
gliffy_transition(21, 3, 11).
gliffy_transition(19, 1, 3).
:- dynamic gliffy_position/3.
table(gliffy_position,[id,x,y]).
gliffy_position(16, 1260, 389).
gliffy_position(13, 1010, 402).
gliffy_position(11, 770, 244).
gliffy_position(8, 770, 514).
gliffy_position(3, 540, 402).
gliffy_position(1, 310, 389).
