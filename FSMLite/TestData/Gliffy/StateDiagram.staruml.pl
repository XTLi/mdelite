:-style_check(-discontiguous).

table(starnode,[nodeid,"name",type]).
starnode(16,'End',state).
starnode(13,'pig',state).
starnode(11,'eat',state).
starnode(8,'drink',state).
starnode(3,'ready',state).
starnode(1,'Start',state).

table(startransition,[transid,startsAt,endsAt]).
startransition(40,11,11).
startransition(38,13,16).
startransition(35,8,8).
startransition(33,8,11).
startransition(31,11,8).
startransition(29,8,13).
startransition(28,11,13).
startransition(24,3,8).
startransition(21,3,11).
startransition(19,1,3).

table(starposition,[id,x,y]).
starposition(16,1260,389).
starposition(13,1010,402).
starposition(11,770,244).
starposition(8,770,514).
starposition(3,540,402).
starposition(1,310,389).

