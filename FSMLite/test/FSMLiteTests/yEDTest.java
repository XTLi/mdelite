package FSMLiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class yEDTest {
    
    private static String fileName = "yED/StateDiagram";
    
    public yEDTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
    public void theWork(String filename, String to, String fileType) {
         String result = filename + fileType;
         String[] args = {"yed", to, filename};
         FSMLite.Convert.main(args);
         RegTest.Utility.validate(result, "Correct/"+result,false);
     }

    @Test
    public void test1() {
        theWork("TestData/"+fileName, "staruml", ".xml");
    }
    
    @Test
    public void test2() {
        theWork("TestData/"+fileName, "gliffy", ".gliffy");
    }

    @Test
    public void test4() {
        theWork("TestData/"+fileName, "umbrello", ".xmi");
    }    

}
