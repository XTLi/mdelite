package FSMLiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConformTest {
    
    public ConformTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    public void theWork(String toolName) {
        String fileName = "TestData/"+toolName+"/StateDiagram";
        String[] args = {toolName.toLowerCase(), fileName};
        FSMLite.Conform.main(args);
    }

    @Test
    public void test1() {
        theWork("StarUML");
    }
    
    @Test
    public void test2() {
        theWork("Gliffy");
    }
    
    @Test
    public void test3() {
        theWork("yED");
    }

    @Test
    public void test4() {
        theWork("Umbrello");
    }    

}
