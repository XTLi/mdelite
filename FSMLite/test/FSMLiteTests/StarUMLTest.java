package FSMLiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StarUMLTest {
    
    private static String fileName = "StarUML/StateDiagram";
    
    public StarUMLTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    public void theWork(String filename, String to, String fileType) {
         String result = filename + fileType;
         String[] args = {"staruml", to, filename};
         FSMLite.Convert.main(args);
         RegTest.Utility.validate(result, "Correct/"+result,false);
     }

    @Test
    public void test2() {
        theWork("TestData/"+fileName, "gliffy", ".gliffy");
    }

    @Test
    public void test3() {
        theWork("TestData/"+fileName, "yed", ".xgml");
    }

    @Test
    public void test4() {
        theWork("TestData/"+fileName, "umbrello", ".xmi");
    }    

}
