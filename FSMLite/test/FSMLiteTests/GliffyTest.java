package FSMLiteTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GliffyTest {
    
    private static String fileName = "Gliffy/StateDiagram";

    public GliffyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    public void theWork(String filename, String to, String fileType) {
         String result = filename + fileType;
         String[] args = {"gliffy", to, filename};
         FSMLite.Convert.main(args);
         RegTest.Utility.validate(result, "Correct/"+result,false);
     }

    @Test
    public void test1() {
        theWork("TestData/"+fileName, "staruml", ".xml");
    }
    
    @Test
    public void test3() {
        theWork("TestData/"+fileName, "yed", ".xgml");
    }

    @Test
    public void test4() {
        theWork("TestData/"+fileName, "umbrello", ".xmi");
    }    

}
