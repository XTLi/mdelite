package FSMLite;

import CoreMDELite.*;

public class Gliffy_FSMpl extends GProlog {

    @Override
    public String fileType() {
        return ".gliffy.pl";
    }

    @Override
    public String partialFileType() {
        return ".gliffy";
    }

    public Gliffy_FSMpl(String filename) {
        super(filename);
    }

    public Gliffy_FSMpl(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public Gliffy toGliffy() {
        return toGliffy("");
    }

    public Gliffy toGliffy(String extra) {
        Gliffy result = new Gliffy(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/gliffy_fsm.vm");
        return result;
    }

    public SDB toSDB() {
        return toSDB("");
    }

    public SDB toSDB(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/gliffy2fsm.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/sdb.schema.pl",
            HomePath.homePath + "libpl/sdb.run.pl",
            this.fullName};
        Gliffy_FSMpl tmp = new Gliffy_FSMpl("tmp", list);
        SDB result = new SDB(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/gliffy_fsm.conform.pl"};
        Gliffy_FSMpl temp = new Gliffy_FSMpl("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
