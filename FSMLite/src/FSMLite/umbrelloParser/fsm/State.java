/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FSMLite.umbrelloParser.fsm;

/**
 *
 * @author troy
 */
public class State {
    private String id;
    private String name;
    private String type;
    private String quote = "\'";
    
    public State(String id, String name, String type){
        this.id = quote + "i" + id.toLowerCase() + quote;
        this.name = name;
        this.type = type;
    }
    
    public String getId(){
        return this.id;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getType(){
        return this.type;
    }
}
