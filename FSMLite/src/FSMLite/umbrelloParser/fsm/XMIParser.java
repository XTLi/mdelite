        
package FSMLite.umbrelloParser.fsm;

import java.util.Iterator;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 *
 * @author troy
 */
public class XMIParser {
    
    public Document parser(String path) throws DocumentException {
        SAXReader reader  = new SAXReader(); 
        Document document = reader.read(path);
       
        return document;
        
        //Node start = document.selectSingleNode("//book[1]");
        //System.out.println(start.getName());
        //String name = start.selectSingleNode("title").getText();
        //System.out.println(name);
                
    }
        
    public StateDiagram createDiagram(Document doc){
        StateDiagram s = new StateDiagram();
        
        //add Start State
        List startNodes = doc.selectNodes("//statewidget[@statetype='0']");
        for (Iterator iter = startNodes.iterator(); iter.hasNext(); ) {
            Node startNode = (Node)iter.next();
            State start = Node2State(startNode,"start");
            Position pos = Node2Position(startNode);
            s.addState(start);
            s.addPosition(pos);
        }
        
        //add Normal State
        List stateNodes = doc.selectNodes("//statewidget[@statetype='1']");
        for (Iterator iter = stateNodes.iterator(); iter.hasNext(); ) {
            Node node = (Node)iter.next();
            State state = Node2State(node, "state"); 
            Position pos = Node2Position(node);
            s.addState(state);
            s.addPosition(pos);
        }
        
        //add Final State
        List finalNodes = doc.selectNodes("//statewidget[@statetype='2']");
        for (Iterator iter = finalNodes.iterator(); iter.hasNext(); ) {
            Node finalNode = (Node)iter.next();
            State fin = Node2State(finalNode, "stop");
            Position pos = Node2Position(finalNode);
            s.addState(fin);
            s.addPosition(pos);
        }
        
        //add transitions
        List transNodes = doc.selectNodes("//assocwidget[@type='514']");
        int i = 0;
        for (Iterator iter = transNodes.iterator(); iter.hasNext(); ) {
            Node node = (Node)iter.next();
            Transition trans = (Transition)Node2Trans(node, i++); 
            s.addTransition(trans);
        }
        
        return s;
    }
    
    public State Node2State(Node n, String type){
        String id = n.valueOf("@xmi.id");
        String name = null;
        
        switch(type) {
            case "start": name = "start"; break;
            case "state": name = n.valueOf("@statename").toLowerCase().replaceAll("\\s",""); break;
            case "stop": name = "stop"; break;
            default : System.out.println("Umbrello Xmi File has fatal error!"); System.exit(-1);
        }
        
        return new State(id, name, type);
    }
    
    public Position Node2Position(Node n){
        String id = n.valueOf("@xmi.id");
        String x = n.valueOf("@x");
        String y = n.valueOf("@y");
        
        Position pos = new Position(id, x, y);
        
        return pos;
    }
    
    public Transition Node2Trans(Node n, int i){
        String id = "t" + i;
        String startsAt = n.valueOf("@widgetaid");
        String endsAt = n.valueOf("@widgetbid");
        
        return new Transition(id, startsAt, endsAt);
    }
}
