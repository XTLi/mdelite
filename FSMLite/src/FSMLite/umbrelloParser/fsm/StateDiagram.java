/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FSMLite.umbrelloParser.fsm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author troy
 */
public class StateDiagram {
    public ArrayList<State> states ;
    public ArrayList<Transition> trans ;
    public ArrayList<Position> positions;
    
    public StateDiagram(){
        this.states = new ArrayList();
        this.trans = new ArrayList(); 
        this.positions = new ArrayList<Position>();
    }
    
    public void addState(State s){
        states.add(s);
    }
    
    public void addTransition(Transition e){
        trans.add(e);
    }
    
    public void addPosition(Position p){
        positions.add(p);
    }
    
    public void generateVioletDB(String target) {
        try {
            FileWriter fstream = new FileWriter(target);
            BufferedWriter out = new BufferedWriter(fstream);

            out.write(":-style_check(-discontiguous).\n");

            //Output database definition
            out.write("dbase(sdb,[node,transition,position]).\n");

            //Ouput Nodes in node table 
            out.write("\ntable(node,[nodeid,\"name\",type]).\n");
            ArrayList<State> states = this.states;
            if (states.size() > 0) {
                for (State s : states) {
                    out.write("node(" + s.getId() + "," + "\'" + s.getName() + "\'" + "," + s.getType() + ").\n");
                }
            }else out.write(":- dynamic node/3.\n");

            //Ouput Transitions in transition table 
            out.write("\ntable(transition,[transid,startsAt,endsAt]).\n");
            ArrayList<Transition> trans = this.trans;
            if (trans.size() > 0) {
                for (Transition t : trans) {
                    out.write("transition(" + t.getId() + "," + t.getStartsAt()+ "," + t.getEndsAt() + ").\n");
                }
            }else out.write(":- dynamic transition/3.\n");
            
            //Output Positions in position table
            out.write("\ntable(position,[id,x,y]).\n");
            
            if(positions.size() > 0){
                for(Position p : positions){
                    out.write("position(" + p.id + "," + p.x + "," + p.y + ").\n");
                }
            }else out.write(":- dynamic position/3.\n");

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
