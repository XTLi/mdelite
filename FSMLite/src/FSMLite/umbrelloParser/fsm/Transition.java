/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FSMLite.umbrelloParser.fsm;

/**
 *
 * @author troy
 */
public class Transition {
    private String id;
    private String startsAt;
    private String endsAt;
    
    public Transition(String id, String startsAt, String endsAt){
        this.id = "\'i" + id.toLowerCase() + "\'";
        this.startsAt = "\'i" + startsAt.toLowerCase() + "\'";
        this.endsAt = "\'i" + endsAt.toLowerCase() + "\'";
    }
    
    public String getId(){
        return this.id;
    }
    
    public String getStartsAt(){
        return this.startsAt;
    }
    
    public String getEndsAt(){
        return this.endsAt;
    }
}
