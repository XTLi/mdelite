/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FSMLite.umbrelloParser.fsm;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;

/**
 *
 * @author troy
 */
public class Main {
    
    public static void main(String[] args) {
        try {
            String input_File = args[0]+".xmi";
            //String input_File = "/home/troy/2013Fall/FOP/FinalProject/state_test1.xmi";
            XMIParser v = new XMIParser();
            Document doc = v.parser(input_File);
            
            //Create State Diagram based on DOM tree
            StateDiagram d = v.createDiagram(doc);
            
            //Create prolog database
            //String output_File = input_File.split("\\.")[0] + ".pl";
            String output_File = args[0] + ".sdb.pl";
            d.generateVioletDB(output_File);
            
    //        //Concult prolog constraints
    //        String arguments = "consult(" + output_File.split("\\.")[0] + "),consult(checker),run,halt.";
    //        Process p = new ProcessBuilder("swipl", "-g", arguments).start();
    //        
    //        String s;
    //        Boolean error = false;
    //        BufferedReader stdInput = new BufferedReader(new 
    //             InputStreamReader(p.getInputStream()));
    //
    //        BufferedReader stdError = new BufferedReader(new 
    //             InputStreamReader(p.getErrorStream()));
    //
    //        // read the output from the command
    //        System.out.println("Here is the standard output of the command:\n");
    //        while ((s = stdInput.readLine()) != null) {
    //            System.out.println(s);
    //        }
    //
    //        // read any errors from the attempted command
    //        System.out.println("Here is the standard error of the command (if any):\n");
    //        while ((s = stdError.readLine()) != null) {
    //            System.out.println(s);
    //            error = true;
    //        }
    //        
    //        //If constriants fail, then exit
    //        if(error == true){
    //            System.exit(-1);
    //        }
    //        
    //        //Use MDELite to generate code 
    //        CoreMDELite.vm2t.Main.main(args);
    //        CoreMDELite.vm2t.Main.main(args);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
