/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FSMLite.starumlparser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 *
 * @author marknv
 */
public class KlassParser {

    public static void generatePL(String filename) throws DocumentException, IOException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(filename + ".xml");
        StringBuilder sb = new StringBuilder();

        // declare db and tables
        sb.append("dbase(staruml,[starclass,starinterface,starassociation,stargeneralization,starimpl]).\n");
        sb.append("table(starclass,[id,\"name\",\"fields\",\"methods\",superid]).\n");
        sb.append("table(starinterface,[id,\"name\",\"methods\"]).\n");
        sb.append("table(starassociation,[id,cid1,eid1,\"role1\",aggr1,cid2,eid2,\"role2\",aggr2]).\n");
        sb.append("table(stargeneralization,[id,child,parent,type]).\n");
        sb.append("table(starimpl,[id,client,supplier]).\n");
        sb.append("table(starposition,[id,x,y]).\n");

        sb.append("\n");

        // building class predicates
        List<Element> classes = document.selectNodes("//UML:Class");
        if (classes.isEmpty()) {
            sb.append(":- dynamic starclass/5.\n");

        }
        for (Element cls : classes) {
            String id = idParse(cls.valueOf("@xmi.id"));
            String name = quotes(cls.valueOf("@name"));
            String superId = cls.valueOf("@generalization");
            if (superId.equalsIgnoreCase("")) {
                superId = "null";
            } else {
                superId = ((Element) cls.selectNodes("//UML:Generalization[@xmi.id='" + superId + "']").get(0)).valueOf("@parent");
                superId = idParse(superId);
            }

            // Attributes
            StringBuilder attrnames = buildAttributes(cls);

            // Operations
            StringBuilder methodnames = buildMethods(cls);

            // predicate
            String nodePredicate = "starclass" + "(" + id + ", " + name + ", " + quotes(attrnames) + ", " + quotes(methodnames) + ", " + superId + ")" + "." + "\n";
            sb.append(nodePredicate);
        }
        sb.append("\n");

        // building interfaces predicates
        List<Element> interfaces = document.selectNodes("//UML:Interface");
        if (interfaces.isEmpty()) {
            sb.append(":- dynamic starinterface/3.\n");
        }
        for (Element face : interfaces) {
            String id = idParse(face.valueOf("@xmi.id"));
            String name = quotes(face.valueOf("@name"));

            // Operations
            StringBuilder methodnames = buildMethods(face);

            // predicate
            String interfacePredicate = "starinterface" + "(" + id + ", " + name + ", " + quotes(methodnames) + ")" + "." + "\n";
            sb.append(interfacePredicate);
        }
        sb.append("\n");

        // building transition predicates
        List<Element> associations = document.selectNodes("//UML:Association");
        if (associations.isEmpty()) {
            sb.append(":- dynamic starassociation/9.\n");

        }
        for (Element as : associations) {
            String id = idParse(as.valueOf("@xmi.id"));

            // two ends
            List<Element> ends = as.selectNodes(".//UML:AssociationEnd");
            String[] cids = new String[2];
            String[] eids = new String[2];
            String[] roles = new String[2];
            String[] aggrs = new String[2];
            int ci = 0;
            for (Element end : ends) {
                cids[ci] = idParse(end.valueOf("@type"));
                aggrs[ci] = end.valueOf("@aggregation").toLowerCase();
                eids[ci] = idParse(end.valueOf("@xmi.id"));
                String roleName = end.valueOf("@name");
                if (!"".equalsIgnoreCase(roleName)) {
                    String cardLow = ((Element) end.selectNodes(".//UML:MultiplicityRange").get(0)).valueOf("@lower").replace(" ", "");
                    String cardUp = ((Element) end.selectNodes(".//UML:MultiplicityRange").get(0)).valueOf("@upper").replace(" ", "");
                    String card = buildCard(cardLow, cardUp);
                    if (!"".equalsIgnoreCase(card)) {
                        roles[ci] = quotes(roleName + " " + card);
                    }
                } else {
                    roles[ci] = "''";
                }
                ci++;
            }
            String transitionPredicate = "starassociation" + "(" + id + ", " + cids[0] + ", " + eids[0] + ", " + roles[0] + ", " + aggrs[0] + ", " + cids[1] + ", " + eids[1] + ", " + roles[1] + ", " + aggrs[1] + ")" + "." + "\n";
            sb.append(transitionPredicate);
        }
        sb.append("\n");

        // Generalization
        List<Element> generalizations = document.selectNodes("//UML:Generalization");
        if (generalizations.isEmpty()) {
            sb.append(":- dynamic stargeneralization/3.\n");

        }
        for (Element gen : generalizations) {
            String id = idParse(gen.valueOf("@xmi.id"));
            String child = idParse(gen.valueOf("@child"));
            String parent = idParse(gen.valueOf("@parent"));
            String type = "";
            if (child.toLowerCase().contains("interface")) {
                type = "interface";
            } else {
                type = "class";
            }
            String genPredicate = "stargeneralization" + "(" + id + ", " + child + ", " + parent + "," + type + ")" + "." + "\n";
            sb.append(genPredicate);
        }
        sb.append("\n");

        // Impl
        List<Element> impls = document.selectNodes("//UML:Abstraction");
        if (impls.isEmpty()) {
            sb.append(":- dynamic starimpl/3.\n");

        }
        for (Element impl : impls) {
            String id = idParse(impl.valueOf("@xmi.id"));
            String client = idParse(impl.valueOf("@client"));
            String supplier = idParse(impl.valueOf("@supplier"));
            String genPredicate = "starimpl" + "(" + id + ", " + client + ", " + supplier + ")" + "." + "\n";
            sb.append(genPredicate);
        }
        sb.append("\n");

        // Position
        List<Element> positions = document.selectNodes("//UML:DiagramElement");
        if (positions.isEmpty()) {
            sb.append(":- dynamic starposition/3.\n");
        }
        for (Element position : positions) {
            String id = idParse(position.valueOf("@subject"));
            String x = position.valueOf("@geometry").split(",")[0].trim();
            String y = position.valueOf("@geometry").split(",")[1].trim();
            String genPredicate = "starposition" + "(" + id + ", " + x + ", " + y + ")" + "." + "\n";
            sb.append(genPredicate);

        }

        // Write File
        writeFile(filename, sb.toString());
    }

    private static void writeFile(String fileName, String content) throws IOException {
        File file = new File(fileName + ".staruml.pl");
        if (!file.exists()) {
            file.createNewFile();
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(content);
            bufferedWriter.flush();
        }
    }

    // Utils
    private static StringBuilder buildAttributes(Element e) {
        StringBuilder attrnames = new StringBuilder();

        List<Element> attrs = e.selectNodes(".//UML:Attribute");
        for (Element attr : attrs) {
            attrnames.append(";");
            if (attr.valueOf("@visibility").equalsIgnoreCase("private")) {
                attrnames.append("-").append(attr.valueOf("@name"));
            } else {
                attrnames.append("+").append(attr.valueOf("@name"));
            }

        }
        if (!attrnames.toString().equalsIgnoreCase("")) {
            attrnames.deleteCharAt(0);
        }
        return attrnames;
    }

    private static StringBuilder buildMethods(Element e) {
        StringBuilder methodnames = new StringBuilder();
        List<Element> methods = e.selectNodes(".//UML:Operation");
        for (Element method : methods) {

            // separator
            methodnames.append(";");

            // visibility
            if (method.valueOf("@visibility").equalsIgnoreCase("private")) {
                methodnames.append("-");
            } else {
                methodnames.append("+");
            }

            // method name
            methodnames.append(method.valueOf("@name"));
            methodnames.append("(");

            // params
            List<Element> params = e.selectNodes(".//UML:Parameter");
            for (Element param : params) {
                methodnames.append(param.valueOf("@name"));
                methodnames.append(",");
            }
            if (methodnames.charAt(methodnames.length()-1) == ',' && !methodnames.toString().equalsIgnoreCase("")) {
                methodnames.deleteCharAt(methodnames.length() - 1);
            }
            // end
            methodnames.append(")");
        }
        if (!methodnames.toString().equalsIgnoreCase("")) {
            methodnames.deleteCharAt(0);
        }
        return methodnames;
    }

    private static String buildCard(String low, String up) {
        String card;
        if ("-1".equalsIgnoreCase(low)) {
            low = "*";
        }
        if ("-1".equalsIgnoreCase(up)) {
            up = "*";
        }
        if ("".equalsIgnoreCase(low)) {
            card = up;
        } else if ("".equalsIgnoreCase(up)) {
            card = low;
        } else {
            card = low + ".." + up;
        }
        return card;
    }

    protected static String idParse(String str) {
        return str.toLowerCase().replace('.', '_');
    }

    protected static String quotes(String str) {
        return "'" + str + "'";
    }

    protected static String quotes(StringBuilder str) {
        return "'" + str + "'";
    }
}