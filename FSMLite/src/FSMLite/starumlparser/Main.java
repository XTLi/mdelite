/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FSMLite.starumlparser;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.DocumentException;

/**
 *
 * @author marknv
 */
public class Main {
    
    public static void marquee() {
        System.out.println("Usage: starUMLParser <StarUML xml_file>");
        System.exit(1);
    }
    
    public static void main(String[] args){
        if (args.length != 1) {
            marquee();
        }
        try {
            FSMParser.generatePL(args[0]);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
