package FSMLite.gliffy;

class UmlInterface extends UmlObject{
	public String methods;
	
	public UmlInterface(String id, String name, String methods){
		this.id = id;
		this.name = name;
		this.methods = methods;
	}
	
	public String toProlog(){
		return "gliffy_interface(" + id + ", " + quote(name) + ", " + quote(methods) +").\n";
	}
}