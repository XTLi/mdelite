package FSMLite.gliffy;

class Position {
	public String id;
	public int x,y;
	
	public Position(String id, int x, int y){
		this.id=id;
		this.x=x;
		this.y=y;
	}
	
	public String toProlog(){
		return "gliffy_position(" + id + ", " + x + ", " + y + ").\n";
	}
}