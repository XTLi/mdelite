package FSMLite.gliffy;

class Association extends UmlAssociation {
	public String startRole;
	public boolean startArrow;
	public String endRole;
	public boolean endArrow;
	
	public Association(String id, String startId, String startRole, boolean startArrow, String endId, String endRole, boolean endArrow){
		this.id = id;
		this.startId = startId;
		this.startRole = startRole;
		this.startArrow = startArrow;
		this.endId = endId;
		this.endRole = endRole;
		this.endArrow = endArrow;
	}
	
	public String toProlog(){
		return "gliffy_association(" + id + ", " + startId + ", " + quote(startRole) + ", " + startArrow + ", " + endId + ", " + quote(endRole) + ", " + endArrow + ").\n";
	}
}