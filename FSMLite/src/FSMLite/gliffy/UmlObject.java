package FSMLite.gliffy;

abstract class UmlObject {
	public String id;
	public String name;
	
	protected String quote(String s){
		return "'" + s + "'";
	}
	
	public abstract String toProlog();
}