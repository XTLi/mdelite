package FSMLite.gliffy;

abstract class UmlAssociation {
	public String id;
	public String startId;
	public String endId;
	
	protected String quote(String s){
		return "'" + s + "'";
	}
	
	public abstract String toProlog();
}