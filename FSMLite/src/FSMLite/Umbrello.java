package FSMLite;

import CoreMDELite.*;

public class Umbrello extends MDELiteObject {

    @Override
    public String fileType() {
        return ".xmi";
    }

    @Override
    public String partialFileType() {
        return "";
    }

    public Umbrello(String filename) {
        super(filename);
    }

    /* the following are transformations */

    public SDB toSDB() {
        SDB result = new SDB(filename);
        String[] args = { partialName };
        FSMLite.umbrelloParser.fsm.Main.main(args);
        result.conform();
        return result;
    }

}
