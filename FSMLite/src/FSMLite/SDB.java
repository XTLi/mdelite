package FSMLite;

import CoreMDELite.*;

public class SDB extends GProlog {

    @Override
    public String fileType() {
        return ".sdb.pl";
    }

    @Override
    public String partialFileType() {
        return ".sdb";
    }

    public SDB(String filename) {
        super(filename);
    }

    public SDB(String filename, String[] files) {
        super(filename, files);
    }

    /* the following are transformations */

    public StarUMLpl toStarUMLpl() {
        return toStarUMLpl("");
    }

    public StarUMLpl toStarUMLpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/sdb2staruml.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/staruml.schema.pl",
            HomePath.homePath + "libpl/staruml.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        StarUMLpl result = new StarUMLpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public Umbrello toUmbrello() {
        return toUmbrello("");
    }

    public Umbrello toUmbrello(String extra) {
        Umbrello result = new Umbrello(filename + extra);
        invokeVm2t(result, HomePath.homePath + "libvm/FSM2XMI.vm");
        return result;
    }

    public Gliffy_FSMpl toGliffy_FSMpl() {
        return toGliffy_FSMpl("");
    }

    public Gliffy_FSMpl toGliffy_FSMpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/fsm2gliffy.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/gliffy_fsm.schema.pl",
            HomePath.homePath + "libpl/gliffy_fsm.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        Gliffy_FSMpl result = new Gliffy_FSMpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    public yEDFSMpl toyEDFSMpl() {
        return toyEDFSMpl("");
    }

    public yEDFSMpl toyEDFSMpl(String extra) {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl", 
            HomePath.homePath + "libpl/fsm2yed.pl",
            HomePath.homePath + "libpl/print.pl",
            HomePath.homePath + "libpl/yedfsm.schema.pl",
            HomePath.homePath + "libpl/yedfsm.run.pl",
            this.fullName};
        SDB tmp = new SDB("tmp", list);
        yEDFSMpl result = new yEDFSMpl(filename+extra);
        tmp.executeProlog(result);
        result.conform();
        tmp.delete();
        return result;
    }

    @Override
    public void conform() {
        String[] list = {HomePath.homePath + "libpl/discontiguous.pl",
            fullName, HomePath.homePath + "libpl/conform.pl", HomePath.homePath + "libpl/sdb.conform.pl"};
        SDB temp = new SDB("tmp", list);
        temp.executeProlog();
        temp.delete();
    }

}
