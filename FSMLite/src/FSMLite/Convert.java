package FSMLite;

public class Convert {

        public static void marquee() {
            System.out.println("Usage: Convert <from> <to> <filename(not including dot extension)>");
            System.out.println("       <from> <to> := staruml umbrello");
            System.out.println("                    | staruml gliffy");
            System.out.println("                    | staruml yed");
            System.out.println("                    | umbrello staruml");
            System.out.println("                    | umbrello gliffy");
            System.out.println("                    | umbrello yed");
            System.out.println("                    | gliffy staruml");
            System.out.println("                    | gliffy umbrello");
            System.out.println("                    | gliffy yed");
            System.out.println("                    | yed staruml");
            System.out.println("                    | yed umbrello");
            System.out.println("                    | yed gliffy");
            System.exit(1);
        }
        
        public static void main(String args[]) {
            if (args.length!=3)
                marquee();
            args[0] = args[0].toLowerCase();
            args[1] = args[1].toLowerCase();
            if (args[0].equals("staruml") && args[1].equals("umbrello")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("staruml") && args[1].equals("gliffy")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toGliffy_FSMpl().toGliffy();
                return;
            }
            if (args[0].equals("staruml") && args[1].equals("yed")) {
                StarUML staruml = new StarUML(args[2]);
                staruml.toStarUMLpl().toSDB().toyEDFSMpl().toyED();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("staruml")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("gliffy")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toGliffy_FSMpl().toGliffy();
                return;
            }
            if (args[0].equals("umbrello") && args[1].equals("yed")) {
                Umbrello umbrello = new Umbrello(args[2]);
                umbrello.toSDB().toyEDFSMpl().toyED();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("staruml")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_FSMpl().toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("umbrello")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_FSMpl().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("gliffy") && args[1].equals("yed")) {
                Gliffy gliffy = new Gliffy(args[2]);
                gliffy.toGliffy_FSMpl().toSDB().toyEDFSMpl().toyED();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("staruml")) {
                yED yed = new yED(args[2]);
                yed.toyEDFSMpl().toSDB().toStarUMLpl().toStarUML();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("umbrello")) {
                yED yed = new yED(args[2]);
                yed.toyEDFSMpl().toSDB().toUmbrello();
                return;
            }
            if (args[0].equals("yed") && args[1].equals("gliffy")) {
                yED yed = new yED(args[2]);
                yed.toyEDFSMpl().toSDB().toGliffy_FSMpl().toGliffy();
                return;
            }
            System.err.println("unsupported transformation from " + args[0] + "to " + args[1]);
    }
        
}
