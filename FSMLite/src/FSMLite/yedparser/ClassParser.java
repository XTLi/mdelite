/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FSMLite.yedparser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 *
 * @author Hang
 */
public class ClassParser {

    /**
     * first declare database table
     * @param fileName
     * @throws DocumentException
     * @throws IOException 
     */
    public static void generatePL(String fileName) throws DocumentException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("dbase(yeduml,[yedclass,yedinterface,yedassociation,yedImplements,yedInterfaceExtends]).\n");
        sb.append("table(yedclass,[id,\"name\",\"fields\",\"methods\",superid,x,y]).\n");
        sb.append("table(yedinterface,[id,\"name\",\"methods\",x,y]).\n");
        sb.append("table(yedassociation,[cid1, \"role1\",arrow1,cid2,\"role2\",arrow2]).\n");
        sb.append("table(yedImplements,[cid,iid]).\n");
        sb.append("table(yedInterfaceExtends,[id,idx]).\n");
        sb.append("\n");

        SAXReader reader = new SAXReader();
        Document document = reader.read(fileName + ".xgml");

        // class information
        List<Node> nodes = document.selectNodes("//section[@name='node']");
        
        ArrayList<String> classArr          = new ArrayList<String>();
        ArrayList<String> interfaceArr      = new ArrayList<String>();
        ArrayList<String> associationArr    = new ArrayList<String>();
        ArrayList<String> implementArr      = new ArrayList<String>();
        ArrayList<String> interfaceExtendArr = new ArrayList<String>();
        
        // the key is the id for the node, true is class, false is interface
        HashMap<String, Boolean> classTypeMap = new HashMap<String, Boolean>();
        
                
        if (nodes.isEmpty()) {
            sb.append(":- dynamic yedclass/5.\n");
        } else {
            for(Node node : nodes) {
                // node can be either class or interface
                boolean isInterface = false;
                
                List<Node> attributes = node.selectNodes("./attribute");
                String id   = attributes.get(0).getText();
                String name = "'" + attributes.get(1).getText() + "'";
                String superid = "null";
                
                Node graphics = node.selectSingleNode("./section[@name='graphics']");
                String xpos = graphics.selectSingleNode("./attribute[@key='x']").getText();
                String ypos = graphics.selectSingleNode("./attribute[@key='y']").getText();
                
                String nodeType = graphics.selectSingleNode("./attribute[@key='customconfiguration']").getText();
                if(nodeType.equals("com.yworks.flowchart.start2")) {
                    isInterface = true;
                }
                
                // id name fields methods superid
                
                List<Node> labels = node.selectNodes("./section[@name='LabelGraphics']");
                String fields   = "";
                String methods  = "";
                String fNm = "";    // fields and methods
                for(Node label : labels) {
                    List<Node> labelAttributes = label.selectNodes("./attribute[@key='text']");
                    String temp = labelAttributes.get(0).getText();
                    if(temp.equals(name)) {
                        continue;
                    } else {
                        fNm = temp;
                    }
                }
                
                if(fNm.length()!=0) {
                    String []ss = fNm.split("\n");
                    for(String s : ss) {
                        if(s.indexOf("(")==-1) {
                            fields  += s + " ";
                        } else {
                            methods += s + " ";
                        }
                    }
                }
                fields  = "'" + fields + "'";
                methods = "'" + methods + "'";
                classTypeMap.put(id, !isInterface);
                if(!isInterface) {
                    String nodeRow = "yedclass" + "(" + id + ", " + name + ", " + fields + ", " + methods + ", " + superid + ", " + xpos + ", " + ypos + ")" + "." + "\n";
                    classArr.add(nodeRow);
                } else {
                    String nodeRow = "yedinterface" + "(" + id + ", " + name + ", " + methods + ", " + xpos + ", " + ypos + ")" + "." + "\n";
                    interfaceArr.add(nodeRow);
                }
            }
        }
        
        List<Node> associations = document.selectNodes("//section[@name='edge']");
        // sb.append("table(yedassociation,[cid1, \"role1\",arrow1,cid2,\"role2\",arrow2]).\n");
        for(Node association : associations) {
            List<Node> attributes = association.selectNodes("./attribute");
            String cid1 = attributes.get(0).getText();
            String cid2 = attributes.get(1).getText();
            String role1 = "";
            String role2 = "";
            List<Node> roles = association.selectNodes("./section[@name='LabelGraphics']");
            if(roles.size() >= 2) {
                role1 = roles.get(0).selectSingleNode("./attribute[@key='text']").getText();
                role2 = roles.get(1).selectSingleNode("./attribute[@key='text']").getText();
            }        
            role1 = "'" + role1 + "'";
            role2 = "'" + role2 + "'";
            String arrow1 = "none";
            String arrow2 = "none";
            Node graphics = association.selectSingleNode("./section[@name='graphics']");
            if(graphics!=null) {
                Node sourceArrow = graphics.selectSingleNode("attribute[@key='sourceArrow']");
                if(sourceArrow!=null) {
                    arrow1 = sourceArrow.getText();
                }
                Node targetArrow = graphics.selectSingleNode("attribute[@key='targetArrow']");
                if(targetArrow!=null) {
                    arrow2 = targetArrow.getText();
                }
            }
            
            if(!classTypeMap.get(cid1) && !classTypeMap.get(cid2)) {   // interfaceExtend
                String interfaceExtend = "yedInterfaceExtends" + "(" + cid1 + ", " + cid2 + ")" + "." + "\n"; 
                interfaceExtendArr.add(interfaceExtend);
            } else if(classTypeMap.get(cid1) && !classTypeMap.get(cid2)) {  // implements
                String implement = "yedImplements" + "(" + cid1 + ", " + cid2 + ")" + "." + "\n";
                implementArr.add(implement);
            } else {
                String yedassociation = "yedassociation" + "(" + cid1 + ", " + role1 + ", " + arrow1 + ", " + cid2 + ", " + role2 + ", " + arrow2  + ")" + "." + "\n";
                associationArr.add(yedassociation);
            }
        }
        
        if(classArr.isEmpty()) {
            sb.append(":- dynamic yedclass/5.\n");
        } else {
            for(String s : classArr) {
                sb.append(s);
            }
        }
        sb.append("\n");
        
        if(interfaceArr.isEmpty()) {
            sb.append(":- dynamic yedinterface/3.\n");
        } else {
            for(String s : interfaceArr) {
                sb.append(s);
            }
        }
       sb.append("\n");
        
       if(associationArr.isEmpty()) {
           sb.append(":- dynamic yedassociation/6.\n");
       } else {
           for(String s : associationArr) {
               sb.append(s);
           }
       }
       sb.append("\n");
       
       if(implementArr.isEmpty()) {
           sb.append(":- dynamic yedImplements/2.\n");
       } else {
           for(String s : implementArr) {
               sb.append(s);
           }
       }
       sb.append("\n");

       if(interfaceExtendArr.isEmpty()) {
           sb.append(":- dynamic yedInterfaceExtends/2.\n");
       } else {
           for(String s : interfaceExtendArr) {
               sb.append(s);
           }
       }
       sb.append("\n");
       
       writeFile(fileName, sb.toString());
    }

    private static void writeFile(String fileName, String content) throws IOException {
        File file = new File(fileName + ".yeduml.pl");
        if (!file.exists()) {
            file.createNewFile();
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(content);
            bufferedWriter.flush();
        }
    }
}


