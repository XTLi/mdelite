/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FSMLite.yedparser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 *
 * @author Hang
 */
public class FSMParser {
     public static void generatePL(String fileName) throws DocumentException, IOException {
        
         // first output the database and corresponding tables
        StringBuilder sb = new StringBuilder();
        sb.append("dbase(yedfsm,[yednode,yedtransition]).\n");
        sb.append("table(yednode,[nodeid,\"name\",type,x,y]).\n");
        sb.append("table(yedtransition,[transid,startsAt,endsAt]).\n");
//        sb.append("table(yedposition,[id,x,y]).\n");
        sb.append("\n");

        SAXReader reader = new SAXReader();
        Document document = reader.read(fileName + ".xgml");
//        ArrayList<String> posInfo = new ArrayList<String>();
        
        List<Node> nodes = document.selectNodes("//section[@name='node']");
        for (Node node : nodes) {
            List<Node> attributes = node.selectNodes("./attribute");
            String nodeID   = attributes.get(0).getText();
            String nodeName = attributes.get(1).getText();
            String nodeType = getType(nodeName);
            Node graphics = node.selectSingleNode("./section[@name='graphics']");
            String xpos = graphics.selectSingleNode("./attribute[@key='x']").getText();
            String ypos = graphics.selectSingleNode("./attribute[@key='y']").getText();
            int x = (int)Double.parseDouble(xpos);
            int y = (int)Double.parseDouble(ypos);
            String nodeRow = "yednode" + "(" + nodeID + ", '" + nodeName + "' ," + nodeType + ", " + x + ", " + y + ")" + "." + "\n";
            sb.append(nodeRow);
            

//            String posRow = "yedposition" + "(" + nodeID + ", " + xpos + ", " + ypos + ")" + "." + "\n";
//            posInfo.add(posRow);
        }
        
        sb.append("\n");
        
        List<Node> transitions = document.selectNodes("//section[@name='edge']");
        int fakeID = 0;
        for (Node transition : transitions) {
            String transitionID = "edge" + Integer.toString(fakeID);
            fakeID++;
            List<Node> attributes = transition.selectNodes("./attribute");
            String transitionSrc  = attributes.get(0).getText();
            String transitionDes  = attributes.get(1).getText();
            String transitionRow = "yedtransition" + "(" + transitionID + ", " + transitionSrc + ", " + transitionDes + ")" + "." + "\n";
            sb.append(transitionRow);
        }
        sb.append("\n");
        
//        for(String s : posInfo) {
//            sb.append(s);
//        }
        
        sb.append("\n");
        

        writeFile(fileName, sb.toString());
    }

    private static void writeFile(String fileName, String content) throws IOException {
        File file = new File(fileName + ".yedfsm.pl");
        if (!file.exists()) {
            file.createNewFile();
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(content);
            bufferedWriter.flush();
        }
    }

    private static String getType(String name) {
        switch (name) {
            case "start":
                return "start";
            case "end":
                return "stop";
            default:
                return "state";
        }
    }
    public static void main(String[] args) throws DocumentException {
        try {
                generatePL("yedfsm");
        } catch (IOException ex) {
            Logger.getLogger(FSMParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
