:-style_check(-discontiguous).

dbase(gliffy_fsm,[gliffy_node,gliffy_transition,gliffy_position]).
table(gliffy_node,[nodeid,"name",type]).
table(gliffy_transition,[transid,startsAt,endsAt]).
table(gliffy_position,[id,x,y]).

to_gliffy_id(I1, I2) :- findall(I, node(I,_,_), L1), findall(I, transition(I,_,_), L2), append(L1, L2, L), nth0(I2, L, I1).
max_id(I) :- findall(I, node(I,_,_,_,_), L1), findall(I, transition(I,_,_), L2), append(L1, L2, L), length(L, I).

gliffy_node(I2,N,T):-node(I1,N,T), to_gliffy_id(I1, I2).
gliffy_transition(I2,N1_2,N2_2):-transition(I1,N1_1,N2_1), to_gliffy_id(I1, I2), to_gliffy_id(N1_1, N1_2), to_gliffy_id(N2_1, N2_2).

gliffy_position(I2,X,Y) :- position(I1,X,Y), to_gliffy_id(I1, I2).