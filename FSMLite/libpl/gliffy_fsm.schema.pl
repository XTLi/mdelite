/*  file gliffy_fsm.schema.pl -- Gliffy FSM Schema Declaration */

dbase(gliffy_fsm,[gliffy_node,gliffy_transition,gliffy_position]).
table(gliffy_node,[nodeid,"name",type]).
table(gliffy_transition,[transid,startsAt,endsAt]).
table(gliffy_position,[id,x,y]).

tuple(gliffy_node, L) :- gliffy_node(I,N,T), L=[I,N,T].
tuple(gliffy_transition, L) :- gliffy_transition(I,N1,N2), L=[I,N1,N2].
tuple(gliffy_position, L) :- gliffy_position(I,X,Y), L=[I,X,Y].