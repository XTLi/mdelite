:-style_check(-discontiguous).

dbase(fsm,[node,transition,position]).
table(node,[nodeid,"name",type]).
node(I,N,T) :- gliffy_node(I,N,T).
table(transition,[transid,startsAt,endsAt]).
transition(I,N1,N2) :- gliffy_transition(I,N1,N2).
table(position,[id,x,y]).
position(I,X,Y) :- gliffy_position(I,X,Y).

