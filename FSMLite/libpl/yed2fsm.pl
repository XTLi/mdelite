:-style_check(-discontiguous).

table(yednode,[nodeid,"name",type]).
node(I,N,T):-yednode(I,N,T,_,_).
table(yedtransition,[transid,startsAt,endsAt]).
transition(I,N1,N2):-yedtransition(I,N1,N2).
position(I,X,Y) :-yednode(I,_,_,X,Y).
