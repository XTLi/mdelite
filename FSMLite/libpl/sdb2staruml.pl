/* file: sdb2staruml.pl */

table(starnode,[nodeid,"name",type]).
table(startransition,[transid,startsAt,endsAt]).
table(starposition,[id,x,y]).

starnode(I,N,T):-node(I,N,T).
startransition(I,S,E):-transition(I,S,E).
starposition(I,X,Y):-position(I,X,Y).
