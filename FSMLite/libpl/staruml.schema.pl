/* file staruml.schema.pl -- declares the StarUML schema */

dbase(staruml,[starnode,startransition,starposition]).

table(starnode,[nodeid,"name",type]).
table(startransition,[transid,startsAt,endsAt]).
table(starposition,[id,x,y]).

tuple(starnode,L):-starnode(I,N,T),L=[I,N,T].
tuple(startransition,L):-startransition(I,S,E),L=[I,S,E].
tuple(starposition,L):-starposition(I,X,Y),L=[I,X,Y].
