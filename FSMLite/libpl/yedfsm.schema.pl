/*  file yedfsm.schema.pl -- yed FSM Schema Declaration */

dbase(yedfsm,[yednode,yedtransition]).
table(yednode,[nodeid,"name",type,x,y]).
table(yedtransition,[transid,startsAt,endsAt]).

tuple(yednode, L):-yednode(I,N,T,X,Y),L=[I,N,T,X,Y].
tuple(yedtransition, L):-yedtransition(I,N1,N2),L=[I,N1,N2].
