/* file sdb.schema.pl -- declares the SDB schema */

dbase(sdb,[node,transition,position]).

table(node,[nodeid,"name",type]).
table(transition,[transid,startsAt,endsAt]).
table(position,[id,x,y]).

tuple(node,L):-node(I,N,T),L=[I,N,T].
tuple(transition,L):-transition(I,S,E),L=[I,S,E].
tuple(position,L):-position(I,X,Y),L=[I,X,Y].
