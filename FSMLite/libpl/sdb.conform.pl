/* file staruml.conform.pl */

/*Constraint a: The attribute 'nodeid' in node table must be unique*/
a_error :- findall(X, node(X, _, _), L), not(is_set(L)).
nodeid_Constraint :- forall(a_error, isError('Error:     a_error -> nodeid is not unique.', '')).

/*Constraint b: The attribute 'type' in node table can only be specified as 'start', 'state' or 'stop'*/
b_error(X) :- node(X, _, Y), not(member(Y, [start, state, stop])).
nodetype_Constraint :- forall(b_error(X), isError('Error:     b_error -> node type is not from [start, state, stop]: ', X)).

/*Constraint c: The attribute 'transid' in transition table must be unique*/
c_error :- findall(X, transition(X, _, _), L), not(is_set(L)).
transid_Constraint :- forall(c_error, isError('Error:     c_error -> transid is not unique.', '')).

/*Constraint d: The attributes 'startsAt' and 'endsAt' must be type of 'nodeid' in node table*/
d_error(X) :- transition(X, Y, _), not(node(Y, _, _)).
d_error(X) :- transition(X, _, Y), not(node(Y, _, _)).
startsAt_endsAt_Constraint :- forall(d_error(X), isError('Error:     d_error -> startsAt or endsAt are not type of nodeid: ', X)).

/*Constraint e: Any node except stopnode could be a starting node in a transition*/
e_error(X) :- transition(X, Y, _), Y==stopNode.
startsAt_Constraint :- forall(e_error(X), isError('Error:     e_error -> startsAt is type of stopnode: ', X)).

/*Constraint f: Any node except startnode could be a ending node in a transition*/
f_error(X) :- transition(X, _, Y), Y==startNode.
endsAt_Constraint :- forall(f_error(X), isError('Error:     f_error -> endsAt is type of startnode: ', X)).

/*Constraint g: startnode and stopnode must be unique*/
h_error(X) :- node(X1, _, Y), node(X, _, Y), Y==start, X1@<X.
h_error(X) :- node(X1, _, Y), node(X, _, Y), Y==stop, X1@<X.
startnode_endnode_duplicate_Constraint :- forall(h_error(X), isError('Error:     h_error -> either startnode or stopnode is duplicated: ', X)).

run :- nodeid_Constraint, nodetype_Constraint, transid_Constraint, startsAt_endsAt_Constraint, startsAt_Constraint, endsAt_Constraint, startnode_endnode_duplicate_Constraint.
