/* file: staruml2sdb.pl */

:-style_check(-discontiguous).

table(starnode,[nodeid,"name",type]).
table(startransition,[transid,startsAt,endsAt]).
table(starposition,[id,x,y]).

node(I,N,T):-starnode(I,N,T).
transition(I,S,E):-startransition(I,S,E).
position(I,X,Y):-starposition(I,X,Y).
